/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.obra.ObraModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.obra-tela',

    data: {
    },

    stores: {
        Store: {
            type: 'obra-tela',
        }
    }
});