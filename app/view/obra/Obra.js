/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.obra.Obra', {
    extend: 'Ext.Panel',
    xtype: 'obra-tela',
    layout: 'vbox',
    requires: [],

    controller: 'obra-tela',
    viewModel: 'obra-tela',

    bodyCls:'obra-tela',

    voltarAposSelecao: true,

    padding: 20,

    scrollable: 'y',

    defaults: {
        xtype: 'obra-card',
    },

    items: [],

    listeners: {
        activate: 'onActivate'
    }
});