/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.obra.Card', {
    extend: 'Ext.Component',
    xtype: 'obra-card',
    requires: [],

    controller: 'obra-card',
    viewModel: 'obra-card',

    margin: '0 0 15 0',

    shadow: true,

    cls:'obra-card',

    tpl: [
        '<div>',
        '   <div class="obra-card-header">',
        '       <div class="obra-card-identificacao">{ObraId}<br>{Sigla}</div>',
        '       <div class="obra-card-titulo">{Obra}</div>',
        '       <div class="obra-card-checklist">',
        '           <div class="obra-card-checklist-item"><div class="status-icon-pendente"></div><br>{Checklist.Pendentes}</div>',
        '           <div class="obra-card-checklist-item"><div class="status-icon-nao-aprovado"></div><br>{Checklist.Rejeitados}</div>',
        '           <div class="obra-card-checklist-item"><div class="status-icon-aprovado"></div><br>{Checklist.Aprovados}</div>',
        '       </div>',
        '   </div>',
        '</div>'
    ],

    bind:{
        data:'{data}'
    },

});