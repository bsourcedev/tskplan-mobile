/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.obra.CardModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.obra-card',

    data: {
        data: {
            ObraId: 0,
            Nome: '',
            Sigla: '',
            Checklist: {
                Pendentes: 0,
                Aprovados: 0,
                NaoAprovados: 0
            }
        }
    }
});