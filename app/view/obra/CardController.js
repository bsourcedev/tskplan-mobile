/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.obra.CardController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.obra-card',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();

        viewModel.set('data', {
            ObraId: view.ObraId,
            Obra: view.Nome,
            Sigla: view.Sigla,
            Checklist: {
                Pendentes: view.Pendentes,
                Aprovados: view.Aprovados,
                Rejeitados: view.Rejeitados
            }
        });

        view.element.on('tap', me.onContainerTap, me);
    },

    onContainerTap: function () {
        var me = this,
            view = me.getView(),
            obraTela = view.up('obra-tela'),
            viewModel = me.getViewModel();

        obraTela.getController().obraSelecionada({
            ObraId: viewModel.get('data.ObraId'),
            Sigla: viewModel.get('data.Sigla'),
            Obra: viewModel.get('data.Obra')
        });
    }
});