/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.obra.SeletorController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.obra-tela',

    onActivate: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            store = viewModel.get('Store');

        view.mask('Carregando dados...');
        
        store.load({
            scope: me,
            callback: me.loadStoreCallback
        });
    },

    loadStoreCallback: function (records) {
        var me = this,
            view = me.getView();

        view.unmask();

        if (records) {
            Ext.each(records, function (rec) {
                view.add(rec.data);
            });
        }
    },

    obraSelecionada: function (obra) {
        var me = this,
            view = me.getView();

        view.fireEvent('obraSelecionada', obra);

        //<debug>
        console.log(`Obra Selecionada: ${obra.ObraId} - ${obra.Obra}`);
        //</debug>  

        if (view.voltarAposSelecao)
            ScreenManager.voltar();
    }
});