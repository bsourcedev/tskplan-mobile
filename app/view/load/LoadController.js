/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.load.LoadController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.load-tela',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            references = view.getReferences();

        //<debug>
        console.log('Carregando stores IDB');
        //</debug>

        Ext.on('appLoadingProgressUpdate', function (progressInfo) {
            var progresso = ((progressInfo.current / progressInfo.total) * 100).toFixed(0);

            if (viewModel.isDestroyed) {
                return;
            }

            //<debug>
            console.log(`${(' '.repeat(7-progresso.toString().length))}[${progresso}%] Store [${progressInfo.store}] carregada`);
            //</debug>

            viewModel.set('progress', progresso);
        });

        Ext.on('appLoadingFinished', function () {
            if (viewModel.isDestroyed) {
                return;
            }
            
            references.progressBar.setStyle('background-color: #4CAF50;');
            references.loadDone.addCls('load-done-animate');
            references.loadBounce.addCls('loading-bounce-done');

            //<debug>
            console.log('Carregamento concluido');
            //</debug>

            setTimeout(function () {
                view.close();
            }, 1100);
        });

        DataManager.syncStores();
    }

});