/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.load.LoadModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.load-tela',

    data: {
        progress: 0
    }
});