/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.tarefa.TarefaLocaisChecklistController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.tarefa-locais-checklist',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            viewData = view.extraData,
            store = viewModel.get('Store'),
            storeAuxiliar = viewModel.get('StoreAuxiliar');

        me.selectedNodes = [];

        storeAuxiliar.getProxy().extraParams = {
            ObraId: viewData.record.get('ObraId')
        };

        store.getProxy().extraParams = {
            ObraId: viewData.record.get('ObraId'),
            TarefaId: viewData.record.get('TarefaId')
        };

        store.load({
            scope: me,
            callback: me.loadStoreCallback
        });
    },

    onActive: function () {

    },

    loadStoreCallback: function () {
        var me = this,
            viewModel = me.getViewModel(),
            store = viewModel.get('Store'),
            storeAuxiliar = viewModel.get('StoreAuxiliar'),
            items = store.data.items;

        if (items.length > 0)
            me.prepareTreeStore(items);
        else {
            storeAuxiliar.load({
                scope: me,
                callback: me.loadStoreAuxiliarCallback
            });
        }
    },

    loadStoreAuxiliarCallback: function () {
        var me = this,
            viewModel = me.getViewModel(),
            storeAuxiliar = viewModel.get('StoreAuxiliar'),
            items = storeAuxiliar.data.items;

        //<debug>
        console.log('Utilizando store auxiliar');
        //</debug>

        if (items.length > 0)
            me.prepareTreeStore(items);
        else {
            Ext.toast('Nenhum local disponivel na obra ou salvo no modo offline.', 8000)
            ScreenManager.voltar();
        }
    },

    prepareTreeStore: function (records) {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            treeStore = Ext.create('Ext.data.TreeStore', {
                rootVisible: false,
                root: {
                    expanded: true,
                    children: me.prepareData(records)
                }
            });

        view.setStore(treeStore);

        view.unmask();
    },

    prepareData: function (recordSet, parentId) {
        var me = this,
            data = [],
            records;
        parentId = (parentId ? parentId : 0);
        records = recordSet.filter(rec => rec.get('PaiId') == parentId);

        records.forEach(record => {
            var childrens = me.prepareData(recordSet, record.get('ObraLocalId'));

            data.push({
                Selecionado: false,
                ObraLocalId: record.get('ObraLocalId'),
                Caminho: record.get('Caminho'),
                Nome: record.get('Nome'),
                parentId: record.get('PaiId'),
                children: childrens,
                leaf: childrens.length > 0 ? false : true,
                guid: record.get('guid')
            });
        });

        return data;
    },

    onCheckChange: function (cell, rowIndex, checked, record) {
        var me = this,
            viewModel = me.getViewModel();

        if (!record.isLeaf())
            me.updateChildNodes(record.childNodes, checked);
        else {
            viewModel.set('locaisSelecionados', viewModel.get('locaisSelecionados') + (checked ? 1 : -1));
            checked ? me.selectedNodes.push(record) : Ext.Array.remove(me.selectedNodes, record);
        }

        if (record.parentNode.id !== 'root')
            me.updateParentNode(record.parentNode);
    },

    updateParentNode: function (parentNode) {
        var me = this;

        parentNode.set('Selecionado', (parentNode.childNodes.filter(node => node.get('Selecionado') == true).length > 0));

        if (parentNode.parentNode.id !== 'root')
            me.updateParentNode(record.parentNode);
    },

    updateChildNodes: function (childNodes, checked) {
        var me = this,
            viewModel = me.getViewModel();

        childNodes.forEach(node => {
            node.set('Selecionado', checked);
            if (!node.isLeaf())
                me.updateChildNodes(node.childNodes, checked);
            else {
                viewModel.set('locaisSelecionados', viewModel.get('locaisSelecionados') + (checked ? 1 : -1));
                checked ? me.selectedNodes.push(node) : Ext.Array.remove(me.selectedNodes, node);
            }
        });
    },

    onClickConfirmar: function () {
        var me = this,
            view = me.getView(),
            selectedNodes = me.selectedNodes
            .filter(node => node.get('leaf'))
            .map(node => new Object({
                Nome: node.get('Nome'),
                Caminho: node.get('Caminho'),
                ObraLocalId: node.get('ObraLocalId')
            }));

        if (selectedNodes.length <= 0) {
            Ext.toast('Nenhum local selecionado!', 5000);
            return;
        }

        view.fireEvent('LocaisSelecionados', selectedNodes);

        ScreenManager.voltar();
    }
});