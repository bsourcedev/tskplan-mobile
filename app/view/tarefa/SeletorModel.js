/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.tarefa.SeletorModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.tarefa-seletor-tela',

    data: {

    },

    stores:{
        Store: {            
            type: 'tarefa-seletor',
        }
    }
});
