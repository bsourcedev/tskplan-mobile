/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.tarefa.SeletorController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.tarefa-seletor-tela',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            store = viewModel.get('Store');

        store.getProxy().extraParams = {
            obraId: view.obraId ? view.obraId : 0,
            obraLocalId: view.obraLocalId ? view.obraLocalId : 0
        };

        store.load({
            callback: function (records) {
                if (!records || records.length == 0) {
                    //<debug>
                    console.log('0 Tarefas encontradas, retornando a tela anterior')
                    //</debug>
                    ScreenManager.voltar();
                }
            }
        });
    },

    onItemSelect: function (listView, selected) {
        var me = this,
            view = me.getView(),
            tabView = view.up();

        view.fireEvent('tarefaSelecionada', {
            TarefaId: selected.get('ObraCronogramaTarefaId'),
            Nome: selected.get('Tarefa')
        });

        //<debug>
        console.log(`Tarefa Selecionada: ${selected.get('ObraCronogramaTarefaId')} - ${selected.get('Tarefa')}`);
        //</debug>  

        ScreenManager.voltar();
    }
});