/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.tarefa.TarefaLocaisChecklist', {
    extend: 'Ext.grid.Tree',
    xtype: 'tarefa-locais-checklist',
    requires: [
        'Ext.grid.plugin.CellEditing'
    ],

    controller: 'tarefa-locais-checklist',
    viewModel: 'tarefa-locais-checklist',

    flex: 1,
    width: '100%',
    height: '100%',
    hideHeaders: true,

    columns: [{
        xtype: 'treecolumn',
        flex: 1,
        dataIndex: 'Nome',
        editable: true
    }, {
        xtype: 'checkcolumn',
        width: 80,
        dataIndex: 'Selecionado',
        editable: true,
        listeners: {
            checkchange: 'onCheckChange'
        }
    }],

    items: {
        xtype: 'container',
        docked: 'bottom',
        width: '100%',
        flex: 1,
        items: [{
            xtype: 'label',
            cls: 'tarefa-locais-selecionados',
            bind: {
                html: 'Locais Selecionados: {locaisSelecionados}'
            }
        }, {
            xtype: 'button',
            text: 'confirmar',
            margin: '0 20 5 20',
            width: '-webkit-fill-available',
            handler: 'onClickConfirmar'
        }]
    }

    // listeners: {
    //     activate: 'onActive'
    // }
});