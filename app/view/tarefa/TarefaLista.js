/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.tarefa.TarefaLista', {
    extend: 'Ext.Panel',
    xtype: 'tarefa-lista-tela',
    requires: [
        'Ext.SegmentedButton'
    ],
    layout: 'vbox',

    controller: 'tarefa-lista-tela',
    viewModel: 'tarefa-lista-tela',


    bodyCls: 'tela-background',

    items: [{
        xtype: 'panel',
        bodyPadding: '10 10 0 10',
        layout: 'vbox',
        items: [{
            xtype: 'container',
            layout: 'vbox',
            items: [{
                xtype: 'segmentedbutton',
                width: '100%',
                // margin: '5 0 10 0',
                items: [{
                    text: 'Aberta',
                    value: 0,
                    cls: 'tarefa-lista-segmented-button',
                    flex: 0.8
                }, {
                    text: 'Andamento',
                    value: 1,
                    pressed: true,
                    cls: 'tarefa-lista-segmented-button',
                    flex: 1.1
                }, {
                    text: 'Concluida',
                    value: 2,
                    cls: 'tarefa-lista-segmented-button',
                    flex: 1.1
                }],
                listeners: {
                    toggle: 'onToggleStatus'
                }
            }]
        }, {
            xtype: 'container',
            items: [{
                xtype: 'combobox',
                // margin: '-10 0 0 0',
                forceSelection: true,
                queryMode: 'local',
                label: '<i class="far fa-building tarefa-icon-descricao"></i> Obra',
                displayField: 'Nome',
                valueField: 'ObraId',
                bind: {
                    store: '{Obra}',
                    value: '{FiltroObraId}'
                }
            }, {
                xtype: 'textfield',
                label: 'Tarefa',
                margin: '-10 0 0 0',
                bind: {
                    value: '{FiltroTarefa}'
                }
            }],
            bind: {
                hidden: '{!showFilters}'
            }
        }, {
            xtype: 'button',
            textAlign: 'left',
            margin: '10 0 0 0',
            width: 'fit-content',
            handler: 'onClickFiltros',
            bind: {
                text: '{filterButtonText}',
            }
        }]
    }, {
        xtype: 'component',
        cls: 'divisor',
    }, {
        xtype: 'list',
        bodyCls: 'tela-background',
        padding: '0 10',
        width: '100%',
        flex: 1,
        bind: {
            store: '{Store}'
        },
        itemCls: 'tarefa-card x-shadow',
        itemContentCls: 'tarefa-content',
        itemTpl: [
            '<div class="card-titulo tarefa-nome">{Nome}</div>',
            '<spam class="tarefa-obra">{Obra}</spam>',
            '<spam class="tarefa-id">#{TarefaId}</spam>',
            '<spam class="tarefa-porcentagem">',
            // '<div style="text-align: center;">{Porcentagem}%</div>',
            '{[TskPlanManager.progressBar(values.Porcentagem)]}',
            '</spam>',
            '<spam class="tarefa-icone">{IconeFormatado}</spam>',
            '<spam class="tarefa-icone-status">{IconeStatus}</spam>',
            '<spam class="tarefa-data">{DataInicioFormatada} - {DataFimFormatada}</spam>'
        ],
        listeners: {
            select: 'onSelect'
        }
    }, {
        xtype: 'button',
        text: 'Nova Tarefa',
        iconCls: 'x-fa fa-plus',
        floating: true,
        renderTo: Ext.getBody(),
        ui: 'action raised round shadow',
        height: 40,
        handler: 'onClickNovaTarefa',
        bind: {
            // style: 'position: absolute; top: {addButtonTopPossition}px; right: 10px;'
            style: 'position: absolute; top: 70px; right: 10px;',
            hidden:'{showFilters}'
        }
    }, {
        xtype: 'button',
        text: 'Filtrar',
        iconCls: 'x-fa fa-search',
        floating: true,
        renderTo: Ext.getBody(),
        ui: 'action raised round shadow',
        height: 40,
        handler: 'onClickFiltrar',
        bind: {
            style: 'position: absolute; top: 224px; right: 10px;',
            hidden:'{!showFilters}'
        }
    }],

    // listeners: {
    //     painted: 'onActivate'
    // }
});