/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.tarefa.TarefaLocaisModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.tarefa-locais',

    data: {
    },

    formulas: {
        // lblObra: function (get) {
        //     var lbl = get('Obra');
        //     return lbl ? lbl : 'Selecionar';
        // }
    },

    stores: {
        Store: {
            type: 'tarefa-locais',
        }
    }
});