/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.tarefa.TarefaLocaisChecklistModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.tarefa-locais-checklist',

    data: {
        locaisSelecionados: 0
    },

    formulas: {
        // lblObra: function (get) {
        //     var lbl = get('Obra');
        //     return lbl ? lbl : 'Selecionar';
        // }
    },

    stores: {
        Store: {
            type: 'tarefa-locais-checklist',
        },
        StoreAuxiliar: {
            type: 'tarefa-locais-checklist-auxiliar',
        }
    }
});