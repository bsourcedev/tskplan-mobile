/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.tarefa.TarefaController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.tarefa-tela',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            viewData = view.extraData,
            record = viewData && viewData.record ? viewData.record : null;

        if (!record) {
            record = Ext.create('TskPlan.Mobile.model.tarefa.Tarefa', {
                DataInicio: new Date(),
                DataFim: new Date(),
            });

            viewModel.set({
                Record: record,
                Editor: true,
                Creation: true,
                Loading: false,
            });
        } else {
            viewModel.set({
                Record: record,
                Editor: false,
                Creation: false,
                Loading: true
            });

            me.tarefaGuid = record.getId();

            me.loadRecord();
        }
    },

    onClickEditar: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();

        viewModel.set('Editor', true);
    },

    onClickSalvar: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            store = viewModel.get('Store'),
            references = view.getReferences();

        if (viewModel.get('Creation')) {
            var record = viewModel.get('Record');

            record.set({
                Obra: references.cmbObra.getInputValue(),
                UnidadeMedida: references.cmbUnidadeMedida.getInputValue(),
                ModeloServico: references.cmbModeloServico.getInputValue()
            });

            store.add(record);
        }

        if (store.getModifiedRecords().length > 0) {
            view.mask();
            store.sync({
                success: function () {
                    var record = store.data.items[0];

                    viewModel.set({
                        Creation: false,
                        Editor: false,
                        Record: record
                    });

                    me.tarefaGuid = record.getId();

                    if (view.extraData)
                        view.extraData.record = record;
                    else
                        view.extraData = {
                            record: record
                        };

                    view.unmask();
                    Ext.toast('Registro salvo', 5000);
                },
                failure: function () {
                    store.rejectChanges();
                    view.unmask();
                    Ext.toast('Não foi possivel salvar as alterações', 5000);
                }
            });
        } else
            Ext.toast('Nenhuma alteração para salvar', 5000);
    },

    onClickCancelar: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            store = viewModel.get('Store');

        store.rejectChanges();
        viewModel.set('Editor', false);

        Ext.toast('Alterações canceladas', 4000);
    },

    loadRecord: function () {
        var me = this,
            viewModel = me.getViewModel(),
            store = viewModel.get('Store'),
            storeArquivos = viewModel.get('Arquivos');

        //Store Arquivos
        storeArquivos.setParams([{
            name: 'tarefaGuid',
            value: me.tarefaGuid
        }]);

        storeArquivos.on({
            scope: me,
            load: me.onStoreArquivoLoad
        });

        storeArquivos.load();

        //Store Tarefa
        store.getProxy().extraParams = {
            TarefaGuid: me.tarefaGuid
        };

        store.on({
            scope: me,
            load: me.onStoreLoad
        });

        storeArquivos.load();
        store.load();
    },

    onStoreArquivoLoad: function () {
        var me = this,
            view = me.getView(),
            references = view.getReferences(),
            viewModel = me.getViewModel(),
            storeArquivos = viewModel.get('Arquivos'),
            items = storeArquivos.data.items.map(rec => new Object({
                Nome: rec.get('Nome'),
                Caminho: rec.get('Caminho'),
                Extensao: rec.get('Tipo')
            }));

        if (items.length && window.navigator.onLine){
            viewModel.set('hideGallery', false)
            references.galeria.loadFiles(items);
        }
        else
            viewModel.set('hideGallery', true)
    },

    onStoreLoad: function (store) {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            record = store.findRecord('guid', me.tarefaGuid),
            records = store.data.items;

        if (record && records.length < 1)
            Ext.toast('Não foi possivel buscar dados atuais da tarefa', 5000);

        if (record) {
            viewModel.set({
                Record: record,
                Loading: false,
                Editor: false,
                Creation: false
            });
        } else {
            Ext.Msg.alert('Atenção', 'Registro não encontrado (verifique sua conexão com a internet)', () => view.close());
        }
    },

    onClickLocais: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            component = Ext.create('TskPlan.Mobile.view.tarefa.TarefaLocais', {
                extraData: view.extraData,
                TarefaGuid: me.tarefaGuid
            });

        ScreenManager.abrirTela({
            screen: component,
            anterior: view,
            title: 'Tarefa - Locais'
        });
    }

});