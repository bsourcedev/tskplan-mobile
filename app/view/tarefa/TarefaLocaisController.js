/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.tarefa.TarefaLocaisController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.tarefa-locais',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            store = viewModel.get('Store');

    },

    onActive: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            store = viewModel.get('Store');

        store.getProxy().extraParams = {
            tarefaGuid: view.TarefaGuid,
            checked: 1
        };

        store.load();
    },

    createMenu: function () {
        var me = this;

        me.menu = me.menu ? me.menu : Ext.create({
            xtype: 'actionsheet',
            viewModel: me.getViewModel(),
            hideOnMaskTap: false,
            side: 'bottom',
            reveal: false,
            cover: true,
            hideAnimation: 'slideOut',
            items: [{
                xtype: 'component',
                cls: 'card-titulo',
                html: 'Editar Local',
                bind: {
                    html: 'Editar - {Record.Local}',
                }
            }, {
                xtype: 'datefield',
                label: 'Data Inicio',
                dateFormat: 'd/m/Y',
                bind: {
                    value: '{Record.DataInicio}'
                }
            }, {
                xtype: 'datefield',
                label: 'Data Final',
                dateFormat: 'd/m/Y',
                margin: '-15 0 0 0',
                bind: {
                    value: '{Record.DataFim}'
                }
            }, {
                text: 'Confirmar',
                scope: me,
                ui: 'action',
                handler: 'onSheetClickConfirmar'
                // }, {
                //     text: 'Remover Local',
                //     scope: me,
                //     ui: 'decline',
                //     handler: 'onSheetClickRemoverLocal'
            }, {
                text: 'Cancelar',
                scope: me,
                handler: 'onSheetClickCancelar'
            }]
        });

        me.menu.show();
    },

    hideMenu: function () {
        var me = this,
            menu = me.menu;

        me.clearSheetData();

        //criar override global para os sheets que removam o viewmodel antes de fechar
        menu.setViewModel();

        menu.hide();
        // menu.close();
    },

    clearSheetData: function () {
        var me = this,
            viewModel = me.getViewModel();

        viewModel.set({
            Record: null
        });
    },

    onSheetClickConfirmar: function () {
        var me = this,
            viewModel = me.getViewModel();

        if (store.getModifiedRecords().length) store.sync();
        else Ext.toast('Nenhuma alteração a salvar!', 50000);
    },

    // onSheetClickRemoverLocal: function () {},

    onSheetClickCancelar: function () {
        var me = this;

        me.hideMenu();
    },


    onSelect: function (list, selected) {
        var me = this,
            viewModel = me.getViewModel();

        viewModel.set({
            Record: selected
        });

        me.createMenu();
    },

    onClickAdicionar: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            component = Ext.create('TskPlan.Mobile.view.tarefa.TarefaLocaisChecklist', {
                extraData: view.extraData
            });

        component.on({
            scope: me,
            LocaisSelecionados: me.onAdicionarLocais
        });

        ScreenManager.abrirTela({
            screen: component,
            anterior: view,
            title: 'Adicionar Ordens de Serviço'
        });
    },

    onAdicionarLocais: function (locais) {
        var me = this,
            view = me.getView(),
            record = view.extraData.record,
            viewModel = me.getViewModel(),
            store = viewModel.get('Store');

        locais.forEach(local => {
            store.add({
                TarefaGuid: record.getId(),
                ObraLocalId: local.ObraLocalId,
                Checked: true,
                Local: local.Nome,
                DataInicio: record.get('DataInicio'),
                DataFim: record.get('DataFim')
            });
        });

        if (store.getModifiedRecords().length) {
            store.sync();
        }
    }
});