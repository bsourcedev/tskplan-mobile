/**
 * View de seleção de obra para outras telas
 */
Ext.define('TskPlan.Mobile.view.tarefa.Seletor', {
    extend: 'Ext.dataview.List',
    xtype: 'tarefa-seletor-tela',
    requires: [
    ],
   
    controller: 'tarefa-seletor-tela',

    viewModel: 'tarefa-seletor-tela',

    bind: {
        store: '{Store}'
    },

    itemTpl: '<div>{Tarefa}</div>',

    listeners:{
        // activate:'onActivate',
        select:'onItemSelect'
    }
});
