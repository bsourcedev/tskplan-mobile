/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.tarefa.TarefaLocais', {
    extend: 'Ext.Panel',
    xtype: 'tarefa-locais',
    requires: [],
    // layout: 'vbox',

    controller: 'tarefa-locais',
    viewModel: 'tarefa-locais',

    bodyCls: 'tela-background',
    padding: '0 10',

    items: [{
        xtype: 'list',
        bodyCls: 'tela-background',
        width: '100%',
        height: '100%',
        flex: 1,
        bind: {
            store: '{Store}'
        },
        itemCls: 'tarefa-local-card x-shadow',
        itemContentCls: 'tarefa-local-content',
        itemTpl: [
            '<div class="card-titulo" style="display: contents;"><i class="fas fa-circle" style="font-size:12px" tarefa-status="{AndamentoTexto}"></i>&nbsp;{Local}</div> ',
            '<div class="card-descricao">{DataInicioFormatada} - {DataFimFormatada}</div>'
        ],
        listeners: {
            select: 'onSelect'
        }
    }, {
        xtype: 'button',
        text: 'Adicionar',
        iconCls: 'x-fa fa-plus',
        floating: true,
        renderTo: Ext.getBody(),
        ui: 'action raised round shadow',
        height: 40,
        style: 'position: absolute; bottom: 10px; right: 10px;',
        handler: 'onClickAdicionar'
    }],

    listeners: {
        activate: 'onActive'
    }
});