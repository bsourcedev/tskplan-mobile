/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.tarefa.TarefaListaController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.tarefa-lista-tela',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();

        me.lastStatus = 1;

        me.loadStore();
    },


    loadStore: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            store = viewModel.get('Store');

        store.setParams([{
            name: 'statusId',
            value: me.lastStatus
        }, {
            name: 'obraId',
            value: viewModel.get('FiltroObraId')
        }, {
            name: 'query',
            field: 'Nome',
            value: viewModel.get('FiltroTarefa'),
            operator: 'like',
            exclude: false
        }]);

        view.mask('Carregando tarefas');

        store.load({
            callback: function () {
                view.unmask();
            }
        });
    },

    onClickNovaTarefa: function () {
        var me = this,
            view = me.getView();

        ScreenManager.abrirTela({
            screen: Ext.create('TskPlan.Mobile.view.tarefa.Tarefa', {
                // dataEntrega: new Date(),
                // store: viewModel.get('Store')
            }),
            anterior: view,
            title: 'Tarefa'
        });
    },

    onSelect: function (list, selected) {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();

        ScreenManager.abrirTela({
            screen: Ext.create('TskPlan.Mobile.view.tarefa.Tarefa', {
                extraData: {
                    dataEntrega: new Date(),
                    store: viewModel.get('Store'),
                    record: selected
                }
            }),
            anterior: view,
            title: 'Tarefa'
        });
    },

    onDateChange: function (dtpicker, newValue, oldValue) {
        // var me = this,
        //     view = me.getView(),
        //     viewModel = me.getViewModel(),
        //     store = viewModel.get('Store');

        // viewModel.set('showCleanButton', newValue ? true : false);

        // store.getProxy().extraParams.data = newValue ? Ext.Date.format(newValue, 'd/m/Y') : '';

        // store.load();
    },

    onEntregasChange: function (chk, newValue, oldValue) {
        // var me = this,
        //     view = me.getView(),
        //     viewModel = me.getViewModel(),
        //     store = viewModel.get('Store');

        // store.getProxy().extraParams.processados = newValue;

        // store.load();
    },

    cleanData: function () {
        // var me = this,
        //     view = me.getView(),
        //     viewModel = me.getViewModel(),
        //     refs = view.getReferences();

        // // viewModel.set('showCleanButton', false);

        // refs.dtEntrega.clearValue();
    },

    onToggleStatus: function (cnt, button, pressed) {
        var me = this

        if (pressed) {
            me.lastStatus = button.getValue();

            me.loadStore();
        }
    },

    onClickFiltros: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            showFilter = !viewModel.get('showFilters');

        viewModel.set('showFilters', showFilter);
    },

    onClickFiltrar: function () {
        var me = this;

        me.loadStore();
    }
});