/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.tarefa.TarefaListaModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.tarefa-lista-tela',

    data: {
        showCleanButton: false,
        showFilters: false,
        FiltroObraId: null,
        FiltroTarefa: null
    },

    formulas: {
        filterButtonText: function (get) {
            return get('showFilters') ? '- Filtros' : '+ Filtros';
        },
        addButtonTopPossition: function (get) {
            return get('showFilters') ? '225' : '70';
        }
    },

    stores: {
        Store: {
            type: 'tarefa-lista-tela',
        },
        Obra: {
            type: 'obra-global'
        }
    }
});