/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.tarefa.Tarefa', {
    extend: 'Ext.Panel',
    xtype: 'tarefa-tela',
    layout: 'vbox',
    requires: [
        'TskPlan.Mobile.view.tarefa.CardLocais'
    ],

    controller: 'tarefa-tela',
    viewModel: 'tarefa-tela',

    bodyPadding: '10 20 10 20',
    scrollable: 'y',
    defaults: {
        xtype: 'panel',
        cls: 'card',
        shadow: true,
        margin: '0 0 20 0',
        defaults: {
            xtype: 'label'
        }
    },

    bodyCls: 'checklistDetail-tela',

    style: 'background-color: whitesmoke;',
    bodyStyle: 'background-color: whitesmoke;',


    items: [{
        items: [{
            cls: 'card-titulo',
            html: 'Tarefa',
            style: 'line-height: 22px',
            bind: {
                html: '{Record.Nome}'
            }
        }],
        bind: {
            hidden: '{Creation}'
        }
    }, {
        cls: 'card checklist-detail-titulo',
        height: 46,
        items: {
            xtype: 'component',
            bind: {
                html: `{Record.IconeStatus}&nbsp;&nbsp;<spam class="checklist-detail-status">{status}</spam>`
            }
        },
        // html: '<div class="status-icon-aprovado"></div>&nbsp;&nbsp;<spam class="checklist-detail-status">Concluida</spam>',
        bind: {
            hidden: '{Creation}'
        }
    }, {
        items: [{
            html: 'Detalhes',
            cls: 'card-titulo'
        }, {
            cls: 'card-descricao',
            html: '<i class="far fa-building tarefa-icon-descricao"></i> -',
            bind: {
                html: '<i class="far fa-building tarefa-icon-descricao"></i> {Record.Obra}'
            }
        }, {
            cls: 'card-descricao tarefa-item-descricao',
            html: '<i class="fas fa-wrench tarefa-icon-descricao"></i> -',
            bind: {
                html: '<i class="fas fa-wrench tarefa-icon-descricao"></i> {Record.ModeloServico}'
            }
        }, {
            cls: 'card-descricao tarefa-item-descricao',
            html: '<i class="fas fa-balance-scale tarefa-icon-descricao"></i> -',
            bind: {
                html: '<i class="fas fa-balance-scale tarefa-icon-descricao"></i> {Record.UnidadeMedida}'
            }
        }, {
            cls: 'card-descricao tarefa-item-descricao',
            html: '<i class="fas fa-calculator tarefa-icon-descricao"></i> -',
            bind: {
                html: '<i class="fas fa-calculator tarefa-icon-descricao"></i> {Record.Quantidade}'
            }
        }, {
            cls: 'card-descricao tarefa-item-descricao',
            html: '<i class="fas fa-calendar-day tarefa-icon-descricao"></i> -',
            bind: {
                html: '<i class="fas fa-calendar-day tarefa-icon-descricao"></i> {Record.DataInicioFormatada} - {Record.DataFimFormatada}'
            }
        }, {
            cls: 'card-descricao tarefa-item-descricao',
            html: '<i class="far fa-comment-dots tarefa-icon-descricao"></i> -',
            bind: {
                html: '<i class="far fa-comment-dots tarefa-icon-descricao"></i> {Record.Observacao}',
                hidden: '{hideObservacao}'
            }
        }, {
            xtype: 'button',
            html: 'Editar',
            docked: 'bottom',
            margin: '15 0 0',
            handler: 'onClickEditar'
        }],
        hideAnimation: 'slideOut',
        bind: {
            hidden: '{Editor}'
        }
    }, {
        xtype: 'tarefa-card-locais',
        hideAnimation: 'slideOut',
        bind: {
            hidden: '{Editor}'
        }
    }, {
        defaults: {
            xtype: 'combobox',
            width: '-webkit-fill-available',
            margin: '-10 0 0 0',
            height: 86,
            forceSelection: true,
            queryMode: 'local'
        },
        items: [{
            xtype: 'label',
            html: 'Detalhes',
            cls: 'card-titulo',
            margin: '0 0 15 0',
            height: 'auto'
        }, {
            xtype: 'textfield',
            label: '<i class="far fa-edit tarefa-icon-descricao"></i> Tarefa',
            bind: {
                value: '{Record.Nome}'
            }
        }, {
            reference:'cmbObra',
            label: '<i class="far fa-building tarefa-icon-descricao"></i> Obra',
            displayField: 'Nome',
            valueField: 'ObraId',
            bind: {
                store: '{Obra}',
                value: '{Record.ObraId}',
                readOnly: '{!editObra}'
            }
        }, {
            reference:'cmbModeloServico',
            label: '<i class="fas fa-wrench tarefa-icon-descricao"></i> Modelo de Serviço',
            displayField: 'Nome',
            valueField: 'ModeloServicoId',
            bind: {
                store: '{ModeloServico}',
                value: '{Record.ModeloServicoId}'
            }
        }, {
            reference:'cmbUnidadeMedida',
            label: '<i class="fas fa-balance-scale tarefa-icon-descricao"></i> Unidade Medida',
            displayField: 'Nome',
            valueField: 'UnidadeMedidaId',
            bind: {
                store: '{UnidadeMedida}',
                value: '{Record.UnidadeMedidaId}'
            }
        }, {
            xtype: 'numberfield',
            label: '<i class="fas fa-calculator tarefa-icon-descricao"></i> Quantidade',
            bind: {
                value: '{Record.Quantidade}'
            }
        }, {
            xtype: 'datefield',
            label: 'Data de Inicio',
            dateFormat: 'd/m/Y',
            bind: {
                value: '{Record.DataInicio}'
            }
        }, {
            xtype: 'datefield',
            label: 'Data Final',
            dateFormat: 'd/m/Y',
            bind: {
                value: '{Record.DataFim}'
            }
        }, {
            xtype: 'textareafield',
            label: '<i class="far fa-comment-dots tarefa-icon-descricao"></i> Observação',
            height: 'auto',
            bind: {
                value: '{Record.Observacao}'
            }
        }, {
            xtype: 'button',
            html: 'Cancelar',
            docked: 'bottom',
            margin: '15 0 0',
            handler: 'onClickCancelar',
            height: 'auto',
            ui: 'decline',
            bind: {
                hidden: '{hideCancelar}'
            }
        }, {
            xtype: 'button',
            html: 'Salvar',
            docked: 'bottom',
            margin: '15 0 0',
            handler: 'onClickSalvar',
            ui: 'action raised',
            height: 'auto'
        }],
        hideAnimation: 'fadeOut',
        bind: {
            hidden: '{!Editor}'
        }
    }, {
        xtype:'galeria',
        reference:'galeria',
        bind: {
            hidden: '{hideGallery}'
        }
    },{
        defaults: {
            xtype: 'button',
            ui: 'action raised',
            width: '-webkit-fill-available',
            margin: '15 10 0 10'
        },

        items: [{
            xtype: 'label',
            html: 'Planejamento',
            cls: 'card-titulo',
            margin: '0'
        }, {
            xtype: 'button',
            text: 'Produtos'
        }, {
            xtype: 'button',
            text: 'Mão de obra'
        }, {
            xtype: 'button',
            text: 'Equipamentos'
        }, {
            xtype: 'button',
            text: 'Equipamentos Terceiros'
        }, {
            xtype: 'button',
            text: 'Checklist',
            margin: '15 10 10 10'
        }],
        bind: {
            // hidden: '{!showAprovado}'
        },
        hidden: true
    }, {
        defaults: {
            xtype: 'button',
            ui: 'action raised',
            width: '-webkit-fill-available',
            margin: '15 10 0 10'
        },

        items: [{
            xtype: 'label',
            html: 'Planejamento',
            cls: 'card-titulo',
            margin: '0'
        }, {
            xtype: 'button',
            text: 'Entregas'
        }, {
            xtype: 'button',
            text: 'Croqui'
        }, {
            xtype: 'button',
            text: 'Solicitações'
        }, {
            xtype: 'button',
            text: 'Risco & Segurança'
        }, {
            xtype: 'button',
            text: 'Arquivos'
        }, {
            xtype: 'button',
            text: 'Relatórios',
            margin: '15 10 10 10'
        }],
        bind: {
            // hidden: '{!showAprovado}'
        },
        hidden: true
    }, {
        layout: 'hbox',
        margin: '0 0 10 0',
        defaults: {
            xtype: 'component',
            cls: 'card-no-image',
            height: 150,
            flex: 1
        },
        items: [{
            margin: '0 10 0 0'
        }, {

        }],
        hidden: true
    }, {
        layout: 'hbox',
        defaults: {
            flex: 1
        },
        items: [{
            html: '<i class="fa fa-eye"></i>',
            bind: {
                cls: `checklist-conferencia {Visual}`
            }
        }, {
            html: '<i class="fa fa-tape"></i>',
            bind: {
                cls: `checklist-conferencia {Trena}`
            }
        }, {
            html: '<i class="fa fa-file invoice"></i>',
            bind: {
                cls: `checklist-conferencia {Projeto}`
            }
        }, {
            html: '<i class="fa fa-weight-hanging"></i>',
            bind: {
                cls: `checklist-conferencia {Prumo}`
            }
        }, {
            html: '<i class="fa fa-grip-lines"></i>',
            bind: {
                cls: `checklist-conferencia {Nivel}`
            }
        }],
        hidden: true
    }, {
        layout: 'hbox',
        cls: 'checklist-detail-action-bar',
        style: 'width: -webkit-fill-available;',
        docked: 'bottom',
        margin: '0 0 10 0',
        height: 36,
        defaults: {
            xtype: 'button',
            width: '100%',
            flex: 1
        },
        items: [{
            text: 'Aprovar',
            iconCls: 'x-fa fa-check',
            // handler: 'onClickAprovar',
            bind: {
                hidden: '{!showAprovar}'
            }
        }, {
            text: 'Rejeitar',
            iconCls: 'x-fa fa-times',
            // handler: 'onClickRejeitar',
            bind: {
                hidden: '{!showRejeitar}'
            }
        }],
        hidden: true
    }],

    // listeners: {
    //     painted: 'onActivate'
    // }
});