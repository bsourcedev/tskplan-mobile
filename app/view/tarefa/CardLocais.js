/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.tarefa.CardLocais', {
    extend: 'Ext.Panel',
    xtype: 'tarefa-card-locais',
    layout: 'vbox',
    requires: [],

    // controller: 'tarefa-tela',
    // viewModel: 'tarefa-tela',

    // style: 'background-color: whitesmoke;',
    // bodyStyle: 'background-color: whitesmoke;',

    cls: 'card',
    shadow: true,
    margin: '0 0 20 0',
    defaults: {
        flex: 1,
        xtype: 'label'
    },

    items: [{
        html: 'Ordens de Serviço',
        cls: 'card-titulo'
    }, {
        cls: 'card-descricao',
        html: '<spam class="tarefa-card-locais-andamento">' +
            '<div><i class="fas fa-circle local-status-icon" tarefa-status="Aberta"></i> 0</div>' +
            '<div><i class="fas fa-circle local-status-icon" tarefa-status="Andamento"></i> 0</div>' +
            '<div><i class="fas fa-circle local-status-icon" tarefa-status="Concluida"></i> 0</div>' +
            '</spam>',
        bind: {
            html: '<spam class="tarefa-card-locais-andamento">' +
                '<div><i class="fas fa-circle local-status-icon" tarefa-status="Aberta"></i> {Record.OSAberta}</div>' +
                '<div><i class="fas fa-circle local-status-icon" tarefa-status="Andamento"></i> {Record.OSAndamento}</div>' +
                '<div><i class="fas fa-circle local-status-icon" tarefa-status="Concluida"></i> {Record.OSConcluidas}</div>' +
                '</spam>'
        }
    }, {
        bind: {
            html: '<div class="barra-progresso"><div class="barra-progresso-valor" style="width:{progressoLocais}%"></div></div>'
        },
        margin: '20 0 0 0'
    }, {
        xtype: 'button',
        html: 'Editar locais',
        docked: 'bottom',
        margin: '15 0 0',
        handler: 'onClickLocais'
    }]

    // items: [{
    //     items: ,
    //     hideAnimation: 'slideOut',
    //     bind: {
    //         hidden: '{Editor}'
    //     }
    // }],

    // listeners: {
    //     painted: 'onActivate'
    // }
});