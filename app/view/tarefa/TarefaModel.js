/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.tarefa.TarefaModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.tarefa-tela',

    data: {
        Editor: false,
        Creation: true,
        Loading: false,
        Record: {
            StatusId: 1,
            Obra: ''
        },
        hideGallery: true,
    },

    formulas: {
        status: function (get) {
            var status = get('Record.StatusId');

            return status == 0 ? 'Em Aberto' : status == 1 ? 'Em Andamento' : 'Concluido';
        },
        editObra: function (get) {
            return get('Creation') || (!get('Creation') && get('Record') && !get('Record.ObraId')) ? true : false;
        },
        hideObservacao: function (get) {
            return !get('Record.Obsercacao') ? true : false;
        },
        hideCancelar: function (get) {
            return get('Creation') ? true : false;
        },
        progressoLocais: function (get) {
            var aberta = get('Record.OSAberta'),
                andamento = get('Record.OSAndamento'),
                concluida = get('Record.OSConcluidas'),
                total = (aberta + andamento + concluida) * 2,
                valor = (concluida * 2) + andamento,
                porcentagem = valor / total * 100;

            return porcentagem ? porcentagem : 0;
        }

    },

    stores: {
        Store: {
            type: 'tarefa-tela',
            listeners: {
                // load: 'onStoreLoad'
            }
        },
        UnidadeMedida: {
            type: 'unidademedida-global'
        },
        ModeloServico: {
            type: 'modeloservico-global'
        },
        Obra: {
            type: 'obra-global'
        },
        Arquivos: {
            type: 'tarefa-arquivos'
        }
    }
});