/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.main.NavigationBarController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.navigation-bar',

    init: function () {
        var me = this,
            view = me.getView(),
            references = view.getReferences();

    },

    onVoltarClick: function () {
        var me = this,
            view = me.getView(),
            references = view.getReferences();

        ScreenManager.voltar();
    },

    exibirVoltar: function (exibir) {
        var me = this,
            view = me.getView(),
            references = view.getReferences(),
            viewModel = me.getViewModel();

        viewModel.set('exibirVoltar', exibir);
    }
});