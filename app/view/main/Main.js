/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 */
Ext.define('TskPlan.Mobile.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.MessageBox',
        'Ext.layout.Fit',
        'Ext.data.identifier.Uuid',
        'Ext.Toast'
    ],

    controller: 'main',
    viewModel: 'main',

    defaults: {
        tab: {
            iconAlign: 'top',
            cls: 'app-main-tab',
            layout: 'fit',
            height: 50,
            listeners: {
                tap: 'onTapMenu'
            }
        }
    },

    cls:'app-main',

    tabBarPosition: 'bottom',

    items: [{
        sessao: 'Inicio',
        extraData:{
            sessao: 'Inicio',
        },
        // title: 'Inicio',
        iconCls: 'x-fas fa-home',
        items: [{
             xtype: 'home-tela'
        }]
    }, {
        sessao: 'Notificações',
        iconCls: 'x-fas fa-bell',
        // badgeText: '7',//Todo: Incluir qtd de notificações
        items: [{
            xtype: 'notificacoes-tela'
        }]
    }, {
        sessao: 'Menu',
        xtype: 'panel',
        reference: 'menuTab',
        layout: 'card',
        iconCls: 'x-fas fa-bars',
        items: [{
            xtype: 'menu-tela'
        }]
    }, {
        xtype: 'navigation-bar',
        reference: 'navigationBar',
        docked: 'top',
        layout: 'hbox',
    }]
});