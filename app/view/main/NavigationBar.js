/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.main.NavigationBar', {
    extend: 'Ext.Panel',
    xtype: 'navigation-bar',
    controller: 'navigation-bar',
    viewModel: 'navigation-bar',
    requires: [],
    bodyCls: 'navigation-bar',
    width: '100%',
    layout: 'hbox',
    items: [{
        xtype: 'button',
        reference: 'btnVoltar',
        iconCls: 'x-fas fa-chevron-left',
        ui: 'action',
        handler: 'onVoltarClick',
        bind: {
            hidden: '{!exibirVoltar}'
        }
    }, {
        xtype: 'component',
        width: 36,
        bind: {
            hidden: '{exibirVoltar}'
        }
    }, {
        xtype: 'label',
        width: '100%',
        flex: 1,
        bind: {
            html: '<div class="navigation-bar-screen-title">{tela}</div>',
        }
    }]
});