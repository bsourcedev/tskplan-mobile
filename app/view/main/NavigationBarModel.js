/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.main.NavigationBarModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.navigation-bar',

    data: {
        tela: 'Nome da Tela',
        exibirVoltar: false
    }

});