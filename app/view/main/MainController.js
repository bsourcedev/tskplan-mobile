/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.main',

    init: function () {
        var me = this,
            view = me.getView(),
            references = view.getReferences();

        me.setGlobalReferences();

        me.configureWorkers();
    },

    setGlobalReferences: function () {
        var me = this,
            view = me.getView(),
            references = view.getReferences();

        window.navigationBar = references.navigationBar;
        window.mainView = view;
        window.mainTabView = references.menuTab;
        window.mainTabMenu = mainTabView.items.items[0];
        window.mainController = me;
        window.navigationTabs = view._tabBar;
        // window.bearerToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjUzIiwibmFtZWlkIjoiQlNvdXJjZVgiLCJuYmYiOjE1OTA1MDQ4OTMsImV4cCI6MjUzNzIzMjg5MywiaWF0IjoxNTkwNTA0ODkzfQ.axGrOWhQ7Ed79yTMPzrPrkar6IcPfZLfk7X5iKc5PO4'
    },

    onItemSelected: function (sender, record) {
        Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },

    ativarMenu: function () {
        var me = this,
            view = me.getView(),
            references = view.getReferences();

        if (view.getActiveItem() != references.menuTab) {

            //<debug>
            console.log('Ativando tab [Menu]')
            //</debug>
            view.setActiveItem(view.items.findIndex('id', references.menuTab.getId()));
        }
    },

    ativarTela: function (id) {
        var me = this,
            view = me.getView(),
            references = view.getReferences();

        view.setActiveItem(view.items.findIndex('id', id));
        references.menuTab.setActiveItem(0);
    },

    onTapMenu: function () {
        var me = this;

        //<debug>
        //Deixar aqui para alterações futuras casa haja necessidade de alguma ação ao trocar de menu
        console.log('Menu pressionado');
        //</debug>

        ScreenManager.limparTelasAbertas();
    },

    configureWorkers: function () {
        var me = this;

        me.configureMessageWorker();
        // me.configureServiceWorker();
        me.configureServiceWorker2();

        //Registro da session do socket de mensagens
        MessageLib.registerSession(window.TskPlanManager.newGuid());
        console.log('%c Alterar para pegar o token da sessao','color:#ff47fa');

        //Atualiza canais de mensagens abertos em outros dispositivos
        MessageLib.loadUserChannels();
    },

    configureMessageWorker: function () {
        var me = this;
               
        //Inicia o Web worker do socket de mensagem
        window.messageWorker = new Worker('MessageWorker.js');

        window.messageWorker.onmessage = function (event) {

            if (event.data.method == 'messageReceived')
                MessageLib.receiveMessage(event.data.data);
        }
    },

    configureServiceWorker: function () {
        var me = this;

        window.GlobalSWMessageEventListener = function (event) {
            var response = event.data;

            console.log('[app] Data received from sw: ', response);

            if (response.method == 'messageReceived')
                MessageLib.receiveMessage(response.data)
            else if (response.method == 'messageBuffer')
                MessageLib.receiveMessageBuffer(response.data)
            else if (response.method == 'userChannels')
                MessageLib.loadUserChannelsCallback(response.data)

        }

        navigator.serviceWorker.register('/sw.js')
        .catch(function(err) {
            //<debug>
            console.error('ServiceWorker failed to register...');
            console.error(err.message);
            //</debug>
        });;
        
        navigator.serviceWorker.ready.then(function(swReg) {
            window.syncReg  = swReg;
            // messaging.useServiceWorker(swReg);
            //return syncReg.sync.register('start');
            navigator.serviceWorker.addEventListener('message', window.GlobalSWMessageEventListener);
        });        
    },

    configureServiceWorker2: function () {
        var me = this;

        window.GlobalSWMessageEventListener = function (event) {
            var response = event.data;

            console.log('[app] Data received from sw: ', response);

            if (response.method == 'messageReceived')
                MessageLib.receiveMessage(response.data)
            else if (response.method == 'messageBuffer')
                MessageLib.receiveMessageBuffer(response.data)
            else if (response.method == 'userChannels')
                MessageLib.loadUserChannelsCallback(response.data)

        }
              
        //Inicia o Web worker do socket de mensagem
        window.serviceWorker = new Worker('sw.js');

        window.serviceWorker.onmessage = window.GlobalSWMessageEventListener;

        // navigator.serviceWorker.register('/sw.js')
        // .catch(function(err) {
        //     //<debug>
        //     console.error('ServiceWorker failed to register...');
        //     console.error(err.message);
        //     //</debug>
        // });;
        
        // navigator.serviceWorker.ready.then(function(swReg) {
        //     window.syncReg  = swReg;
        //     // messaging.useServiceWorker(swReg);
        //     //return syncReg.sync.register('start');
        //     navigator.serviceWorker.addEventListener('message', window.GlobalSWMessageEventListener);
        // });        
    },
});