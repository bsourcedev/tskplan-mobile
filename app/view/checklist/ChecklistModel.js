/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.checklist.ChecklistModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.checklist-tela',

    data: {
        ObraId: null,
        Obra: '',
        LocalId: null,
        Local: '',
        TarefaId: null,
        Tarefa: '',
        Sigla: '',
        Checklists: []
    },

    formulas: {
        BreadCrumb: function (get) {
            var local = get('Local'),
                tarefa = get('Tarefa');

            return `${get('Obra')}${(local?(`<br>${local}`):'')}${(tarefa?(`<br>${tarefa}`):'')}`;
        },
        Pendentes: function (get) {
            var checklists = get('Checklists').filter(checklist => checklist.StatusId == -1);

            //<debug>
            console.groupCollapsed(`Checklists Pendentes [${checklists.length}]`);
            console.log(checklists);
            console.groupEnd();
            //</debug>  

            return checklists;
        },
        Aprovados: function (get) {
            var checklists = get('Checklists').filter(checklist => checklist.StatusId == 1);

            //<debug>
            console.groupCollapsed(`Checklists Aprovados [${checklists.length}]`);
            console.log(checklists);
            console.groupEnd();
            //</debug>  

            return checklists;
        },
        Rejeitados: function (get) {
            var checklists = get('Checklists').filter(checklist => checklist.StatusId == 0);

            //<debug>
            console.groupCollapsed(`Checklists Rejeitados [${checklists.length}]`);
            console.log(checklists);
            console.groupEnd();
            //</debug>  

            return checklists;
        },
    },

    stores: {
        Store: {
            type: 'checklist-tela',
        }
    }
});