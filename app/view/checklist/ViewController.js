/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.checklist.ViewController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.checklist-view',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();

        viewModel.set({
            ObraId: view.ObraId ? view.ObraId : null,
            Obra: view.Obra ? view.Obra : '',
            Sigla: view.Sigla ? view.Sigla : '',
            LocalId: view.LocalId ? view.LocalId : null,
            Local: view.Local ? view.Local : '',
            TarefaId: view.TarefaId ? view.TarefaId : null,
            Tarefa: view.Tarefa ? view.Tarefa : '',
            Step: view.ObraId ? 2 : 1,
        });
    },

    onClickObra: function () {
        var me = this;

        me.selecionarObra();
    },

    onClickLocal: function () {
        var me = this;

        me.selecionarLocal();
    },

    onClickTarefa: function () {
        var me = this;

        me.selecionarTarefa();
    },

    onClickFiltrar: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            references = view.getReferences(),
            checklistTela = references.checklistTela,
            checklistTelaController = checklistTela.getController(),
            checklistTelaViewModel = checklistTela.getViewModel();

        if (me.validar()) {
            checklistTelaViewModel.set({
                ObraId: viewModel.get('ObraId'),
                Obra: viewModel.get('Obra'),
                LocalId: viewModel.get('LocalId'),
                Local: viewModel.get('Local'),
                TarefaId: viewModel.get('TarefaId'),
                Tarefa: viewModel.get('Tarefa'),
                Sigla: viewModel.get('Sigla'),
            });

            // checklistTelaController.filtrar();
            view.setActiveItem(1);
        }
    },

    selecionarObra: function () {
        var me = this,
            view = me.getView(),
            tabView = view.up(),
            component = Ext.create('TskPlan.Mobile.view.obra.Obra');

        component.on({
            scope: me,
            obraSelecionada: me.onObraSelecionada
        });

        ScreenManager.abrirTela({
            screen: component,
            anterior: view,
            title: 'Selecionar Obra',
            voltar: false
        });
    },

    onObraSelecionada: function (obra) {
        var me = this,
            viewModel = me.getViewModel();

        viewModel.set({
            ObraId: obra.ObraId,
            Obra: obra.Obra,
            Sigla: obra.Sigla,
            Tarefa: '',
            TarefaId: null,
            Local: '',
            LocalId: null,
            Step: 2
        });
    },

    selecionarLocal: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            tabView = view.up(),
            component;

        if (!viewModel.get('ObraId')) {
            Ext.Msg.alert('Atenção', 'Selecione uma Obra para visualizar seus locais.');
            return false;
        }

        component = Ext.create('TskPlan.Mobile.view.local.Seletor', {
            obraId: viewModel.get('ObraId')
        });

        component.on({
            scope: me,
            localSelecionado: me.onLocalSelecionado
        });

        ScreenManager.abrirTela({
            screen: component,
            anterior: view,
            title: 'Selecionar Local',
            voltar: false
        });
    },

    onLocalSelecionado: function (local) {
        var me = this,
            viewModel = me.getViewModel();

        viewModel.set({
            LocalId: local.ObraLocalId,
            Local: local.Nome,
            Tarefa: '',
            TarefaId: null,
            Step: 3
        });
    },

    selecionarTarefa: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            tabView = view.up(),
            component;

        if (!viewModel.get('ObraId')) {
            Ext.Msg.alert('Atenção', 'Selecione uma Obra para visualizar seus locais.');
            return false;
        }

        if (!viewModel.get('LocalId')) {
            Ext.Msg.alert('Atenção', 'Selecione um Local para visualizar suas tarefas.');
            return false;
        }

        component = Ext.create('TskPlan.Mobile.view.tarefa.Seletor', {
            obraId: viewModel.get('ObraId'),
            obraLocalId: viewModel.get('LocalId')
        });

        component.on({
            scope: me,
            tarefaSelecionada: me.onTarefaSelecionada
        });

        ScreenManager.abrirTela({
            screen: component,
            anterior: view,
            title: 'Selecionar Tarefa',
            voltar: false
        });
    },

    onTarefaSelecionada: function (tarefa) {
        var me = this,
            viewModel = me.getViewModel();

        viewModel.set({
            TarefaId: tarefa.TarefaId,
            Tarefa: tarefa.Nome,
            Step: 4
        });

        if (viewModel.get('filtroInicial')) {
            viewModel.set('filtroInicial', false);

            me.onClickFiltrar();
        }
    },


    validar: function () {
        var me = this,
            viewModel = me.getViewModel(),
            msg = '',
            valido = true;


        if (!viewModel.get('ObraId')) {
            valido = false;
            msg = 'Selecione uma obra para continuar.';
        } else if (!viewModel.get('LocalId')) {
            valido = false;
            msg = 'Selecione uma local para continuar.';
        } else if (!viewModel.get('TarefaId')) {
            valido = false;
            msg = 'Selecione uma tarefa para continuar.';
        }

        if (!valido)
            Ext.Msg.alert('Atenção', msg);

        return valido;
    }
});