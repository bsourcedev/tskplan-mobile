/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.checklist.View', {
    extend: 'Ext.Panel',
    layout: 'card',
    xtype: 'checklist-view',
    requires: [],

    controller: 'checklist-view',
    viewModel: 'checklist-view',

    items: [{
        xtype: 'panel',
        alwaysOnTop: -1,
        header: false,
        flex: 1,
        bodyPadding: 10,
        bodyCls: 'tela-background',
        layout: 'vbox',
        defaults: {
            xtype: 'button',
            width: '100%',
            ui: 'raised',
            style: 'text-align:center'
        },
        items: [ {
            xtype: 'label',
            cls: 'filtro-titulo',
            html: 'Obra'
        }, {
            xtype: 'container',
            ui: '',
            style: '',
            layout: 'hbox',
            items: [{
                xtype: 'component',
                width: 36,
                bind: {
                    html: '<div class="filtro-step {stepObra}"><i class="fa fa-check"></i><i class="fa fa-circle"></i></div>'
                }
            }, {
                xtype: 'button',
                width: '100%',
                flex: 1,
                style: 'text-align:left',
                handler: 'onClickObra',
                margin: '5 5 15 10',
                bind: {
                    text: '{lblObra}',
                    ui: '{styleObra}',
                }
            }]
        }, {
            xtype: 'label',
            cls: 'filtro-titulo',
            html: 'Local'
        }, {
            xtype: 'container',
            ui: '',
            style: '',
            layout: 'hbox',
            items: [{
                xtype: 'component',
                width: 36,
                bind: {
                    html: '<div class="filtro-step {stepLocal}"><i class="fa fa-check"></i><i class="fa fa-circle"></i></div>'
                }
            }, {
                xtype: 'button',
                width: '100%',
                flex: 1,
                style: 'text-align:left',
                handler: 'onClickLocal',
                margin: '5 5 15 10',
                bind: {
                    text: '{lblLocal}',
                    ui: '{styleLocal}'
                }
            }]
        }, {
            xtype: 'label',
            cls: 'filtro-titulo',
            html: 'Tarefas'
        }, {
            xtype: 'container',
            ui: '',
            style: '',
            layout: 'hbox',
            items: [{
                xtype: 'component',
                width: 36,
                bind: {
                    html: '<div class="filtro-step {stepTarefa}"><i class="fa fa-check"></i><i class="fa fa-circle"></i></div>'
                }
            }, {
                xtype: 'button',
                width: '100%',
                flex: 1,
                margin: '5 5 15 10',
                style: 'text-align:left',
                handler: 'onClickTarefa',
                bind: {
                    text: '{lblTarefa}',
                    ui: '{styleTarefa}'
                }
            }]
        }, {
            xtype: 'container',
            flex: 1
        }, {
            text: 'Filtrar',
            margin: '0 0 10 0',
            ui: 'action raised',
            handler: 'onClickFiltrar'
        }]
    }, {
        xtype: 'checklist-tela',
        reference: 'checklistTela'
    }],


    // listeners: {
    //     painted: 'onActivate'
    // }
});