/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.checklist.ChecklistDetail', {
    extend: 'Ext.Panel',
    xtype: 'checklistDetail-tela',
    layout: 'vbox',
    requires: [
        'TskPlan.Mobile.components.galeria.Galeria'
    ],

    controller: 'checklistDetail-tela',
    viewModel: 'checklistDetail-tela',

    bodyPadding: '10 20 40 20',
    scrollable: 'y',
    defaults: {
        xtype: 'panel',
        cls: 'card',
        shadow: true,
        margin: '0 0 20 0',
        defaults: {
            xtype: 'label'
        }
    },

    bodyCls: 'checklistDetail-tela',

    style: 'background-color: whitesmoke;',
    bodyStyle: 'background-color: whitesmoke;',

    items: [{
        xtype: 'component',
        collapsed: false,
        shadow: false,
        margin: 0,
        cls: 'checklist-breadcrumb',
        docked: 'top',
        bind: {
            html: '{BreadCrumb}'
        }
    }, {
        margin: '0 -10 20 -10',
        cls: 'card checklist-detail-titulo',
        height: 43,
        bind: {
            html: `<div class="status-icon-{StatusIcon}"></div>&nbsp;&nbsp;<spam class="checklist-detail-status">{Status}</spam>`
        }
    }, {
        items: [{
            cls: 'card-descricao',
            bind: {
                html: '{htmlMedia}'
            }
        }],
        bind: {
            hidden: '{!showAprovado}'
        }
    }, {
        items: [{
            html: 'Motivo da rejeição',
            cls: 'card-titulo'
        }, {
            cls: 'card-descricao',
            bind: {
                html: '{Record.Motivo}'
            }
        }, {
            cls: 'card-descricao',
            bind: {
                html: '{htmlMedia}',
                hidden: '{!showImagemReprovado}'
            }
        }],
        bind: {
            hidden: '{!showMotivo}'
        }
    }, {
        items: [{
            html: 'Item',
            cls: 'card-titulo'
        }, {
            cls: 'card-descricao',
            bind: {
                html: '{Record.Descricao}'
            }
        }]
    }, {
        items: [{
            html: 'Quanto Conferir',
            cls: 'card-titulo'
        }, {
            cls: 'card-descricao',
            bind: {
                html: '{Record.QuantoConferir}'
            }
        }]
    }, {
        items: [{
            html: 'Aceitar se',
            cls: 'card-titulo'
        }, {
            cls: 'card-descricao',
            bind: {
                html: '{Record.Aceitacao}'
            }
        }]
    }, {
        items: [{
            html: 'Arquivos do checklist',
            margin: '0 0 10 0',
            cls: 'card-titulo'
        }, {
            xtype: 'galeria',
            reference: 'galeria',
            bind: {
                hidden: '{hideGallery}'
            }
        }, {
            cls: 'card-descricao',
            bind: {
                html: '{hideGalleryText}',
                hidden: '{!hideGallery}'
            }
        }]
    }, {
        items: [{
            html: 'Arquivos da tarefa',
            margin: '0 0 10 0',
            cls: 'card-titulo'
        }, {
            xtype: 'galeria',
            reference: 'galeriaTarefa',
            bind: {
                hidden: '{hideGalleryTarefa}'
            }
        }, {
            cls: 'card-descricao',
            bind: {
                html: '{hideGalleryTarefaText}',
                hidden: '{!hideGalleryTarefa}'
            }
        }]
    }, {
        layout: 'hbox',
        defaults: {
            flex: 1
        },
        items: [{
            html: '<i class="fa fa-eye"></i>',
            bind: {
                cls: `checklist-conferencia {Visual}`
            }
        }, {
            html: '<i class="fa fa-tape"></i>',
            bind: {
                cls: `checklist-conferencia {Trena}`
            }
        }, {
            html: '<i class="fa fa-file invoice"></i>',
            bind: {
                cls: `checklist-conferencia {Projeto}`
            }
        }, {
            html: '<i class="fa fa-weight-hanging"></i>',
            bind: {
                cls: `checklist-conferencia {Prumo}`
            }
        }, {
            html: '<i class="fa fa-grip-lines"></i>',
            bind: {
                cls: `checklist-conferencia {Nivel}`
            }
        }]
    }, {
        layout: 'hbox',
        cls: 'checklist-detail-action-bar',
        style: 'width: -webkit-fill-available;',
        docked: 'bottom',
        margin: '00 10 0',
        height: 36,
        defaults: {
            xtype: 'button',
            width: '100%',
            flex: 1
        },
        items: [{
            text: 'Aprovar',
            iconCls: 'x-fa fa-check',
            handler: 'onClickAprovar',
            bind: {
                hidden: '{!showAprovar}'
            }
        }, {
            text: 'Rejeitar',
            iconCls: 'x-fa fa-times',
            handler: 'onClickRejeitar',
            bind: {
                hidden: '{!showRejeitar}'
            }
        }]
    }],

    // listeners: {
    //     painted: 'onActivate'
    // }
});