/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.checklist.ChecklistDetailModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.checklistDetail-tela',

    data: {
        ObraId: 0,
        Obra: '',
        Sigla: '',
        TarefaId: 0,
        Tarefa: '',
        LocalId: 0,
        Local: '',
        PreData: {
            StatusId: 0,
            Titulo: '',
            Descricao: '',
        },
        Item: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras imperdiet ante et maximus varius. Cras posuere quam odio, quis condimentum urna faucibus nec. Maecenas nec tellus gravida, aliquam justo non, finibus nisi. Ut nibh ipsum, tempor at commodo quis, suscipit id turpis. Quisque tempus lectus sit amet sodales blandit. ',
        QuantoConferir: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras imperdiet ante et maximus varius. Cras posuere quam odio, quis condimentum urna faucibus nec. Maecenas nec tellus gravida.',
        AceitarSe: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras imperdiet ante et maximus varius. Cras posuere quam odio, quis condimentum urna faucibus nec. Maecenas nec tellus gravida, aliquam justo non, finibus nisi. Ut nibh ipsum, tempor at commodo quis, suscipit id turpis. Nunc iaculis urna purus, a molestie justo porttitor sed.',
        SheetMotivo: '',
        Aprovar: false,
        ImageURI: '',
        Record: null,
        MediaLocal: '',
        hideGallery: true,
        hideGalleryText:'',
        hideGalleryTarefa: true,
        hideGalleryTarefaText:'',
    },

    formulas: {
        StatusId: function (get) {
            return get('Record') ? get('Record.StatusId') : get('PreData.StatusId');
        },
        Status: function (get) {
            var status = get('StatusId');

            return status == -1 ? 'Pendente' : status == 1 ? 'Aprovado' : 'Rejeitado';
        },
        StatusIcon: function (get) {
            var status = get('StatusId');

            return status == -1 ? 'pendente' : status == 1 ? 'aprovado' : 'nao-aprovado';
        },
        Visual: function (get) {
            return get('Record.Visual') ? 'ativo' : '';
        },
        Trena: function (get) {
            return get('Record.Trena') ? 'ativo' : '';
        },
        Projeto: function (get) {
            return get('Record.Projeto') ? 'ativo' : '';
        },
        Prumo: function (get) {
            return get('Record.Prumo') ? 'ativo' : '';
        },
        Nivel: function (get) {
            return get('Record.Nivel') ? 'ativo' : '';
        },
        BreadCrumb: function (get) {
            var local = get('Local'),
                tarefa = get('Tarefa');

            return `${get('Obra')}${(local?(`<br>${local}`):'')}${(tarefa?(`<br>${tarefa}`):'')}`;
        },
        getSheetTitle: function (get) {
            return get('Aprovar') ? 'Aprovar' : 'Rejeitar';
        },
        showAprovar: function (get) {
            return get('StatusId') == 1 ? false : true;
        },
        showRejeitar: function (get) {
            return get('StatusId') == 0 ? false : true;
        },
        showMotivo: function (get) {
            return get('Record.Motivo') && get('StatusId') == 0 ? true : false;
        },
        showImagemReprovado: function (get) {
            return get('Record.MotivoImagemURL') || get('MediaLocal') || (get('Record.MotivoImagemGuid') && MediaManager.getImagemLocal(get('Record.MotivoImagemGuid'))) ? true : false;
        },
        showAprovado: function (get) {
            return get('StatusId') == 1 && (get('Record.MotivoImagemURL') || get('MediaLocal') || (get('Record.MotivoImagemGuid') && MediaManager.getImagemLocal(get('Record.MotivoImagemGuid')))) ? true : false;
        },
        MediaMotivo: function (get) {
            var media = get('MediaLocal') || get('Record.MotivoImagemURL') || (get('Record.MotivoImagemGuid') && MediaManager.getImagemLocal(get('Record.MotivoImagemGuid')));

            if (!media)
                console.log('Sem imagens para exibir');

            return media;
        },
        htmlMedia: function (get) {
            var media = get('MediaMotivo');
            if (media)
                return `<img style="width: 100%;" src="${media}" onerror="this.style.display='none';this.onerror=null;this.src=\'resources/cordova-no-image.png\';this.parentElement.classList.add(\'image-not-avaliable\');">`
        }
    },

    stores: {
        Store: {
            type: 'checklist-dados',
            listeners: {
                load: 'onStoreLoad'
            }
        },
        StoreArquivos: {
            type: 'checklist-arquivos',
            listeners: {
                load: 'onStoreArquivosLoad'
            }
        },
        StoreArquivosTarefa: {
            type: 'tarefa-arquivos',
            listeners: {
                load: 'onStoreArquivosTarefaLoad'
            }
        }
    }
});