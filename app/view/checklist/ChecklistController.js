/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.checklist.ChecklistController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.checklist-tela',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();

        viewModel.set({
            ObraId: view.ObraId ? view.ObraId : null,
            Obra: view.Obra ? view.Obra : '',
            Sigla: view.Sigla ? view.Sigla : '',
            LocalId: view.LocalId ? view.LocalId : null,
            Local: view.Local ? view.Local : '',
            TarefaId: view.TarefaId ? view.TarefaId : null,
            Tarefa: view.Tarefa ? view.Tarefa : '',
        });

    },

    onActivate: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            store = viewModel.get('Store');

        store.getProxy().extraParams = {
            ObraId: viewModel.get('ObraId') ? viewModel.get('ObraId') : 0,
            ObraLocalId: viewModel.get('LocalId') ? viewModel.get('LocalId') : 0,
            TarefaId: viewModel.get('TarefaId') ? viewModel.get('TarefaId') : 0
        };

        view.mask('Carregando dados...');

        store.load({
            callback: function () {
                me.filtrar(store.data.items);
            }
        });
    },

    filtrar: function (checklists) {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();

        viewModel.set('Checklists', checklists.map(checklist => checklist.data));

        //<debug>
        console.groupCollapsed(`Checklists Carregados [${checklists.length}]`);
        console.log(checklists);
        console.groupEnd();
        //</debug>  

        view.unmask();
    },

    onHeaderClickExpand: function (event, component) {
        var cmp = Ext.getCmp(component.closest('.x-panel').id);
        if (cmp) cmp.expand();
    },

    onClickFiltrar: function () {
        var me = this,
            view = me.getView(),
            cardLayout = view.up();

        cardLayout.setActiveItem(0);
    }
});