/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.checklist.ViewModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.checklist-view',

    data: {
        ObraId: null,
        Obra: '',
        LocalId: null,
        Local: '',
        TarefaId: null,
        Tarefa: '',
        Sigla: '',
        Step: 1,
        filtroInicial: true
    },

    formulas: {
        stepObra: function (get) {
            var step = get('Step');

            return step == 1 ? 'actual' : step > 1 ? 'done' : '';
        },
        stepLocal: function (get) {
            var step = get('Step');

            return step == 2 ? 'actual' : step > 2 ? 'done' : '';
        },
        stepTarefa: function (get) {
            var step = get('Step');

            return step == 3 ? 'actual' : step > 3 ? 'done' : '';
        },
        styleObra: function (get) {
            var lbl = get('Obra');

            return lbl ? 'raised confirm' : get('Step') == 1 ? 'raised action' : 'raised';
        },

        styleLocal: function (get) {
            var lbl = get('Local');

            return lbl ? 'raised confirm' : get('Step') == 2 ? 'raised action' : 'raised';
        },

        styleTarefa: function (get) {
            var lbl = get('Tarefa');

            return lbl ? 'raised confirm' : get('Step') == 3 ? 'raised action' : 'raised';
        },
        lblObra: function (get) {
            var lbl = get('Obra');

            return lbl ? lbl : 'Selecionar';
        },
        lblLocal: function (get) {
            var lbl = get('Local');

            return lbl ? lbl : 'Selecionar';
        },
        lblTarefa: function (get) {
            var lbl = get('Tarefa');

            return lbl ? lbl : 'Selecionar';
        },
    },

    stores: {
        Store: {
            type: 'checklist-tela',
        }
    }
});