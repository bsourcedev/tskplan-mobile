/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.checklist.CardModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.checklist-card',

    data: {
        data:{
            checklistId:0,
            titulo:'',
            descricao:''
        }        
    },

    stores: {
        Store: {
            type: 'checklist-tela',
        }
    }
});