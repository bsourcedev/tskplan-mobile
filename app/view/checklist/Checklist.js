/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.checklist.Checklist', {
    extend: 'Ext.panel.Accordion',
    xtype: 'checklist-tela',
    requires: [],

    controller: 'checklist-tela',
    viewModel: 'checklist-tela',

    cls: 'checklist-tela',

    expandedFirst: true,

    defaults: {
        xtype: 'panel',
        collapsible: true,
        collapsed: true,
        flex: 1,
        bodyPadding: '10 20',
        scrollable: 'y',
        header: {
            cls: 'checklist-group-header',
            listeners: {
                element: 'element',
                tap: 'onHeaderClickExpand'
            }
        },
        defaults: {
            xtype: 'checklist-card',
        }
    },

    items: [{
        xtype: 'container',
        docked: 'top',
        layout: 'hbox',
        cls: 'checklist-breadcrumb',
        items: [{
            collapsible: false,
            xtype: 'component',
            padding: '0 0 0 0',
            style: 'margin-top: 6px;',
            bind: {
                html: '{BreadCrumb}'
            },
            flex: 1
        }, {
            xtype: 'button',
            minHeight: 32,
            iconCls: 'x-fa fa-filter button-medium-icon checklist-button-icon-color',
            style: 'color:white',
            text: 'filtros',
            ui: 'icon',
            margin: 0,
            handler: 'onClickFiltrar',
        }]
    }, {
        collapsed: false,
        bind: {
            title: '<div class="status-icon-pendente"></div> Pendentes <spam class="checklist-titulo-quantidade">{Pendentes.length}</spam>',
            items: '{Pendentes}'
        }
    }, {
        bind: {
            title: '<div class="status-icon-nao-aprovado"></div> Rejeitados <spam class="checklist-titulo-quantidade">{Rejeitados.length}</spam>',
            items: '{Rejeitados}'
        }
    }, {
        bind: {
            title: '<div class="status-icon-aprovado"></div> Aprovados <spam class="checklist-titulo-quantidade">{Aprovados.length}</spam>',
            items: '{Aprovados}'
        }
    }],

    listeners: {
        painted: 'onActivate'
    }
});