/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.checklist.ChecklistDetailController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.checklistDetail-tela',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            viewData = view.extraData;

        viewModel.set({
            ObraId: viewData.ObraId ? viewData.ObraId : null,
            Obra: viewData.Obra ? viewData.Obra : '',
            Sigla: viewData.Sigla ? viewData.Sigla : '',
            LocalId: viewData.LocalId ? viewData.LocalId : null,
            Local: viewData.Local ? viewData.Local : '',
            TarefaId: viewData.TarefaId ? viewData.TarefaId : null,
            Tarefa: viewData.Tarefa ? viewData.Tarefa : '',
            PreData: viewData.PreData,
        });

        me.carregarDados();
    },

    createMenu: function () {
        var me = this;

        me.menu = Ext.create({
            xtype: 'actionsheet',
            viewModel: me.getViewModel(),
            displayed: true,
            hideOnMaskTap: false,
            side: 'bottom',
            reveal: false,
            cover: true,
            items: [{
                xtype: 'component',
                cls: 'card-titulo',
                bind: {
                    html: '{getSheetTitle}'
                }
            }, {
                xtype: 'component',
                flex: 1,
                bind: {
                    html: '<img id="minhaImagem" src="{ImageURI}" class="card-no-image checklist-detail-image"></img>', //width:250px;
                }
            }, {
                xtype: 'textfield',
                label: 'Motivo',
                flex: 1,
                bind: {
                    value: '{SheetMotivo}',
                    hidden: '{Aprovar}'
                }
            }, {
                text: 'Tirar Foto',
                scope: me,
                handler: 'tirarFoto'
            }, {
                text: 'Confirmar',
                scope: me,
                handler: 'onClickConfirmar'
            }, {
                text: 'Cancelar',
                scope: me,
                handler: 'onClickCancelar'
            }]
        });
    },

    onClickRejeitar: function () {
        var me = this,
            viewModel = me.getViewModel();

        viewModel.set('Aprovar', false);

        me.clearSheetData();

        me.createMenu();
    },

    clearSheetData: function () {
        var me = this,
            viewModel = me.getViewModel();

        viewModel.set({
            SheetMotivo: '',
            Aprovar: false,
            ImageURI: ''
        });
    },

    onClickAprovar: function () {
        var me = this,
            viewModel = me.getViewModel();

        viewModel.set('Aprovar', true);

        me.createMenu();
    },

    onClickConfirmar: function () {
        var me = this,
            viewModel = me.getViewModel();

        if (viewModel.get('Aprovar')) {
            me.hideMenu();
            me.onAprovar();
        } else {
            if (!viewModel.get('SheetMotivo')) {
                Ext.Msg.alert('Atenção', 'Informe o motivo da rejeição');
            } else {
                me.hideMenu();
                me.onRejeitar();
            }
        }
    },

    salvar: function () {
        var me = this,
            viewModel = me.getViewModel(),
            record = viewModel.get('Record'),
            imagem = viewModel.get('ImageURI');

        record.set('DataInspecao', new Date());

        if (imagem)
            record.set('MotivoImagemGuid', MediaManager.addMedia(imagem));

        record.store.sync();

        me.clearSheetData();
    },

    onAprovar: function () {
        var me = this,
            viewModel = me.getViewModel();

        viewModel.set('Record.Motivo', '');
        viewModel.set('Record.StatusId', 1);

        viewModel.set({
            Motivo: '',
            StatusId: 1,
            Status: 'Aprovado',
            StatusIcon: 'aprovado'
        });

        me.salvar();
    },

    onRejeitar: function () {
        var me = this,
            viewModel = me.getViewModel();

        //<debug>
        console.log(`Checklist reprovado \nMotivo: ${viewModel.get('SheetMotivo')}`);
        //</debug>  

        viewModel.set('Record.Motivo', viewModel.get('SheetMotivo'));
        viewModel.set('Record.StatusId', 0);

        viewModel.set({
            Motivo: viewModel.get('SheetMotivo'),
            StatusId: 0,
            Status: 'Rejeitado',
            StatusIcon: 'nao-aprovado',
        });

        me.salvar();
    },

    onClickCancelar: function () {
        var me = this;

        me.hideMenu();
    },

    hideMenu: function () {
        var me = this,
            menu = me.menu;

        //criar override global para os sheets que removam o viewmodel antes de fechar
        menu.setViewModel();

        menu.close();
    },

    tirarFoto: function () {
        var me = this;

        NavigatorManager.camera.getPicture(Ext.bind(me.onSuccess, me), me.onFail);
    },

    onSuccess: function (imageURI) {
        var me = this,
            viewModel = me.getViewModel(),
            image = document.getElementById('minhaImagem');

        viewModel.set('ImageURI', `${imageURI}`);
        viewModel.set('MediaLocal', `${imageURI}`);
    },

    onFail: function (message) {
        alert('Failed because: ' + message);
    },

    carregarDados: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            store = viewModel.get('Store'),
            storeArquivo = viewModel.get('StoreArquivos'),
            storeArquivosTarefa = viewModel.get('StoreArquivosTarefa'),
            viewData = view.extraData,
            params = [{
                name: 'ObraCronogramaTarefaLocalId',
                value: viewData.ObraCronogramaTarefaLocalId
            }, {
                name: 'ObraCronogramaTarefaCheckListId',
                value: viewData.ObraCronogramaTarefaCheckListId
            }];

        store.setParams(params);

        storeArquivo.setParams(params);

        storeArquivosTarefa.setParams([{
            name: 'tarefaGuid',
            value: viewData.TarefaGuid
        }]);

        view.mask();

        store.load();
        storeArquivo.load();
        storeArquivosTarefa.load();
    },

    onStoreLoad: function (store, _records) {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            records = store.data.items,
            record;

        view.unmask();

        if (records && records.length > 0) {
            record = records[0];

            //<debug>
            console.log('Checklist Aberto');
            console.log(record);
            //</debug>  

            viewModel.set('Record', record);
        }
    },

    onStoreArquivosLoad: function (store, _records) {
        var me = this,
            view = me.getView(),
            references = view.getReferences(),
            viewModel = me.getViewModel(),
            items = store.data.items.map(rec => new Object({
                Nome: rec.get('Nome'),
                Caminho: rec.get('Caminho'),
                Extensao: rec.get('Tipo')
            }));

        if (items.length > 0 && window.navigator.onLine) {
            viewModel.set('hideGallery', false)
            references.galeria.loadFiles(items);
        } else {
            viewModel.set('hideGallery', true)
            viewModel.set('hideGalleryText', !items.length ? 'Nenhum arquivo disponivel' : window.navigator.onLine ? 'Sem conexão com a internet' : 'Nenhum arquivo disponivel')
        }
    },

    onStoreArquivosTarefaLoad: function (store, _records) {
        var me = this,
            view = me.getView(),
            references = view.getReferences(),
            viewModel = me.getViewModel(),
            items = store.data.items.map(rec => new Object({
                Nome: rec.get('Nome'),
                Caminho: rec.get('Caminho'),
                Extensao: rec.get('Tipo')
            }));

        if (items.length > 0 && window.navigator.onLine) {
            viewModel.set('hideGalleryTarefa', false)
            references.galeriaTarefa.loadFiles(items);
        } else {
            viewModel.set('hideGalleryTarefa', true)
            viewModel.set('hideGalleryTarefaText', !items.length ? 'Nenhum arquivo disponivel' : window.navigator.onLine ? 'Sem conexão com a internet' : 'Nenhum arquivo disponivel')
        }
    }

});