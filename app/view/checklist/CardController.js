/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.checklist.CardController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.checklist-card',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();

        viewModel.set('data', {
            checklistId: view.ChecklistId,
            titulo: view.Titulo,
            descricao: view.Descricao
        });

        view.element.on('tap', me.onContainerTap, me);
    },

    onContainerTap: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            checklistTela = view.up('checklist-tela'),
            checklistViewModel = checklistTela.getViewModel();

        me.telaDetalhes = Ext.create('TskPlan.Mobile.view.checklist.ChecklistDetail', {
            extraData: {
                ObraId: checklistViewModel.get('ObraId'),
                Obra: checklistViewModel.get('Obra'),
                Sigla: checklistViewModel.get('Sigla'),
                TarefaId: checklistViewModel.get('TarefaId'),
                Tarefa: checklistViewModel.get('Tarefa'),
                LocalId: checklistViewModel.get('LocalId'),
                Local: checklistViewModel.get('Local'),
                TarefaGuid: view.TarefaGuid,
                ObraCronogramaTarefaLocalId: view.ObraCronogramaTarefaLocalId,
                ObraCronogramaTarefaCheckListId: view.ObraCronogramaTarefaCheckListId,
                PreData: {
                    StatusId: view.StatusId,
                    Titulo: view.Titulo,
                    Descricao: view.Descricao,
                }
            }
        });

        ScreenManager.abrirTela({
            screen: me.telaDetalhes,
            anterior: view.up('checklist-view'),
            title: view.Titulo
        });
    }
});