/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.checklist.Card', {
    extend: 'Ext.Component',
    xtype: 'checklist-card',
    requires: [],

    controller: 'checklist-card',
    viewModel: 'checklist-card',

    margin: '0 0 15 0',

    shadow: true,

    cls:'checklist-card',

    tpl: [
        '<div>',
        '<div class="checklist-card-titulo">{titulo}</div>',
        '<div class="checklist-card-descricao">{descricao}</div>',
        '</div>'
    ],

    bind:{
        data:'{data}'
    },

});