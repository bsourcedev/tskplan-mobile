/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.agendaEntrega.AgendaModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.agenda-tela',

    data: {
        Record: null
    },

    formulas: {},

    stores: {
         StoreProduto: {
             type: 'agenda-entrega-produto',
             listeners: {
                 load: 'onStoreLoad'
             }
         }
    }
});