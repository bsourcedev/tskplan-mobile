/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.agendaEntrega.AgendaController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.agenda-tela',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();

        viewModel.set('Record', view.extraData.record)

        me.loadData();
    },

    loadData: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            store = viewModel.get('StoreProduto');

        store.setParams([{
            name: 'AgendaGuid',
            value: view.extraData.record.getId()
        }]);

        store.load();
    },

    onClickCriarEntrega: function () {
        var me = this;

        //criar uma entrega e deixar a tela de voltar como a agenda e nao a tela de criar entrega

    },

    onStoreLoad: function () {
        var me = this,
            viewModel = me.getViewModel(),
            store = viewModel.get('StoreProduto'),
            view = me.getView(),
            references = view.getReferences();

        store.data.items.forEach(produto => {
            references.pnlProdutos.add({
                cls: 'card-descricao tarefa-item-descricao',
                html: `${produto.get('Nome')} <span class="agenda-produto-quantidade">x${produto.get('Quantidade')}</span>`
            })
        });
    }
});