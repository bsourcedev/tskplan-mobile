/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.agendaEntrega.AgendaEntregaModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.agenda-entrega-tela',

    data: {
        showFiltrar: false
    },

    formulas: {},

    stores: {
        Store: {
            type: 'agenda-entrega-agenda-entrega'
        },
        Calendario: {
            type: 'agenda-entrega-calendario',
            listeners: {
                load: 'onStoreCalendarioLoad'
            }
        }
    }
});