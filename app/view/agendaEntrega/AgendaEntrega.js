/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.agendaEntrega.AgendaEntrega', {
    extend: 'Ext.Panel',
    xtype: 'agenda-entrega-tela',
    layout: 'vbox',
    requires: [],

    controller: 'agenda-entrega-tela',
    viewModel: 'agenda-entrega-tela',

    scrollable: 'y',

    bodyCls: 'tela-tela',

    style: 'background-color: whitesmoke;',
    bodyStyle: 'background-color: whitesmoke;',

    items: [{
            xtype: 'agenda-entrega-calendario',
            reference: 'calendar',
            height: '-webkit-fill-available',
            handler: function (calendar, newDate) {
                var me = this,
                    view = calendar.up('agenda-entrega-tela'),
                    viewController = view.getController();

                viewController.onDateChange();
            },
        },
        {
            xtype: 'component',
            cls: 'divisor',
        }, {
            xtype: 'button',
            // text: '<span/>',
            width:60,
            cls:'agenda-entrega-button',
            iconCls: 'x-fa fa-calendar-alt',
            floating: true,
            renderTo: Ext.getBody(),
            ui: 'alt action raised round shadow',//
            height: 40,
            handler: 'showCalendario',
            bind: {
                style: 'position: absolute; top: 42px; right: 10px;z-index: 999;',
                hidden: '{!showFiltrar}'
            }
        },
        {
            xtype: 'list',
            flex: 1,
            bodyCls: 'tela-background',
            padding: '0 10',
            width: '100%',
            flex: 1,
            bind: {
                store: '{Store}'
            },
            itemCls: 'entrega-card-pedido x-shadow',
            itemContentCls: 'entrega-card-pedido-content',
            itemTpl: [
                '<div class="entrega-card-pedido-id">Pedido #{PedidoCompraId}</div>',
                '<div class="entrega-card-pedido-status">{Status}</div>',
                '<div class="entrega-card-pedido-fornecedor">{Fornecedor}</div>',
                '<div class="entrega-card-pedido-obra">{Obra}</div>',
            ],
            listeners: {
                select: 'onSelect'
            }
        }
    ],

    // listeners: {
    //     painted: 'onActivate'
    // }
});