/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.agendaEntrega.Calendario', {
    extend: 'Ext.panel.Date',
    xtype: 'agenda-entrega-calendario',
    requires: [],

    xtype: 'datepanel',

    // collapsible: 'top',

    // collapsed: false,

    showTodayButton: true,

    selectOnNavigate: true,

    standardButtons: {},

    weekendDays: [],

    dias: [],

    buttons: {
        ok: {
            hidden: true
        },
        cancel: {
            hidden: true
        },
        filtrar: {
            xtype: 'button',
            text: 'Filtrar',
            handler: 'onClickFiltrar'
        }
    },

    //#region Funções

    onClickFiltrar: function () {
        var me = this,
            view = me.up('agenda-entrega-tela'),
            viewController = view.getController();

        viewController.onClickFiltrar();
    },

    carregar: function () {
        var me = this;

        me.items.items[3].refresh();
    },

    addDays: function (dias) {
        var me = this,
            items = me.items.items;

        items[2].dias = items[3].dias = items[4].dias = dias ? dias : [];

        items[3].refresh();
        items[2].refresh();
        items[4].refresh();
    },

    transformCellCls: function (date, cls) {
        var me = this,
            view = me.up('agenda-entrega-tela'),
            viewReferences = view.getReferences(),
            hoje = date.getUTCDate(),
            dia = me.dias ? me.dias.find(dia => dia.dia == hoje) : null,
            calendarValue = viewReferences ? viewReferences.calendar.getValue() : null;

        if (!dia || date.getMonth() !== me.getMonth().getUTCMonth())
            return;

        cls.push(dia ? cls.push(dia.cls) : 'entrega-calendario-sem-entregas');

        if (calendarValue.toLocaleDateString() == date.toLocaleDateString())
            cls.push('x-selected');
    }

    //#endregion
});