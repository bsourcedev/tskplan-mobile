/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.agendaEntrega.Agenda', {
    extend: 'Ext.Panel',
    xtype: 'agenda-tela',
    layout: 'vbox',
    requires: [],

     controller: 'agenda-tela',
     viewModel: 'agenda-tela',

    bodyPadding: '10 20 10 20',
    scrollable: 'y',
    defaults: {
        xtype: 'panel',
        cls: 'card',
        shadow: true,
        margin: '0 0 20 0',
        defaults: {
            xtype: 'label'
        }
    },

    bodyCls: 'checklistDetail-tela',

    style: 'background-color: whitesmoke;',
    bodyStyle: 'background-color: whitesmoke;',

    items: [{
        items: [{
            cls: 'card-titulo',
            html: 'Pedido #0000',
            style: 'line-height: 22px',
            bind: {
                html: 'Pedido #{Record.PedidoCompraId}'
            }
        }]
    }, {
        items: [{
            html: 'Detalhes',
            cls: 'card-titulo'
        }, {
            cls: 'card-descricao',
            html: '<i class="far fa-building tarefa-icon-descricao"></i> - Obra',
            bind: {
                html: '<i class="far fa-building tarefa-icon-descricao"></i> {Record.Obra}'
            }
        }, {
            cls: 'card-descricao tarefa-item-descricao',
            html: '<i class="fas fa-industry tarefa-icon-descricao"></i> - Fornecedor',
            bind: {
                html: '<i class="fas fa-wrench tarefa-icon-descricao"></i> {Record.Fornecedor}'
            }
        }]
    }, {
        reference:'pnlProdutos',
        items: [{
            html: 'Produtos',
            cls: 'card-titulo'
        }]
    },{
        xtype:'button',
        cls:'',
        shadow: true,
        margin: '0 0 20 0',
        ui: 'action raised',
        text:'Criar entrega',
        handler:'onClickCriarEntrega'
    }],

    // listeners: {
    //     painted: 'onActivate'
    // }
});