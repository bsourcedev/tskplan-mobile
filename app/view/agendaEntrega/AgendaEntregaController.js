/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.agendaEntrega.AgendaEntregaController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.agenda-entrega-tela',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();

        me.loadData();
    },

    loadData: function () {
        var me = this,
            viewModel = me.getViewModel(),
            storeCalendario = viewModel.get('Calendario'),
            view = me.getView(),
            references = view.getReferences(),
            date = references.calendar.getValue(),
            mes = date.getUTCMonth() + 1,
            ano = date.getUTCFullYear();

        if (mes == me.mes && ano == me.ano)
            return;

        me.ano = ano;
        me.mes = mes;

        //<debug>
        console.log(`Filtrando ${Ext.Date.monthNames[me.mes-1]}/${me.ano}`);
        //</debug>

        storeCalendario.setParams([{
            name: 'mes',
            value: me.mes
        }, {
            name: 'ano',
            value: me.ano
        }]);

        storeCalendario.load();
    },

    refreshCalendar: function (dates) {
        var me = this,
            view = me.getView(),
            references = view.getReferences();

        references.calendar.addDays(dates);
    },

    onStoreCalendarioLoad: function (store, records) {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            items = store.data.items,
            dias = me.prepareData(items);

        me.refreshCalendar(dias);
    },

    prepareData: function (data) {
        var me = this,
            view = me.getView(),
            references = view.getReferences(),
            date = references.calendar.getValue(),
            ano = date.getUTCFullYear(),
            mes = date.getUTCMonth(),
            lastDayInMonth = new Date(ano, mes + 1, 0).getUTCDate(),
            dataAtual = new Date(),
            dias = [],
            statusCls = [
                'entrega-calendario-sem-entregas',
                'entrega-calendario-entregas-atrasadas',
                'entrega-calendario-entregas',
                'entrega-calendario-entregas-concluidas',
                'entrega-calendario-entregas-parciais',
            ];

        data = data.filter(x => x.get('Ano') == ano && x.get('Mes') == (mes + 1));

        for (let index = 1; index <= lastDayInMonth; index++) {
            var diaLoop = new Date(ano, mes, index),
                _dias = data.filter(x => x.get('Dia') == index),
                statusPeso = _dias.length ? Math.max(..._dias.map(x => x.get('StatusPeso'))) : 0,
                cls = 0;

            if (statusPeso > 0) {
                if (statusPeso == 3) {
                    if (diaLoop.getTime() < dataAtual.getTime())
                        cls = 1;
                    else
                        cls = 2;
                } else if (statusPeso == 2) {
                    cls = 2;
                } else if (statusPeso == 1) {
                    cls = 3;
                }
            }

            dias.push({
                dia: index,
                cls: statusCls[cls]
            });
        }

        return dias;
    },

    onDateChange: function () {
        var me = this;

        me.loadData();
    },

    onClickFiltrar: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            references = view.getReferences(),
            storeCalendario = viewModel.get('Calendario'),
            store = viewModel.get('Store'),
            date = references.calendar.getValue(),
            dias = storeCalendario.data.items.filter(dia => dia.get('Dia') == date.getUTCDate());

        store.data.clear();
        store.loadData(dias);

        references.calendar.setHeight(64);

        viewModel.set('showFiltrar', true);
    },

    showCalendario: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            references = view.getReferences();

        references.calendar.setHeight('-webkit-fill-available');
        viewModel.set('showFiltrar', false);
    },

    onSelect: function (list, selected) {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();

        ScreenManager.abrirTela({
            screen: Ext.create('TskPlan.Mobile.view.agendaEntrega.Agenda', {
                extraData: {
                    record: selected
                }
            }),
            anterior: view,
            title: 'Agenda'
        });
    }
});