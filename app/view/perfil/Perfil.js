/**
 * View de seleção de obra para outras telas
 */
Ext.define('TskPlan.Mobile.view.perfil.Perfil', {
    extend: 'Ext.Panel',
    xtype: 'perfil-tela',
    layout: 'vbox',
    requires: [],

    controller: 'perfil-tela',
    viewModel: 'perfil-tela',

    bodyPadding: 10,

    items: [{
        reference: 'login-foto',
        cls: 'perfil-usuario-foto'
    }, {
        xtype: 'label',
        cls: 'perfil-nome',
        html: '{Nome}',
    }, {
        xtype: 'label',
        cls: 'perfil-email',
        html: '{Email}',
    }, {
        xtype: 'button',
        text: 'Editar informações',
        handler: 'onClickAlterarSenha'
    }, {
        xtype: 'component',
        flex: 1
    }, {
        xtype: 'button',
        text: 'Dispositivos Conectados',
        handler: ''
    }, {
        xtype: 'button',
        text: 'Alterar Senha',
        handler: 'onClickAlterarSenha'
    }, {
        xtype: 'button',
        text: 'Sair',
        handler: 'onClickSair',
        ui: 'decline'
    }]
});