/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.perfil.PerfilController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.perfil-tela',

    onClickSair: function () {
        var me = this;

        TskPlan.Mobile.lib.SessionManager.logout();
    },

    onClickAlterarSenha: function () {
        var me = this;
    },

    onClickFoto: function () {
        var me = this;
    },

    onClickEditar: function () {
        var me = this;
    }
});