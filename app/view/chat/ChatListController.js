/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.chat.ChatListController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.chat-list-tela',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            store = viewModel.get('Store');


        me.newChannel = false;


        me.loadServerList('');

        // me.txtPesquisar.on({
        //     scope: me,
        //     keydown: function () {
        //         var me = this;

        //         if (me.searchTimeout !== null)
        //             clearTimeout(me.searchTimeout);

        //         me.searchTimeout = setTimeout(function () {
        //             me.onPesquisar();
        //         }, 600);
        //     }
        // })

        // me.on({
        //     scope: me,
        //     beforeclose: function () {
        //         MessageLib.activeChannel = null;
        //     }
        // });

        // store.load();
    },

    onSelect: function (list, selected) {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            serverRecord = me.activeServer.getRecord();

        ScreenManager.abrirTela({
            screen: Ext.create('TskPlan.Mobile.view.chat.Chat', {
                channel: selected.data.channel,
                chatList: me
            }),
            anterior: view,
            title: serverRecord.get('ServerName'),
            fullScreen: true,
        });
    },

    doSearch: function (field) {
        var me = this,
            viewModel = me.getViewModel(),
            store = viewModel.get('Store'),
            value = field.getValue();

        if (me.searchTimeout !== null)
            clearTimeout(me.searchTimeout);

        me.searchTimeout = setTimeout(function () {
            me.onPesquisar();
        }, 600);
    },

    onPesquisar: function () {
        var me = this;

        //Chatlist aberto
        if (!me.newChannel) {
            me.loadChatList()
        }
        //Novos chats abertos
        else {
            me.loadNewChats();
        }
    },

    prepareString: function (string) {
        return string.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toUpperCase();
    },

    onUpdateChannelsLastMessage: function (lastMessage) {
        var me = this,
            viewModel = me.getViewModel(),
            store = viewModel.get('Store'),
            record = store.data.findBy(x => x.data.channel.id == lastMessage.channel);

        if (me.newChannel || !record)
            return;

        lastMessage.time = me.formatTime(lastMessage.senderCreationDate);

        record.data.channel.lastMessage = lastMessage;

        record.commit();

        store.data.items.sort(me.chatSortStore);
    },

    //Carrega a lista de servidores
    loadServerList: function (serverId) {
        var me = this,
            viewModel = me.getViewModel(),
            store = viewModel.get('StoreServers'),
            view = me.getView(),
            references = view.getReferences();

        store.clearData();

        MessageLib.getUserServers(function (servers) {

            servers.sort(function (a, b) {
                if (a.server.obraId > b.server.obraId)
                    return 1
                if (a.server.obraId < b.server.obraId)
                    return -1
                return 0
            });

            var serverMap = servers.map(x => new Object({
                Id: x.server.id,
                IconUrl: x.server.iconUrl,
                ObraId: x.server.obraId,
                ServerName: x.server.serverName,
                ShortName: x.server.shortName,
                UnreadMessages: x.server.unreadMessages,
            }))

            store.loadData(serverMap, true);

            var chatListItems = references.serverList.items.items;

            if (chatListItems.length > 0) {
                var serverIndex = serverId !== '' ? chatListItems.findIndex(function (x) {
                    return x.serverData && x.serverData.id.toUpperCase() == serverId.toUpperCase()
                }) : -1;

                me.changeActiveServer(references.serverList.items.items[(serverIndex >= 0 ? serverIndex : 0)]);

                if (serverId !== '' && serverIndex) {
                    //<debug>
                    console.log('Server nao encontrado')
                    //</debug>
                }
            }
        });
    },

    //Carrega a lista de chats
    loadChatList: function () {
        var me = this,
            viewModel = me.getViewModel(),
            pesquisa = viewModel.get('pesquisa'),
            viewModel = me.getViewModel(),
            store = viewModel.get('Store'),
            storeRecords = [];

        store.clearData()

        MessageLib.getUserChannels(function (channels) {
            channels = channels.filter(function (server) {
                if (me.activeServer && server.channel.server && server.channel.server.toUpperCase() == me.activeServer.getRecord().get('Id').toUpperCase())
                    return server
            });

            channels.sort(me.chatSort);

            channels.forEach(function (channel) {
                var adicionar = false;

                //<debug>
                console.log('Mudar este trecho para carregar os canais de forma mais dinamica')
                //</debug>

                if (pesquisa == '')
                    adicionar = true;
                else if (me.prepareString(channel.channel.name).match(new RegExp('.*' + me.prepareString(pesquisa) + '.*')))
                    adicionar = true;

                if (adicionar) {
                    if (channel.channel.lastMessage)
                        channel.channel.lastMessage.time = me.formatTime(channel.channel.lastMessage.senderCreationDate)
                    storeRecords.push(
                        new Object({
                            channel: channel.channel,
                            Id: channel.channel.id,
                            LastMessage: channel.channel.lastMessage,
                            Name: channel.channel.name,
                            Server: channel.channel.server,
                        }));
                }

            });

            store.loadData(storeRecords, true);
        });
    },

    changeActiveServer: function (serverCmp) {
        var me = this,
            viewModel = me.getViewModel(),
            serverRecord = serverCmp.getRecord();

        if (me.activeServer && me.activeServer.rendered)
            me.activeServer.el.dom.classList.remove('x-selected');

        me.activeServer = serverCmp;

        me.activeServer.el.dom.classList.add('x-selected')

        viewModel.set('titulo', serverRecord.get('ServerName'));

        if (!me.newChannel) {
            me.loadChatList();
        } else {
            me.loadNewChats();
        }
    },

    onSelectServer: function (serverList, data) {
        var me = this,
            serverCmp = serverList.items.items.find(x => x.getRecord().get('Id').toUpperCase() == data.get('Id').toUpperCase());

        me.changeActiveServer(serverCmp);
    },

    //Função para ordenar os chats
    chatSort: function (a, b) {
        if (!a.channel.lastMessage)
            return 1
        else if (!b.channel.lastMessage)
            return -1;
        else if (new Date(a.channel.lastMessage.senderCreationDate).getTime() > new Date(b.channel.lastMessage.senderCreationDate).getTime()) {
            return -1;
        } else if (new Date(a.channel.lastMessage.senderCreationDate).getTime() < new Date(b.channel.lastMessage.senderCreationDate).getTime()) {
            return 1;
        }
        return 0;
    },

    
    //Função para ordenar os chats
    chatSortStore: function (a, b) {
        if (!a.data.channel.lastMessage)
            return 1
        else if (!b.data.channel.lastMessage)
            return -1;
        else if (new Date(a.data.channel.lastMessage.senderCreationDate).getTime() > new Date(b.data.channel.lastMessage.senderCreationDate).getTime()) {
            return -1;
        } else if (new Date(a.data.channel.lastMessage.senderCreationDate).getTime() < new Date(b.data.channel.lastMessage.senderCreationDate).getTime()) {
            return 1;
        }
        return 0;
    },

    formatTime: function (dateString) {
        var me = this,
            date = new Date(dateString),
            hoje = new Date(),
            ontem = new Date(),
            horas = date.getHours(),
            minutos = date.getMinutes(),
            format = function (value) {
                return `${('0'.repeat(2 - value.toLocaleString().length))}${value}`;
            };

        ontem.setDate(ontem.getDate() - 1);

        if (date.toLocaleDateString() == hoje.toLocaleDateString())
            return `${format(horas)}:${format(minutos)}`
        else if (date.toLocaleDateString() == ontem.toLocaleDateString())
            return 'ontem'
        else
            return date.toLocaleDateString();
    },

    onActivate: function () {
        var me = this;

        if (me.activeServer)
            me.loadChatList();
    },

    onClickNewChat: function () {
        var me = this,
            viewModel = me.getViewModel();

        viewModel.set('pesquisa', '');

        if (!me.newChannel) {
            me.newChannel = true;
            viewModel.set({
                buttonTip: 'Cancelar',
                buttonIcon: 'fa-times'
            });
            me.loadNewServers();
        } else {
            me.newChannel = false;
            viewModel.set({
                buttonTip: 'Nova Conversa',
                buttonIcon: 'fa-edit'
            });
            me.loadServerList();
        }
    },

    //Carrega os chats para novas mensagens
    loadNewServers: function () {
        var me = this,
            viewModel = me.getViewModel(),
            storeServersNew = viewModel.get('StoreServersNew'),
            storeServers = viewModel.get('StoreServers'),
            view = me.getView(),
            references = view.getReferences();

        storeServers.clearData();

        storeServersNew.getProxy().extraParams = {
            todosServers: false
        }

        storeServersNew.load({
            scope: this,
            callback: function (data) {
                var me = this,
                    first = true;;

                // data.slice(0, 55).forEach(function (servers) {
                //     servers.push({
                //         Id: servers.get('IconUrl'),
                //         IconUrl: servers.get('IconUrl'),
                //         ObraId: servers.get('IconUrl'),
                //         ServerName: servers.get('IconUrl'),
                //         ShortName: servers.get('IconUrl'),
                //         UnreadMessages: servers.get('IconUrl'),
                //     })
                // })

                storeServers.loadData(data.slice(0, 55).map(x => x.data), true);

                me.changeActiveServer(references.serverList.items.items[0]);
            }
        })
    },

    //Filtra os novos chats exibidos
    loadNewChats: function () {
        var me = this,
            viewModel = me.getViewModel(),
            storeChatListRemoto = viewModel.get('StoreChatListRemoto'),
            store = viewModel.get('Store'),
            view = me.getView(),
            references = view.getReferences(),
            pesquisa = viewModel.get('pesquisa');

        store.clearData();

        //<debug>
        console.log('Colocar alerta ao usuario q esta funcionalidade so funciona online')
        //</debug>

        storeChatListRemoto.getProxy().extraParams = {
            ObraId: me.activeServer.getRecord().get('ObraId'),
            Pesquisa: pesquisa ? pesquisa : ''
        }

        storeChatListRemoto.load({
            scope: this,
            callback: function (data) {
                if (data.length > 0)
                    store.loadData(data.slice(0, 55).map(rec => {
                        return new Object({
                            channel: {
                                id: rec.get('Channel'),
                                name: rec.get('ChannelName'),
                                server: rec.get('Server'),
                                lastMessage: {
                                    user: '',
                                    messageText: '<span style="color: #888888;">Toque para iniciar uma conversa</span>',
                                    messageType: 'text',
                                    time: ''
                                },
                            },
                            Id: rec.get('guid'),
                            LastMessage: {
                                user: '',
                                messageText: rec.get('ServerName')
                            },
                            Name: rec.get('ChannelName'),
                            Server: rec.get('Server'),
                            time: ''
                        });
                    }), true);
                else
                    store.loadData([], true)
            }
        })
    }
});