/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.chat.ChatController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.chat-tela',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            store = viewModel.get('Store'),
            references = view.getReferences();

        MessageLib.activeChannel = me;

        me.configuraChat();
        me.configurarAnimacoes();
    },

    getHorario: function (date) {
        var hora = date.getHours(),
            minutos = date.getMinutes(),
            format = function (value) {
                return `${('0'.repeat(2 - value.toLocaleString().length))}${value}`;
            };

        return `${format(hora)}:${format(minutos)}`;
    },

    configuraChat: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();


        me.arrayMessages = [];
        me.messagesIds = [];

        me.channel = view.channel;

        if (view.rendered)
            view.mask('Carregando mensagens');

        MessageLib.getChannelMessages(me.channel, function (messages) {

            console.log('%c Alterar para pegar o user da sessao', 'color:#ff47fa');

            var messages = messages.map(data =>
                new Object({
                    messageId: data.messageId,
                    messageData: data,
                    primeiraMensagem: true,
                    owner: data.user.toUpperCase() == TSKPLAN_SESSION.token.nameid.toUpperCase() ? true : false,
                    data: {
                        usuario: data.userName,
                        mensagem: data.messageText,
                        horario: me.getHorario(new Date(data.senderCreationDate)),
                        primeiraMensagem: true,
                        owner: data.user.toUpperCase() == TSKPLAN_SESSION.token.nameid.toUpperCase() ? true : false,
                        text: data.messageType == 'text' ? true : false,
                        audio: data.messageType == 'audio' ? true : false,
                        audioData: data.urlItem,
                        audioComponent: data.messageType == 'audio' && data.urlItem !== '' ? new Audio(data.urlItem) : null,
                        media: data.messageType == 'media' ? true : false,
                        urlItem: data.urlItem
                    }
                })
            );

            messages.sort(me.messagesSort);

            messages.forEach(newMessage => me.addMessage(newMessage));

            if (view.rendered)
                view.unmask();
        });
    },

    configurarAnimacoes: function () {
        var me = this,
            view = me.getView(),
            references = view.getReferences();

        references.recordButton._hideAnimation._direction = 'right'
        references.recordButton._showAnimation._delay = 250;

        references.sendButton._showAnimation._delay = 250;

        references.recordPanel._hideAnimation._direction = 'right'

        references.recordPanel._showAnimation._direction = 'right'
    },

    onTextChange: function (textfield, value) {
        var me = this,
            view = me.getView(),
            references = view.getReferences();
        
        if (value) {
            references.recordButton.hide()
            setTimeout(() => {
                references.sendButton.show()
            }, 200)
        } else {
            references.sendButton.hide();
            setTimeout(() => {
                references.recordButton.show()
            }, 200)
        }
    },

    onRecordPress: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            references = view.getReferences(),
            startDate = new Date();

        me.taskTimer = setInterval(function (get) {
            var agora = new Date(),
                tempo = '00:00';

            if (startDate) {
                var seconds = Ext.Date.diff(startDate, agora, 's'),
                    minutes = Math.floor(seconds / (60)),
                    minutesString = minutes.toString(),
                    secondsString = (seconds % 60).toString();

                tempo = `${('0').repeat(2-minutesString.length)}${minutesString}:${('0').repeat(2-secondsString.length)}${secondsString}`
            }
            viewModel.set({
                RecordTimerSeconds: seconds,
                RecordTimer: tempo
            });
        }, 25);

        viewModel.set({
            Recording: true,
            RecordStart: startDate
        });

        references.messageToolsArea.hide();

        setTimeout(() => {
            references.recordPanel.show()
        }, 200)

        me.record();
    },

    record: function () {
        var me = this,
            viewModel = me.getViewModel(),
            formatSeconds = function (seconds) {
                var seconds = Math.floor(seconds),
                    secondsString = (seconds % 60).toString();

                return `${Math.floor(seconds / (60))}:${('0').repeat(2-secondsString.length)}${secondsString}`;
            };

        var handleSuccess = function (stream) {
            const options = {
                mimeType: 'video/webm;codecs=vp9'
            };
            const recordedChunks = [];
            mediaRecorder = new MediaRecorder(stream, options);

            mediaRecorder.addEventListener('dataavailable', function (e) {
                if (e.data.size > 0) {
                    recordedChunks.push(e.data);
                }

                if (shouldStop === true && stopped === false) {
                    mediaRecorder.stop();
                    stopped = true;
                }
            });


            mediaRecorder.start();
        };

        if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
            console.log('getUserMedia supported.');
            navigator.mediaDevices.getUserMedia({
                    audio: true,
                    video: false
                })
                // Success callback
                .then(function (stream) {
                    me.chunks = [];
                    mediaRecorder = new MediaRecorder(stream, {
                        mimeType: 'video/webm;codecs=vp9'
                    });
                    mediaRecorder.start();
                    me.mediaRecorder = mediaRecorder;

                    mediaRecorder.ondataavailable = function (e) {
                        if (e.data.size > 0)
                            me.chunks.push(e.data);
                    }

                    me.mediaRecorder.onstop = function (e) {
                        var audio = new Audio(URL.createObjectURL(new Blob(me.chunks))),
                            loaded = false;

                        audio.ontimeupdate = () => {
                            if (loaded && !audio.paused) {
                                lineWidth = ((audio.currentTime / audio.duration) * (100)).toFixed(2)
                                viewModel.set('PreviewAudioLineTime', formatSeconds(audio.currentTime));
                                viewModel.set('PreviewAudioLineWidth', lineWidth > 100 ? 100 : lineWidth);
                            } else {
                                viewModel.set('PreviewAudioLineTime', formatSeconds(audio.duration));
                            }
                        }

                        audio.onended = () => {
                            viewModel.set({
                                PreviewAudioLineWidth: 0,
                                PreviewAudioLineTime: formatSeconds(audio.duration),
                                Playing: false
                            });
                            viewModel.set('PreviewAudioLineTime', formatSeconds(audio.duration));
                        }

                        // audio.controls = true;
                        setTimeout(() => {
                            //<debug>
                            //Nao remover o codigo abaixo, ele corrige um bug do chrome que retorna o audio.duration
                            //</debug>
                            audio.volume = 0;
                            audio.currentTime = 9999999999;
                            audio.play();
                            setTimeout(() => {
                                audio.pause();
                                audio.volume = 1;
                                audio.currentTime = 9999999999;
                                audio.currentTime = 0;

                                viewModel.set('AudioPreview', audio);
                                viewModel.set('PreviewAudioLineTime', formatSeconds(audio.duration));
                                loaded = true;
                            }, 100);
                        }, 150);
                    };

                })

                // Error callback
                .catch(function (err) {
                    console.log('The following getUserMedia error occured: ' + err);
                });
        } else {
            console.log('getUserMedia not supported on your browser!');
        }
    },

    onRecordRelease: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            references = view.getReferences();

        viewModel.set('Recording', false)

        references.recordButton.hide();
        references.recordPanel.hide();

        setTimeout(() => {

            setTimeout(() => {
                references.sendButton.show();
            }, 200);
            references.audioPreviewPanel.show()
        }, 200)

        clearInterval(me.taskTimer)

        me.mediaRecorder.stop();

    },

    onClickCloseAwnser: function () {
        var me = this,
            viewModel = me.getViewModel();

        viewModel.set({
            Awnser: null
        });
    },

    onClickPlayAudioButton: function () {
        var me = this,
            viewModel = me.getViewModel(),
            playing = viewModel.get('Playing'),
            audio = viewModel.get('AudioPreview');

        if (playing) {
            audio.pause();
        } else {
            audio.play();
        }

        viewModel.set('Playing', !playing)
    },

    onClickDeleteButton: function () {
        var me = this,
            viewModel = me.getViewModel(),
            view = me.getView(),
            references = view.getReferences();

        viewModel.set({
            AudioPreview: null,
            PreviewAudioLineWidth: 0,
            PreviewAudioLineTime: '00:00',
            Playing: false
        });

        references.sendButton.hide();
        references.audioPreviewPanel.hide();

        setTimeout(() => {
            references.messageToolsArea.show()

            references.recordButton.show();
        }, 200)
    },

    onClickSend: function () {
        var me = this,
            viewModel = me.getViewModel(),
            view = me.getView(),
            date = new Date(),
            references = view.getReferences(),
            text = viewModel.get('text'),
            audio = viewModel.get('AudioPreview'),
            serverRecord = view.chatList.activeServer.getRecord(),
            messageId = TskPlanManager.newGuid();

        if (audio && audio.duration == Infinity)
            audio.duration = viewModel.get('RecordTimerSeconds');

        //<debug>
        debugger;
        //</debug>

        var execute = function (urlItem) {
            references.chatPanel.add({
                owner: true,
                messageData: {
                    channel: me.channel.id,
                    channelName: me.channel.name,
                    messageId: messageId,
                    messageText: text,
                    messageType: text ? 'text' : (audio ? 'audio' : ''),
                    responseGuid: null,
                    senderCreationDate: date.toUTCString(),
                    server: serverRecord.get('Id'),
                    serverName: serverRecord.get('ServerName'),
                    serverShortName: serverRecord.get('ShortName'),
                    status: 0,
                    urlItem: urlItem,
                    user: TSKPLAN_SESSION.token.nameid,
                    userName: TSKPLAN_SESSION.token.nameid
                },
                data: {
                    usuario: 'User',
                    mensagem: text,
                    horario: Ext.Date.format(new Date, 'H:m'),
                    primeiraMensagem: false,
                    owner: true,
                    text: text ? true : false,
                    audio: audio ? true : false,
                    audioData: urlItem,
                    audioComponent: audio,
                    urlItem: urlItem
                }
            });

            me.scrollBottom();

            MessageLib.sendMessage({
                messageId: messageId,
                messageType: text ? 'text' : (audio ? 'audio' : ''),
                messageText: text,
                senderCreationDate: date.toUTCString(),
                channel: me.channel.id,
                channelName: me.channel.name,
                server: serverRecord.get('Id'),
                serverName: serverRecord.get('ServerName'),
                serverShortName: serverRecord.get('ShortName'),
                urlItem: urlItem
            });
        }
        if (audio)
            me.saveFile(audio.src, function (urlItem) {
                execute(urlItem);
            });
        else
            execute('');


        references.sendButton.hide();
        references.recordPanel.hide();
        references.audioPreviewPanel.hide();

        setTimeout(() => {
            references.messageToolsArea.show()
            references.recordButton.show();

            viewModel.set({
                text: '',
                RecordStart: null,
                RecordTimer: '00:00',
                RecordTimerSeconds: 0,
                recording: false,
                Awnser: null,
                Playing: false,
                AudioPreview: null,
                PreviewAudioLineWidth: 0,
                PreviewAudioLineTime: '00:00'
            });
        }, 200);
    },

    addMessage: function (newMessage) {
        var me = this,
            view = me.getView(),
            references = view.getReferences();

        if (references.chatPanel.items.items.findIndex(x => x.messageData.messageId.toUpperCase() == newMessage.messageId.toUpperCase()) >= 0)
            return;

        if (references.chatPanel.items.items.length > 0) {
            var lastMessage = references.chatPanel.items.items[references.chatPanel.items.items.length - 1];
            if (lastMessage.messageData.user.toUpperCase() == newMessage.messageData.user.toUpperCase()) {
                newMessage.primeiraMensagem = false;
                newMessage.data.primeiraMensagem = false;
            }

        }

        references.chatPanel.add(newMessage);

        me.scrollBottom();
    },

    scrollBottom: function () {
        var me = this,
            view = me.getView(),
            references = view.getReferences(),
            chatPanel = references.chatPanel,
            panelBody = chatPanel.el.down('.x-body-el');

        setTimeout(() => {
            panelBody.scroll('bottom', panelBody.getHeight(), true);
        }, 100)
    },

    messagesSort: function (a, b) {
        return new Date(a.messageData.senderCreationDate) - new Date(b.messageData.senderCreationDate);
    },

    onClickRecordMedia: function () {
        var me = this,
            viewModel = me.getViewModel(),
            view = me.getView(),
            date = new Date(),
            references = view.getReferences(),
            serverRecord = view.chatList.activeServer.getRecord(),
            messageId = TskPlanManager.newGuid();

        NavigatorManager.camera.getPicture(function (imageUrl) {

            me.saveFile(imageUrl, function (urlItem) {

                console.log(`Imagem ${imageUrl}`)

                references.chatPanel.add({
                    owner: true,
                    messageData: {
                        channel: me.channel.id,
                        channelName: me.channel.name,
                        messageId: messageId,
                        messageText: '',
                        messageType: 'media',
                        responseGuid: null,
                        senderCreationDate: date.toUTCString(),
                        server: serverRecord.get('Id'),
                        serverName: serverRecord.get('ServerName'),
                        serverShortName: serverRecord.get('ShortName'),
                        status: 0,
                        urlItem: urlItem,
                        user: TSKPLAN_SESSION.token.nameid,
                        userName: TSKPLAN_SESSION.token.nameid
                    },
                    data: {
                        usuario: 'User',
                        mensagem: '',
                        horario: Ext.Date.format(new Date, 'H:m'),
                        primeiraMensagem: false,
                        owner: true,
                        text: false,
                        audio: false,
                        media: true,
                        audioData: '',
                        audioComponent: null,
                        urlItem: urlItem
                    }
                });

                me.scrollBottom();

                MessageLib.sendMessage({
                    messageId: messageId,
                    messageType: 'media',
                    messageText: '',
                    senderCreationDate: date.toUTCString(),
                    channel: me.channel.id,
                    channelName: me.channel.name,
                    server: serverRecord.get('Id'),
                    serverName: serverRecord.get('ServerName'),
                    serverShortName: serverRecord.get('ShortName'),
                    urlItem: urlItem
                });
            });
        })
    },

    //Salva arquivo no servidor
    saveFile: function (url, callback) {
        var me = this,
            mediaData = {
                guid: TskPlanManager.newGuid(),
                media: url
            };

        //<debug>
        console.groupCollapsed(`Salvando media [${mediaData.guid}]`);
        console.log(mediaData)
        console.groupEnd(`Salvando arquivo`);
        //</debug>


        //Adiciona localização nula caso nao tiver sido preenchida
        if (!mediaData.geolocation) {
            mediaData.geolocation = {
                coords: {
                    latitude: 0,
                    longitude: 0,
                    altitude: 0,
                    accuracy: 0,
                    altitudeAccuracy: 0,
                    heading: 'N 0°',
                    speed: 0
                },
                timestamp: +new Date()
            };
        }

        MediaManager.toDataUrl(mediaData.media, function (myBase64) {
            var varSplit = myBase64.split(';'),
                mimeType = varSplit[0].replace('data:', ''),
                base64 = varSplit[1].replace('base64,', ''),
                nome = mediaData.media.split('/').pop();

            Ext.Ajax.request({
                url: window.API_CONFIG.Media,
                async: false,
                method: 'POST',
                jsonData: {
                    "Guid": mediaData.guid,
                    "Latitude": `${mediaData.geolocation.coords.latitude}`,
                    "Longitude": `${mediaData.geolocation.coords.longitude}`,
                    "Extensao": mimeType,
                    "Nome": nome,
                    "Base64": base64 //myBase64 <- voltar isso aq qnd for aceitar outros tipos de arquivo
                },
                timeout: 300000,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjUzIiwibmFtZWlkIjoiQlNvdXJjZVgiLCJuYmYiOjE1OTA1MDQ4OTMsImV4cCI6MjUzNzIzMjg5MywiaWF0IjoxNTkwNTA0ODkzfQ.axGrOWhQ7Ed79yTMPzrPrkar6IcPfZLfk7X5iKc5PO4' //+ window.bearerToken

                },
                success: function (response, opts) {
                    //<debug>
                    console.log(`Sucesso [${mediaData.guid}]`);
                    //</debug>
                    callback(JSON.parse(response.responseText).Url);
                },
                failure: function (response, opts) {
                    //<debug>
                    console.log(`Erro [${mediaData.guid}]`);
                    console.log('Desenvolver tratamento p/ fila de erros');
                    debugger;
                    //</debug>
                }
            });
        });
    }
});