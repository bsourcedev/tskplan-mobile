/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.chat.components.Mensagem', {
    extend: 'Ext.Component',
    xtype: 'mensagem-component',
    requires: [
        'Ext.SegmentedButton'
    ],
    controller: 'mensagem-component',

    cls: 'chat-mensagem',

    data: {
        usuario: 'User',
        mensagem: `Mensagem`,
        horario: '12:12',
        primeiraMensagem: false,
        owner: false,
        text: true,
        audio: false,
        audioData: 'https://www.kozco.com/tech/LRMonoPhase4.mp3',
        media: false,
        urlItem:'',
    },

    tpl: [
        '<div class="chat-message-content">',

        //Msg
        "<tpl if='text == true'>",
        "<span class='chat-mensagem-text'>",
        "<tpl if='primeiraMensagem == true'>",
        "<span class='chat-mensagem-user'>{usuario}</span>",
        "</tpl>",
        "{mensagem}",
        "</span>",
        "</tpl>",
        //

        //Audio
        "<tpl if='audio == true'>",
        "<span class='chat-mensagem-audio'>",
        "<tpl if='primeiraMensagem == true'>",
        "<span class='chat-mensagem-user'>{usuario}</span>",
        "</tpl>",
        '<i class="fas fa-play chat-mensagem-button"></i>',
        '<div class="message-audio-line"><div class="message-audio-played"></div></div>',
        '<div class="message-audio-time">0:00</div>',
        "</span>",
        "</tpl>",
        //

        //Media
        "<tpl if='media == true'>",
        "<span class='chat-mensagem-media' style='background-image:url({urlItem})'>",
        "<tpl if='primeiraMensagem == true'>",
        "<span class='chat-mensagem-user'>{usuario}</span>",
        "</tpl>",
        // "<img src=\"{urlItem}\">",
        "</span>",
        "</tpl>",
        //

        "<span class='chat-mensagem-horario'>{horario}</span>",
        '</div>',
        '<div class="chat-awnser-target" height="26"/>'
    ],

    listeners: {
        painted: 'onPainted',
        click: 'onClick'
    }
});