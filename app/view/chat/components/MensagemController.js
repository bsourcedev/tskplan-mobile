/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.chat.components.MensagemController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.mensagem-component',

    init: function () {
        var me = this,
            view = me.getView(),
            data = view.getData();

        data.primeiraMensagem = view.primeiraMensagem;
        data.owner = view.owner;

        if (data.primeiraMensagem)
            view.addCls('chat-mensagem-primeira');

        if (data.owner){
            view.addCls('owner');
        }
        
        if (data.media){
            view.addCls('chat-mensagem-media-ctn');
        }

        view.setData(data);

    },

    onPainted: function () {
        var me = this,
            view = me.getView(),
            data = view.getData();

        me.configureDrag();

        if (data.audio)
            me.configureAudio();
    },

    configureDrag: function () {
        var me = this,
            view = me.getView(),
            data = view.getData(),
            el = view.element,
            msg = el.down('.chat-message-content'),
            target = el.down('.chat-awnser-target');

        target.setHeight(msg.getHeight());

        this.horizontalSource = new Ext.drag.Source({
            element: msg,
            constrain: {
                // Constrain dragging vertically only. Also to the parent container.
                element: el,
                horizontal: true
            },
            revert: true,
            proxy: 'original'
        });

        this.target = new Ext.drag.Target({
            element: target,
            // validCls: 'chat-awnser-target',
            listeners: {
                drop: this.onDrop
            }
        });
    },

    configureAudio: function () {
        var me = this,
            view = me.getView(),
            el = view.element,
            button = el.query('.chat-mensagem-button')[0],
            timer = el.query('.message-audio-time')[0],
            played = el.query('.message-audio-played')[0],
            data = view.getData(),
            formatSeconds = function (seconds) {
                var seconds = Math.floor(seconds),
                    secondsString = (seconds % 60).toString();

                return `${Math.floor(seconds / (60))}:${('0').repeat(2-secondsString.length)}${secondsString}`;
            },
            duracao;

        me.audio = data.audioComponent ? data.audioComponent : new Audio(data.audioData);

        duracao = me.audio && me.audio.duration !== Infinity ? me.audio.duration : 1

        me.audio.oncanplaythrough = () => {

            //Testar isso aqui
            if (isNaN(duracao))
                duracao = me.audio && me.audio.duration !== Infinity ? me.audio.duration : 1

            timer.textContent = formatSeconds(duracao);

            button.onclick = function () {
                if (button.classList.contains('fa-play')) {
                    button.classList.remove('fa-play');
                    button.classList.add('fa-pause');
                    me.audio.play();
                } else {
                    button.classList.remove('fa-pause');
                    button.classList.add('fa-play');
                    me.audio.pause();
                }
            }

            me.audio.ontimeupdate = () => {
                var width = ((me.audio.currentTime / duracao) * (100)).toFixed(2);
                played.style.width = `${(width >100 ? 100 : width)}%`;
                timer.textContent = formatSeconds(me.audio.currentTime);
            }

            me.audio.onended = () => {
                button.classList.remove('fa-pause');
                button.classList.add('fa-play');
                played.style.width = '0%';
                timer.textContent = formatSeconds(duracao);
            }
        }
    },

    onDrop: function (target, info) {
        if (!info.source.getElement().hasCls('chat-message-content'))
            return

        var msgComponent = Ext.getCmp(info.source._element.parent('.chat-mensagem').id),
            msgController = msgComponent.getController(),
            msgData = msgComponent.getData(),
            chat = msgComponent.up('chat-tela'),
            chatViewModel = chat.getViewModel(),
            formatSeconds = function (seconds) {
                var seconds = Math.floor(seconds),
                    secondsString = (seconds % 60).toString();

                return `${Math.floor(seconds / (60))}:${('0').repeat(2-secondsString.length)}${secondsString}`;
            };

        chatViewModel.set('Awnser', {
            Usuario: msgData.usuario,
            Mensagem: msgData.text ? msgData.mensagem : msgData.audio ? `<i class="fa fa-microphone" style="color: var(--base-color);"></i><span style="font-weight:600;margin-left: 5px;">${formatSeconds(msgController.audio.duration)}</span>` : '',
        });
    }
});