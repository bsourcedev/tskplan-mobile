/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.tarefa.ChatListModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.chat-list-tela',

    data: {
        pesquisa: '',
        titulo: 'Conversas',
        buttonTip: 'Nova Conversa',
        buttonIcon: 'fa-edit'
    },

    formulas: {},

    stores: {
        Store: {
            type: 'chat-list-tela',
        },
        StoreServers: {
            type: 'chat-servers-local',
        },
        StoreServersNew: {
            type: 'chat-servers',
        },
        StoreChatListRemoto: {
            type: 'chat-list-remoto',
        }
    }
});