/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.tarefa.ChatModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.chat-tela',

    data: {
        text: '',
        RecordStart: null,
        RecordTimer: '00:00',
        RecordTimerSeconds: 0,
        recording: false,
        Awnser: null,
        Playing: false,
        AudioPreview: null,
        PreviewAudioLineWidth: 0,
        PreviewAudioLineTime: '00:00'
    },

    formulas: {
        showEnviar: function (get) {
            return get('text') ? true : false;
        },
        showAwnser: function (get) {
            return get('Awnser') ? true : false;
        },
        playButtonIcon: function (get) {
            return get('Playing') ? 'pause' : 'play';
        }
    },

    stores: {
        Store: {
            type: 'chat-tela',
        }
    }
});