/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.chat.Chat', {
    extend: 'Ext.Panel',
    xtype: 'chat-tela',
    requires: [
        'Ext.SegmentedButton',
        'TskPlan.Mobile.view.chat.components.Mensagem'
    ],
    layout: 'vbox',

    controller: 'chat-tela',
    viewModel: 'chat-tela',

    bodyCls: 'tela-background',

    items: [{
        xtype: 'panel',
        bodyCls: 'chat-body',
        reference: 'chatPanel',
        layout: 'vbox',
        flex: 1,
        defaults: {
            xtype: 'mensagem-component'
        },
        scrollable: 'y',
        items: [],
    }, {
        xtype: 'container',
        layout: 'hbox',
        maxHeight: 46,
        cls: 'chat-awnser-card',
        items: [{
            xtype: 'component',
            flex: 1,
            bind: {
                html: '<span class="chat-awnser-card-user">{Awnser.Usuario}</span><span class="chat-awnser-card-text">{Awnser.Mensagem}</span>',
            }
        }, {
            xtype: 'button',
            text: '<i class="fas fa-times" ></i>', //style="color:var(--faded-color)"
            handler: 'onClickCloseAwnser'
        }],
        bind: {
            hidden: '{!showAwnser}'
        }
    }, {
        xtype: 'panel',
        layout: 'hbox',
        bodyCls: 'chat-toolbar',
        maxHeight: 46,
        items: [{
            xtype: 'container',
            layout: 'hbox',
            reference: 'recordPanel',
            hideAnimation: 'slideOut',
            showAnimation: 'slideIn',
            flex: 1,
            hidden: true,
            items: [{
                xtype: 'label',
                cls: 'chat-message-recording-text',
                html: '<i class="fas fa-microphone-alt chat-message-recording"></i>'
            }, {
                xtype: 'label',
                cls: 'chat-message-recording-text chat-message-recording-span',
                html: '00:00',
                bind: {
                    html: '{RecordTimer}'
                }
            }, {
                xtype: 'component',
                flex: 1
            }, {
                xtype: 'label',
                margin: '0 8 0 0',
                cls: 'chat-message-recording-text chat-message-recording-span',
                html: 'Solte p/ finalizar'
            }]
        }, {
            xtype: 'container',
            layout: 'hbox',
            reference: 'audioPreviewPanel',
            hideAnimation: 'slideOut',
            showAnimation: 'slideIn',
            flex: 1,
            hidden: true,
            items: [{
                xtype: 'button',
                reference: 'deleteAudioButton',
                margin: '0 0 0 0',
                cls: 'chat-toolbar-record-button',
                handler: 'onClickDeleteButton',
                text: '<i class="fas fa-trash-alt" style="color:var(--faded-color)"></i>',
            }, {
                xtype: 'button',
                reference: 'playAudioButton',
                margin: '0 0 0 0',
                handler: 'onClickPlayAudioButton',
                bind: {
                    text: '<i class="fas fa-{playButtonIcon}" style="color:var(--faded-color)"></i>',
                }
            }, {
                xtype: 'component',
                width: '100%',
                flex: 1,
                margin: '0 9',
                bind: {
                    html: '<div class="chat-audio-preview">' +
                        '<div class="chat-audio-preview-line" style="width:{PreviewAudioLineWidth}%"></div>' +
                        '</div>'
                }
                // xtype: 'component',
                // width: '100%',
                // flex: 1,
                // cls: 'chat-audio-preview',
                // item: [{
                //     xtype: 'container',
                //     cls: 'chat-audio-preview-line',
                //     bind: {
                //         width: '{PreviewAudioLineWidth}%'
                //     }
                // }]
            }, {
                xtype: 'label',
                cls: 'chat-audio-preview-time',
                bind: {
                    html: '{PreviewAudioLineTime}'
                }
            }]
        }, {
            xtype: 'container',
            layout: 'hbox',
            reference: 'messageToolsArea',
            hideAnimation: 'slideOut',
            showAnimation: 'slideIn',
            flex: 1,
            items: [{
                xtype: 'button',
                reference: 'mediaButton',
                width: 42,
                margin: '0 0 0 0',
                // hideAnimation: 'slideOut',
                // showAnimation: 'slideIn',
                cls: 'chat-toolbar-media-button',
                text: '<i class="fas fa-camera" style="color:var(--faded-color)"></i>',
                hidden: false,
                handler: 'onClickRecordMedia'
            }, {
                xtype: 'textfield',
                cls: 'chat-text-area',
                reference: 'messageTextfield',
                hideAnimation: 'slideOut',
                showAnimation: 'slideIn',
                margin: '0 5 0 0',
                flex: 1,
                listeners: {
                    change: 'onTextChange'
                },
                bind: {
                    value: '{text}'
                }
            }]
        }, {
            xtype: 'container',
            width: 42,
            layout: 'hbox',
            style: 'background:var(--base-color)',
            items: [{
                xtype: 'button',
                reference: 'sendButton',
                margin: '0 0 0 0',
                width: '100%',
                hideAnimation: 'fadeOut',
                showAnimation: 'fadeIn',
                hidden: true,
                cls: 'chat-toolbar-send-button svg-chat-button',
                text: '<svg xmlns="http://www.w3.org/2000/svg" style="color:var(--faded-color);display: flex;" viewBox="0 0 24 24" width="24" height="24"><path fill="currentColor" d="M1.101 21.757L23.8 12.028 1.101 2.3l.011 7.912 13.623 1.816-13.623 1.817-.011 7.912z"></path></svg>',
                handler: 'onClickSend'
            }, {
                xtype: 'button',
                reference: 'recordButton',
                width: '100%',
                margin: '0 0 0 0',
                hideAnimation: 'slideOut',
                showAnimation: 'slideIn',
                cls: 'chat-toolbar-record-button',
                text: '<i class="fas fa-microphone-alt" style="color:var(--faded-color)"></i>',
                hidden: false,
                listeners: {
                    element: 'element',
                    mousedown: 'onRecordPress',
                    mouseup: 'onRecordRelease'
                }
            }]
        }]
    }],

    // listeners: {
    //     painted: 'onActivate'
    // }
});