/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.chat.ChatList', {
    extend: 'Ext.Panel',
    xtype: 'chat-list-tela',
    requires: [
        'Ext.SegmentedButton'
    ],
    layout: 'hbox',

    controller: 'chat-list-tela',
    viewModel: 'chat-list-tela',

    bodyCls: 'tela-background',

    items: [{
        xtype: 'list',
        bodyCls: 'tela-background',
        reference: 'serverList',
        padding: '0',
        width: 54,
        deselectable: false,
        maxWidth: 54,
        minWidth: 54,
        height: '100%',
        bind: {
            store: '{StoreServers}'
        },
        cls: 'chat-server-list-ui',
        // itemCls: 'chat-server-list-ui',
        itemContentCls: 'chat-server-ui news',
        itemTpl: [
            "<tpl if='IconUrl == \"\"'>" +
            "<div class='chat-server-ui-info' data-qtip='{ServerName}'>" +
            "<span class='chat-server-ui-short-name'>{ShortName}</span>" +
            "<span class='chat-server-ui-id'>{ObraId}</span>" +
            "</div>" +
            "<tpl else>" +
            "<img data-qtip='{ServerName}' src='{IconUrl}'>" +
            "</tpl>",
        ],
        // itemTpl: [
        //     "<div class='chat-server-ui-info' data-qtip='Nome do servidor'>" +
        //     "<span class='chat-server-ui-short-name'>CDS</span>" +
        //     "<span class='chat-server-ui-id'>21</span>" +
        //     "</div>"
        // ],
        listeners: {
            select: 'onSelectServer'
        }
    }, {
        xtype: 'panel',
        layout: 'vbox',
        width: '100%',
        flex: 1,
        tbar: {
            flex: 1,
            layout: 'vbox',
            "items": [{
                xtype: 'container',
                width: '100%',
                layout: 'hbox',
                height: 32,
                items: [{
                    xtype: 'label',
                    flex: 1,
                    style: 'line-height: 38px;',
                    bind: {
                        html: '{titulo}'
                    }
                }, {
                    xtype: 'button',
                    iconCls: `button-small-icon x-fa `,
                    handler: 'onClickNewChat',
                    bind: {
                        iconCls: 'button-small-icon x-fa {buttonIcon}',
                        tooltip:'{buttonTip}'
                    }
                    // text:''
                }]
            }, {
                "xtype": "searchfield",
                "flex": 2,
                width: '100%',
                height: 32,
                "ui": "faded",
                "placeholder": "Buscar",
                listeners: {
                    buffer: 500,
                    change: 'doSearch'
                },
                bind: {
                    value: '{pesquisa}'
                }
            }]
        },
        items: [{
            xtype: 'list',
            bodyCls: 'tela-background',
            padding: '0',
            width: '100%',
            flex: 1,
            bind: {
                store: '{Store}'
            },
            reference: 'chatList',
            itemCls: 'chat-list-card',
            itemContentCls: 'chat-list-content',
            itemTpl: [
                '<div class="chat-list-obra">{Obra}</div>',
                '<div class="chat-list-picture"><i class="far fa-comment"></i></div>',
                '<div class="chat-list-title card-titulo">{channel.name}</div>',
                // '{% debugger; %}',
                '<div class="chat-list-message">',
                "<tpl if='channel.lastMessage'>",
                "<tpl if='channel.lastMessage.user !== \"\"'>",
                "{channel.lastMessage.user}: ",
                "</tpl>",
                "<tpl if='channel.lastMessage.messageType == \"text\"'>",
                "{channel.lastMessage.messageText}",
                "</tpl>",
                "<tpl if='channel.lastMessage.messageType == \"media\"'>",
                "Imagem",
                "</tpl>",
                "<tpl if='channel.lastMessage.messageType == \"audio\"'>",
                "Audio",
                "</tpl>",
                "<tpl else>",
                "<span style=\"color: #6f6f6f;\">Nenhuma mensagem no canal</span>",
                "</tpl>",
                '</div>',
                "<tpl if='channel.lastMessage && channel.lastMessage.time !== \"\"'>",
                '<div class="chat-list-time">{channel.lastMessage.time}</div>',
                "<tpl else>",
                '<div class="chat-list-time"></div>',
                "</tpl>",
                '<div class="chat-list-options"></div>',
            ],
            listeners: {
                select: 'onSelect'
            }
        }]
    }],

    listeners: {
        painted: 'onActivate'
    }
});