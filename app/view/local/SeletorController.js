/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.local.SeletorController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.local-seletor-tela',

    onActivate: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            store = viewModel.get('Store');

        view.mask('Carregando dados...');

        store.getProxy().extraParams = {
            ObraId: (view.obraId ? view.obraId : 0)
        };

        store.load({
            scope: me,
            callback: me.loadStoreCallback
        });
    },

    loadStoreCallback: function (records) {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            store = viewModel.get('Store'),
            treeStore = Ext.create('Ext.data.TreeStore', {
                rootVisible: false,
                root: {
                    expanded: true,
                    children: me.prepareData(store.data.items)
                }
            });

        view.setStore(treeStore);

        view.unmask();
    },

    prepareData: function (recordSet, parentId) {
        var me = this,
            data = [],
            records;

        parentId = (parentId ? parentId : 0);
        records = recordSet.filter(rec => rec.get('PaiId') == parentId);

        records.forEach(record => {
            var childrens = me.prepareData(recordSet, record.get('ObraLocalId'));

            data.push({
                ObraLocalId: record.get('ObraLocalId'),
                Caminho: record.get('Caminho'),
                Nome: record.get('Nome'),
                parentId: record.get('PaiId'),
                children: childrens,
                leaf: childrens.length > 0 ? false : true,
                guid: record.get('guid')
            });
        });

        return data;
    },

    onItemSelect: function (listView, selected) {
        var me = this,
            view = me.getView(),
            tabView = view.up(),
            rec = selected[0];

        if (!rec || !rec.get('leaf'))
            return false;

        view.fireEvent('localSelecionado', rec.data);

        //<debug>
        console.log(`Local Selecionado: ${rec.get('ObraLocalId')} - ${rec.get('Nome')}`);
        //</debug>  

        ScreenManager.voltar();
    }
});