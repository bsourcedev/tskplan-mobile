/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.local.SeletorModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.local-seletor-tela',

    data: {

    },

    stores: {
        Store: {
            type: 'local-store'
        }
    }
});