/**
 * View de seleção de obra para outras telas
 */
Ext.define('TskPlan.Mobile.view.local.Seletor', {
    extend: 'Ext.grid.Tree',
    xtype: 'local-seletor-tela',
    requires: [],

    controller: 'local-seletor-tela',

    viewModel: 'local-seletor-tela',

    bind: '{TreeStore}',

    cls: 'local-seletor-tree',

    expanderOnly: false,

    columns: [{
        xtype: 'treecolumn',
        text: '',
        dataIndex: 'Nome',
        flex: 1
    }],

    hideHeaders: true,

    listeners: {
        activate: 'onActivate',
        select: 'onItemSelect'
    }
});