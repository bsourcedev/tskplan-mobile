/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.login.Login', {
    extend: 'Ext.Panel',
    xtype: 'login-tela',
    layout: 'center',
    requires: [],

    controller: 'login-tela',
    viewModel: 'login-tela',

    bodyCls: 'login-tela',

    padding: '0 20',

    items: [{
        xtype: 'container',
        layout: 'vbox',
        items: [{
            xtype: 'label',
            height: 80,
            cls: 'login-titulo',
            html: '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATUAAABRCAYAAACzDTH8AAAACXBIWXMAAAsSAAALEgHS3X78AAAPjUlEQVR4nO2dPXPcRhKGW1cXXCbzahNE0tHJZdaVmJuBlWwiXiCnkhMyNC8iM0sZGZkXUslRKZXICRKqylZulskfIFqM9gKUJf4CXg2roYMgfMy83TPAovqp2rIl7ccssHjx9kx3z63r62syDMOYCn+yM2kYxpQwUTMMY1KYqBmGMSlM1AzDmBQmaoZhTAoTNcMwJoWJmmEYk8JEzTCMSWGiZhjGpDBRMwxjUpioGYYxKUzUDMOYFCZqhmFMij/b6TRCKPLsHhF9IThoH2bzxVmsg17k2brH097N5ot3scbgA3gcz2bzxYeRjCnKMSzyzH3+vb7nzeaLX9r+bfKth4o82+H/vU9Ej1qe9pKITono/Wy+eJ5weEtDkWd3ieiIiL5WHvO5u0DcBeveH71Qijzb4PHd9nzJd7P54gj5LAk8zgMiugO8zRURbWuPm8XMvedXgS99w+NRuUkVeeaOy/eeT78kovWm38tnolbk2R9EtKIxSAVezuaLb0Pfpsizb4hos0PE+nDC9no2X7yUfIUiz46BMZzO5os1wWf+ygIuwX3/3dl88b7yvmfAjx7BXShPu+7EDd/ZCe7vwGetxHQ+dYo82yaiHxXe6p+z+eKV0pi+4BsKIrLEQrsuFbYiz54S0Q+BL3sxmy+e1P/ykzm1Is9WRiRojouQJxd5dr/IsxMiOhEIGrEgHjuBKPJM8j6IuJyiH8bfXSpoTsy2aoK2kUjQiJ3gz0WeHbFY+bANflZvmKMFh8Uagkbs9LTYEAgasTPWEFjkHDb+PuoLBdILQhtvUeMw07mUbxTHcJ/F7aTIs9WQF/INIug1TJCQVz7vUOG7O0Hbb/j7ZBd/hcfOQRR59tmduAF0fNHm9hp4qvhed9hhaeB74+gbzwb6Yv4uvtMGVRpd9thFzcu1sEPZizgOJxZvA10bImiEiFqRZ3vsLiW0CRoNJGrEP/T/ONfW8zxkfFepQk92adpzkT4LIinfB3XLpH1TqovamEJP8rnAWdA03VkXxywgPqA3iKDwkx3qjsdTu+gSNBKudmrwmOdcPkNwl19Wl1aidaPROrdfB0wX1IkqamNyahfVeZ0meCI+laCV7BR55uOKkBuEW30NCbk3FRxqn6BRBJeB8ENLugZ6QSRJ6Yjk0kjRqWnOlaLijQqrV/iJhkwx6Ly4+YKWTOKjXHimfSA3iBBBc9/9UPhdegVNcPeNQVMYOmpRi+TSSMOpcSqHJhvgXB8k0G0r5B9FTTCxHYvWMIzHGnMOrYstz+chx9J3DtG502Pg/av4ODRSmkjWomlCGr3Le6eMoER0aY7bCjcc7XPrpgF8FnY0xnHZ9g9VpzYmQXN0hZ6bA83/udy1157PjeLUXNpKQkEjxTBHi/pFg44vhVOL5dJKpOcmxgIQsmCApJS0nr+qqC3Tyqd0YhzFSwhYeBD6Qu5VzsGTCHqIoJHgbn7FibTlQ4uHGuOLXSYV2aWVSEUphgsPSu/wLGtrotVpV2s/x+bUGkWN55KGcGnPA1yaejrHQIJGgh9+YzkPz+M84QeyanlzIVTmU5C7/DnyuYHEdmk0UlEjdmu+CbmqiwQ04vDzfcfKp8RRvucL+1b5IKIHng4sRAygYzmbL9qEfIVDTsk5QgSNwAvnsq0+0ZXTzOaLbb6gUHG5uRgFd/kpuDRS+IxYYwxJ71BPnP7o1EJqLDlXKzgEZBGRInFBD+opE+y+Xhd5ts+riU0rqs9DUi0iLBJIy58gQRPkgPVOwrvEV64W+A14//KCQe/ysXPUEJd2xe4jyHnWXGvI62IvAD31XDRQFzW0n1q0lT0P0It7v0uYnDNkYf+2tkhx4+4SjLFxbAr1nKhDo9jpElwELQkFR1ceJXBpR6CDRI9BbFHzTe9AxtFZDYKKWtQcrB7QOSWvjhvcmWOtMt79viTgBlREXyG5WCJoJLhgQpyDJLUCHV/M8ih0Lu0gsajFLn3zTe9Akn87b0rBoibIZ9NyaqioeY+ZHd0aC2FQfzWe0EfG+Inoc4G6JLlYKmgkCO9SJbaiK59RctQELu0Nr8YiDhKdV0yRf9iZ3iEIgTt/X4hTS1ao3UKoayoJmgMsw9FELo2qx4frOSUF6hqCRoJM7xBRkzgG5C7fmrSpAOrSykUVRNTQjh0pmhT0pXeMRtSi5GAleJ9HHM7FBjo+5conC5qkWkJL0Aj80XnPkfGdGnE2Z7Hu8igCl1ZdKUbn+hCBQl6DzH92uTX1HDVK6dTa0hUAfHPFmnDC9pbLjGKBhJ6loEkL1DUFjbQzvRtAmx2+E9zlY5VHSebSbuDJb8RJIuKArGojbcS70juiTG+kEjUtl0YKc3M3SazOtQky/7uAFlFYaCUF6qqCJsgB63UbLgG3yLNfGqoDfLjiVdPR5KgJXNpVg1Agbi3IdaHndjZfHICi2+bWoBC4b3oD2U0qaYvqOm51ssizC4Vk4Ufs3J73pXsEggrlieAztR0aCZzQ3YZdico/3+OHpH10mak+pvATdmkNqQlngNiHipRkkeAAaEv+xPXDa/iuUULgZXRqBOSNdbHJXW3FXT8EezwMvcrZBPrDf8wJtT9XHj/yphoPhYJGCqKmmqMmrB5oCr+R8Di0YweUG8b/RULQ27wXwkcEid29N6UgUYtVqB0K55Jpb2Xnmj/+IdxoJXWp2VYkQaMBW3h3cVnZRWksLbxRl/aiZSwpFguQ8PNmXDzmF8Dr68cpWuJ0qFNDL1q18LPE7XgUQdhWuGX3MbuuUFJ2OtmKvEfpmPqoldxcGGNp4S10aY1imGixAJmgrwowssBzpzaXF81ppxI17fDzBhY2zVC0xLm1XwFnmsqp7SbYdDnVlni+nFdSH8ZSHiVxaV1hVOzKAlEWPy/UIO2kqgsGqKj1Ou1QUYNW9oAEVm84/HoQQTjLVdKQ75xK1KK2MY/Q5lnKVa3kJtoF4YvQpfXNSyHzal5jEZzbutAic2sPK3N/0apBUjg19dCzjuu0MZsvvozg2spw1DcUTRV+3uck3VgMvXtUne3aDuBjyFFDXdp5xDItH8FSWTVm14yEyeWCgWoL7ypJnBrwGgh2bX9VnmtbDUiITblQsBe6wXIAY2rh/V1Db7ZBc9SELu1uV1kT/xvS5588Q1DN0B2ZWytDUOTG6XX+vPPUBBdQtNCzCQ51t7g/mrSGsmTT5bN1VUUIVob3Be3Jj7nwXpsxLBK4u/JGzaGVDN3CW9LV9jaXeTWFb19IOgKz2PeFheixawrdj/hYhIz3DjtKZF7Py+GGOLUkm/Nq4ZJpeSFhTWkMffNYqOjv+7ZFaiBWGDq0qD1zjqJF0AjMdVPZJ0Gpq+0dzturP74XCBp5ujDk3DYeOxY637bdVVAn6jUnGiJqo0nnCMG5q9l8sRbYjruJvnpR5PiUbct3BY52L0K511CbF/9ERH+bzRdN2ec3KE50o6TYewDFx/0g57ZLTJDj4b0xSw2v1euQMinJRTs4s/lil8ur0PpK54pWlPdOuBF85yo5XEarGg61wlCFNs/nnnfUd/xwzz0LmDwfrDwq4d4DMF3tvQXntlVMXEhf5NlPgaVdaFWJuqgNWvNJ/2/Lc8EVBcG43C6eG0RDttWO7yRaGXaLHFzNgBxnJ7h7TriB19aRbIm33hEyajFkjtqYXVrJesfcU6yE1wOwOUEI3tUgscNPNVGriNGxsHXQviDU60rtgJxs7c/em980sKMUhqKicZBA0Cj2vgltLINLY7qOT5T25+wMY2876P3b8hI1QYtqzdDzsDKGE1TYOHzUdpAqLby5U4jEbWk0wUTv5kgyJgKUQ6cguMvg0hxdwhWz/TnaG88X75uSr1MbtJCdw866iJ0Iis+101PUVoY51w49bqsK3Uai9LhSBHJLkrnCJXJpjjsd3xVqAuDzJM4l9HouiLqoDbby2TMHdhx6EbPD065hRRdR2t5PGoZKwnPkh6+SLtGHcBHjleD1y+LSStrOIXJuQxxuTLfmXYXhu1AAhZ5KjRcPez5/hx3bbt8CAgskuvrZtZKr2mPOpaFUkocRDos8WwNXnpE8qZhbzlWRiJpLd/i9yLOQ15zzRb0sLq3kXkv+WOzOJgecdxeDUYSfYkFrCTubWGXX5po9fjZh7sSM3+utwKV17Y0QYxFFFIYighizhbcSqcu3vuKml8vGZ8dJcG69b1iCXms+7z0KUROFnmDqRVmn6doGXZcPFjPpXFPX91EX/UpSLsoOMOc4im6yHYyt0H6sNIWZqZoAxAjVg1ZWe8NPQYtqqVPbE2xcHIPGIvmYx4f3Y3gpaDXkqg1eB4Sho9p2roExduPtwpV7HSGLKOysjsBEVdfeu15mlqRdEyfjvlEO2YOOn49TS77yyVvFRe0ZFshz5UoCCjg+W4LUmJAOIyTYvDiVU1sWUXOrgP/gci9I8DmN4p5gRbF+rFKeW223FjSGmKIGhZ/sfMSboCjSFwaKNi/2eJ40DN0MCEOjbl4sQdDCewjq/d8geI4KFYi6qCGhO3RuWZA1d8JXd2pQCIiufPJFLElp0GarJ3yDNy/2hVt3SzZxPvRsdBl782IJy+LSLhv6v0lAG0rWnRnS6keyqq3p1tRFLXl5lOtkyy26h2bfo8401ZaBkjB0pS+VRdD9IlXoOcaNYJpQDb0Eju+jiAnOraRD7yutZNzQTsGxwk9xOkdF2Ibq8vHcs0A8yfFh5ytpn/SoJwwd+yLBMoiatksrgZKbK2kcyfd04NBZIxk3OIztFDUOWQYrZGdh02ryGMIuN5jsRHB80NB8X3gsDjs6GI/dqY2pxXgb29jLepHuBTrUudUQ+OAx9Dm1wbfE4w62Gk0efXDjfhCwQfAQ5WO9YttB1yIMclc+T7jymcoRoryobLSszQEYypXnFDl2l9INYnjl95nkPRBh7BO1UezITtzkkV0b2vq6i/fszr5kd+hL8uPDq6aS1dBHnDJT5yhwtetc0JYZAb2wU/BsNl9EOxYsDk8Cv/95pVTqVeC5vRR0p/0El9ZCRP8Cz92/kRvFrevr69Z/5GLx4HKb2XxxK/Q1IXAI9Ugh9cOtKp6iGwODx+eCt/MT4UrChN1GHtTTSjhtYsNjDuYsoitpJWB8Ev5CRH8nov/yowsnNq98mxcm/P6N4yry7InPuXULBNrfice+7hkKf+DxQ+68U9SWAa7zLOtDu0TutOLyTgMdmWEYS8LSi5phGEaV0M2MDcMwRo2JmmEYk8JEzTCMSWGiZhjGpDBRMwxjUpioGYYxKUzUDMOYFCZqhmFMChM1wzAmhYmaYRiTwkTNMIxJYaJmGMakMFEzDGM6ENH/AAr0vbPDX8xvAAAAAElFTkSuQmCC"></img>'
            // html: '<spam class="login-titulo-alt">Tsk</spam>Plan'
        }, {
            xtype: 'container',
            cls: 'login-tela-container',
            padding: 16,
            shadow: true,
            defaults: {
                width: '100%'
            },
            items: [{
                html: [
                    '<div style="overflow: hidden; height: 0px;background: transparent;" data-description="dummyPanel for Chrome auto-fill issue">',
                    '<input type="text" style="height:0;background: transparent; color: transparent;border: none;" data-description="dummyEmail"></input>',
                    '<input type="password" style="height:0;background: transparent; color: transparent;border: none;" data-description="dummyPassword"></input>',
                    '</div>'
                ]
            }, {
                xtype: 'container',
                items: [{
                    xtype: 'textfield',
                    reference: 'txtEmailDummy',
                    label: 'Email',
                    bind: '{Email}',
                    placeholder: 'Usuario@Dominio.com.br'
                }],
                hidden: true
            }, {
                xtype: 'container',
                items: [{
                    xtype: 'textfield',
                    reference: 'txtEmail',
                    label: 'Email',
                    bind: '{Email}',
                    placeholder: 'Usuario@Dominio.com.br'
                }]
            }, {
                xtype: 'passwordfield',
                reference: 'txtPass',
                label: 'Senha',
                bind: '{Senha}',
            }, {
                xtype: 'button',
                text: 'login',
                ui: 'raised action',
                margin: '10 0',
                handler: 'onClickLogin'
            }, {
                xtype: 'button',
                text: 'Esqueci minha senha',
                handler: 'onClickEsqueciASenha'
            }, {
                xtype: 'label',
                style: 'color: red; text-align: center;',
                bind: '{loginMessage}'
            }]
        }]
    }]
});