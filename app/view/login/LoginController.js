/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.login.LoginController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.login-tela',

    init: function () {
        var me = this;

    },

    onClickLogin: function () {
        var me = this,
            view = me.getView(),
            references = view.getReferences(),
            viewModel = view.getViewModel(),
            refs = me.getReferences(),
            exit = false;

        if (viewModel.get('loading'))
            return;

        viewModel.set('loginMessage', '');

        if (!refs.txtEmail.getValue() || refs.txtEmail.getValue() === '') {
            references.txtEmail.toggleInvalidCls(true);
            exit = true;
        } else
            references.txtEmail.toggleInvalidCls(false);

        if (!refs.txtPass.getValue() || refs.txtPass.getValue() === '') {
            references.txtPass.toggleInvalidCls(true);
            exit = true;
        } else
            references.txtPass.toggleInvalidCls(false);

        if (exit) return;

        viewModel.set('loadingLogin', true);

        TskPlan.Mobile.lib.SessionManager.login(viewModel.get('Email'),
            viewModel.get('Senha'),
            Ext.bind(me.onLoginSuccess, me),
            Ext.bind(me.onLoginFailure, me)
        );
    },

    onClickEsqueciASenha: function () {
        var me = this,
            view = me.getView(),
            references = view.getReferences();

    },

    onLoginSuccess: function () {
        var me = this,
            view = me.getView(),
            viewModel = view.getViewModel(),
            refs = me.getReferences();

        viewModel.set('loading', false);

        location.reload();
    },

    onLoginFailure: function (failureText) {
        var me = this,
            view = me.getView(),
            viewModel = view.getViewModel(),
            failure = '';

        if (failureText && failureText !== "")
            failure = JSON.parse(failureText);

        viewModel.set({
            'loading': false,
            'loginMessage': (failure && failure.message) ? failure.message : 'Não foi possivel realizar login.'
        });
    }
});