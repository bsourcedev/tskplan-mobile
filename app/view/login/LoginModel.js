/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.login.LoginModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.login-tela',

    data: {
        Email:'',
        Senha:''
    }
});