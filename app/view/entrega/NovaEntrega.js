/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.entrega.NovaEntrega', {
    extend: 'Ext.Panel',
    xtype: 'nova-entrega-tela',
    requires: [],
    layout: 'vbox',
    scrollable: 'y',

    controller: 'nova-entrega-tela',
    viewModel: 'nova-entrega-tela',

    // bodyCls: 'tela-background',

    items: [{
        xtype: 'formpanel',
        height: '100%',
        bodyPadding: 10,
        width: '100%',
        flex: 1,
        defaults: {
            xtype: 'textfield',
            width: '100%',
            maxHeight: 80,
            margin: '-10 0 0 0'
        },
        items: [{
            xtype: 'datefield',
            label: 'Data de entrega',
            dateFormat:'d/m/Y',
            reference: 'dtEntrega',
            name: 'Data',
            value: new Date(),
            bind: {
                value: '{Data}'
            }
        }, {
            label: 'Pedido',
            name: 'PedidoCompraId',
            readOnly:true,
            bind: {
                value: '{PedidoCompraId}'
            }
        }, {
            xtype: 'button',
            width: '100%',
            margin: '0 0 10 0',
            ui: 'action raised',
            text: 'Selecionar Pedido',
            handler: 'onClickSelecionarPedido',
            bind: {
                hidden: '{hidePedidos}'
            }
        }, {
            label: 'Obra',
            name: 'Obra',
            readOnly:true,
            bind: {
                value: '{Obra}',
                hidden: '{hideObra}'
            }
        }, {
            xtype: 'panel',
            layout: 'hbox',
            items: [{
                xtype: 'textfield',
                label: 'Nota Fiscal',
                name: 'NotaFiscal',
                flex: 1,
                bind: {
                    value: '{NotaFiscal}'
                }
            }, {
                xtype: 'textfield',
                label: 'Serie',
                name: 'Serie',
                width: 65,
                margin: '0 0 0 5',
                bind: {
                    value: '{Serie}'
                }
            }]
        }, {
            xtype: 'textareafield',
            label: 'Observação',
            name: 'Observacao',
            maxHeight: 120,
            height: 120,
            margin: 0,
            bind: {
                value: '{Observacao}'
            }
        }, {
            xtype: 'component',
            flex: 1,
            maxHeight: null
        }, {
            xtype: 'panel',
            layout: 'vbox',
            docked: 'bottom',
            bodyPadding: 10,
            items: {
                xtype: 'button',
                ui: 'action raised',
                text: 'Produtos',
                width: '100%',
                handler: 'onClickProdutos',
                bind: {
                    hidden: '{hideProdutos}'
                }
            }
        }, {
            xtype: 'panel',
            layout: 'vbox',
            docked: 'bottom',
            bodyPadding: 10,
            items: {
                xtype: 'button',
                ui: 'action raised',
                text: 'Salvar',
                width: '100%',
                handler: 'onClickSalvar'
            }
        }]
    }],

    // listeners: {
    //     painted: 'onActivate'
    // }
});