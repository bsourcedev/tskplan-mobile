/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.entrega.ProdutoModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.entrega-produto',

    data: {
        Sheet: {
            ProdutoId: null,
            Quantidade: null,
            Observacao: null,
            Record: null
        }
    },

    formulas: {
        readOnlyProduto: function (get) {
            return get('Sheet.Record') ? true : false;
        },
        sheetTitle: function (get) {
            return get('Sheet.Record') ? 'Editar Produto' : 'Adicionar Produto';
        }
    },

    stores: {
        Store: {
            type: 'entrega-produto',
        },
        ProdutosLista: {
            type: 'entrega-produto-lista',
        }
    }
});