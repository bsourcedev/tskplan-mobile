/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.entrega.SeletorPedido', {
    extend: 'Ext.Panel',
    xtype: 'entrega-seletor-pedido',
    requires: [],
    layout: 'vbox',

    controller: 'entrega-seletor-pedido',
    viewModel: 'entrega-seletor-pedido',


    bodyCls: 'tela-background',

    items: [{
        xtype: 'panel',
        bodyPadding: 20,
        layout: 'hbox',
        items: [{
            xtype: 'combobox',
            label: 'Fornecedor',
            reference: 'cmbFornecedor',
            displayField: 'Nome',
            valueField: 'FornecedorId',
            width: '100%',
            flex: 1,
            forceSelection: true,
            margin: '-10 0 10 0',
            bind: {
                store: '{StoreFornecedor}'
            },
            listeners: {
                change: 'onFornecedorChange'
            }
        }, {
            xtype: 'button',
            iconCls: 'x-fas fa-times',
            handler: 'cleanFornecedor',
            bind: {
                hidden: '{!showCleanButton}'
            }
        }]
    }, {
        xtype: 'component',
        cls: 'divisor',
    }, {
        xtype: 'list',
        bodyCls: 'tela-background',
        padding: '0 10',
        width: '100%',
        flex: 1,
        bind: {
            store: '{Store}'
        },
        itemCls: 'entrega-pedido-card x-shadow',
        itemContentCls: 'entrega-pedido-content',
        itemTpl: [
            '<spam style="font-size: 12px;color: #9E9E9E;">Pedido #{PedidoCompraId}</spam>',
            '<div class="card-titulo" style="margin: 8px 0">{Fornecedor}</div>',
            '<div style="text-align: center;">{[Ext.util.Format.number(values.Porcentagem,"0.##")]}%</div>',
            '{[TskPlanManager.progressBar(values.Porcentagem)]}',
            '{Obra}'
        ],
        listeners: {
            select: 'onSelect'
        }
    }],

    // listeners: {
    //     painted: 'onActivate'
    // }
});