/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.entrega.EntregaController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.entrega-tela',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();

    },

    onClickNovaEntrega: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();

        ScreenManager.abrirTela({
            screen: Ext.create('TskPlan.Mobile.view.entrega.NovaEntrega', {
                dataEntrega: new Date(),
                store: viewModel.get('Store')
            }),
            anterior: view,
            title: 'Nova Entrega'
        });
    },

    onSelect: function (list, selected) {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();

        ScreenManager.abrirTela({
            screen: Ext.create('TskPlan.Mobile.view.entrega.NovaEntrega', {
                dataEntrega: new Date(),
                store: viewModel.get('Store'),
                record: selected
            }),
            anterior: view,
            title: 'Nova Entrega'
        });
    },

    onDateChange: function (dtpicker, newValue, oldValue) {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            store = viewModel.get('Store');

        viewModel.set('showCleanButton', newValue ? true : false);

        store.getProxy().extraParams.data = newValue ? Ext.Date.format(newValue, 'd/m/Y') : '';

        store.load();
    },

    onEntregasChange: function (chk, newValue, oldValue) {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            store = viewModel.get('Store');

        store.getProxy().extraParams.processados = newValue;

        store.load();
    },

    cleanData: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            refs = view.getReferences();

        // viewModel.set('showCleanButton', false);

        refs.dtEntrega.clearValue();
    }
});