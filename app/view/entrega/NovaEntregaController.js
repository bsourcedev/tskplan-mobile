/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.entrega.NovaEntregaController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.nova-entrega-tela',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            record = view.getRecord();

        viewModel.set('Data', view.dataEntrega);

        if (record) {
            viewModel.set({
                Data: record.get('Data'),
                PedidoCompraId: record.get('PedidoCompraId'),
                ObraId: record.get('ObraId'),
                Obra: record.get('Obra'),
                NotaFiscal: record.get('NotaFiscal'),
                Serie: record.get('Serie'),
                Observacao: record.get('Observacao'),
                Record: record
            });
        }
    },

    onClickSelecionarPedido: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            component = Ext.create('TskPlan.Mobile.view.entrega.SeletorPedido');

        component.on({
            scope: me,
            pedidoSelecionado: me.onPedidoSelecionado
        });

        ScreenManager.abrirTela({
            screen: component,
            anterior: view,
            title: 'Selecionar Pedido'
        });
    },

    onPedidoSelecionado: function (data) {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();

        viewModel.set({
            PedidoCompraId: data.PedidoCompraId,
            FornecedorId: data.FornecedorId,
            Fornecedor: data.Fornecedor
        });
    },

    onClickProdutos: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            component = Ext.create('TskPlan.Mobile.view.entrega.Produto', {
                EntregaId: viewModel.get('Record').get('EntregaId')
            });

        ScreenManager.abrirTela({
            screen: component,
            anterior: view,
            title: 'Entrega - Produtos'
        });
    },

    onClickSalvar: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            store = view.store,
            record;

        if (!viewModel.get('Record')) {
            record = Ext.create('TskPlan.Mobile.model.entrega.Model', {
                Data: viewModel.get('Data') ? viewModel.get('Data') : new Date(),
                PedidoCompraId: viewModel.get('PedidoCompraId'),
                Observacao: viewModel.get('Observacao'),
                NotaFiscal: viewModel.get('NotaFiscal'),
                Serie: viewModel.get('Serie')
            });

            store.add(record);
        } else {
            record = viewModel.get('Record');

            record.set({
                Data: viewModel.get('Data') ? viewModel.get('Data') : new Date(),
                PedidoCompraId: viewModel.get('PedidoCompraId'),
                Observacao: viewModel.get('Observacao'),
                NotaFiscal: viewModel.get('NotaFiscal'),
                Serie: viewModel.get('Serie')
            });
        }

        if (store.getModifiedRecords().length > 0) {
            view.mask('Salvando entrega');

            store.sync({
                success: function () {
                    Ext.toast('Salvo com sucesso', 3000);
                    viewModel.set('Record', record);
                    view.unmask();
                },
                failure: function () {
                    Ext.toast('Não foi possivel salvar', 3000);
                    store.rejectChanges();
                    view.unmask();
                }
            });
        }
    }
});