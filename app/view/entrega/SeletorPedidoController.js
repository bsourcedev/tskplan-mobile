/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.entrega.SeletorPedidoController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.entrega-seletor-pedido',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();

    },

    onSelect: function (list, selected) {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();

        view.fireEvent('pedidoSelecionado', {
            PedidoCompraId: selected.get('PedidoCompraId'),
            FornecedorId: selected.get('FornecedorID'),
            Fornecedor: selected.get('Fornecedor')
        });

        //<debug>
        console.log(`Pedido Selecionado: ${selected.get('PedidoCompraId')} - ${selected.get('Fornecedor')}`);
        //</debug>  

        ScreenManager.voltar();
    },

    onFornecedorChange: function (combo, newValue, oldValue) {
        var me = this;

        me.loadStore(newValue);
    },

    cleanFornecedor: function () {
        var me = this,
            view = me.getView(),
            refs = view.getReferences();

        refs.cmbFornecedor.clearValue();
        me.loadStore(0);
    },

    loadStore: function (fornecedorId) {
        var me = this,
            viewModel = me.getViewModel(),
            store = viewModel.get('Store');

        viewModel.set('showCleanButton', fornecedorId == 0 ? false : true);

        store.getProxy().extraParams = {
            FornecedorId: fornecedorId
        };

        store.load();
    }
});