/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.entrega.NovaEntregaModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.nova-entrega-tela',

    data: {
        Data: null,
        PedidoCompraId: null,
        ObraId: null,
        Obra: null,
        NotaFiscal: null,
        Serie: null,
        Observacao: null,
        Record: null
    },

    formulas: {
        hideObra: function (get) {
            return get('ObraId') ? false : true;
        },
        hidePedidos: function (get) {
            return (get('Record') && get('Record.PedidoCompraId')) ? true : false;
        },
        hideProdutos: function (get) {
            return get('Record') ? false : true;
        }
    },

    stores: {
        Store: {
            type: 'entrega-tela',
        }
    }
});