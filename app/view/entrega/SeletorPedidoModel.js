/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.entrega.SeletorPedidoModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.entrega-seletor-pedido',

    data: {
        showCleanButton: false
    },

    stores: {
        Store: {
            type: 'entrega-pedido',
        },
        StoreFornecedor: {
            type: 'entrega-fornecedor',
        }
    }
});