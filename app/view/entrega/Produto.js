/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.entrega.Produto', {
    extend: 'Ext.Panel',
    xtype: 'entrega-produto',
    requires: [],
    // layout: 'vbox',

    controller: 'entrega-produto',
    viewModel: 'entrega-produto',

    bodyCls: 'tela-background',
    padding: '0 10',

    items: [{
        xtype: 'list',
        bodyCls: 'tela-background',
        width: '100%',
        height: '100%',
        flex: 1,
        bind: {
            store: '{Store}'
        },
        itemCls: 'entrega-produto-card x-shadow',
        itemContentCls: 'entrega-produto-content',
        itemTpl: [
            '<div class="card-titulo" style="display: contents;">{Produto}</div> x{Quantidade}',
            "<tpl if='Observacao != \"\"'>",
            '<div class="card-descricao">{Observacao}</div>',
            "</tpl>",
            "<tpl if='Pedido'>",
            '<div class="card-descricao">Produto solicitado no pedido</div>',
            "</tpl>"
        ],
        listeners: {
            select: 'onSelect'
        }
    }, {
        xtype: 'button',
        text: 'Adicionar',
        iconCls: 'x-fa fa-plus',
        floating: true,
        renderTo: Ext.getBody(),
        ui: 'action raised round shadow',
        height: 40,
        // width:'100%',
        style: 'position: absolute; bottom: 10px; right: 10px;',
        handler: 'onClickAdicionar'
    }],

    listeners: {
        activate: 'onActive'
    }
});