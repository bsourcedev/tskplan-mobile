/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.entrega.ProdutoController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.entrega-produto',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();

        me.EntregaId = view.EntregaId;

        viewModel.get('ProdutosLista').load();
    },

    onClickAdicionar: function () {
        var me = this;

        me.createMenu();
    },

    createMenu: function () {
        var me = this;

        me.menu = me.menu ? me.menu : Ext.create({
            xtype: 'actionsheet',
            viewModel: me.getViewModel(),
            hideOnMaskTap: false,
            side: 'bottom',
            reveal: false,
            cover: true,
            hideAnimation: 'slideOut',
            items: [{
                xtype: 'component',
                cls: 'card-titulo',
                html: 'Adicionar Produto'
            }, {
                xtype: 'combobox',
                label: 'Produto',
                height: 80,
                margin: '0 0 -10 0',
                valueField: 'ProdutoId',
                displayField: 'Produto',
                bind: {
                    store: '{ProdutosLista}',
                    value: '{Sheet.ProdutoId}',
                    readOnly: '{readOnlyProduto}'
                }
            }, {
                xtype: 'textfield',
                label: 'Quantidade',
                height: 80,
                margin: '0 0 -10 0',
                bind: {
                    value: '{Sheet.Quantidade}'
                }
            }, {
                xtype: 'textareafield',
                label: 'Observacao',
                flex: 1,
                bind: {
                    value: '{Sheet.Observacao}'
                }
            }, {
                text: 'Confirmar',
                scope: me,
                ui: 'action',
                handler: 'onSheetClickConfirmar'
            }, {
                text: 'Cancelar',
                scope: me,
                handler: 'onSheetClickCancelar'
            }]
        });

        me.menu.show();
    },

    onSheetClickConfirmar: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            store = viewModel.get('Store'),
            sheetData = viewModel.get('Sheet'),
            record;

        //<debug>
        console.log('Salvando dados do produto');
        console.log(sheetData);
        //</debug>

        if (!sheetData.Record) {
            var rec = Ext.create('TskPlan.Mobile.model.entrega.Produto');

            rec.set({
                EntregaId: me.EntregaId,
                ProdutoId: sheetData.ProdutoId,
                Quantidade: sheetData.Quantidade,
                Observacao: sheetData.Observacao,
            });

            store.add(rec);
        } else {
            record = sheetData.Record;

            record.set({
                Quantidade: sheetData.Quantidade,
                Observacao: sheetData.Observacao,
            });
        }

        store.sync();

        me.hideMenu();
    },

    onSheetClickCancelar: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel();

        me.hideMenu();
    },

    hideMenu: function () {
        var me = this,
            menu = me.menu;

        me.clearSheetData();

        //criar override global para os sheets que removam o viewmodel antes de fechar
        menu.setViewModel();

        menu.hide();
        // menu.close();
    },

    clearSheetData: function () {
        var me = this,
            viewModel = me.getViewModel();

        viewModel.set({
            ProdutoId: null,
            Quantidade: null,
            Observacao: null,
            Record: null
        });
    },

    onSelect: function (list, selected) {
        var me = this,
            viewModel = me.getViewModel();

        viewModel.set({
            Sheet: {
                ProdutoId: selected.get('ProdutoId'),
                Quantidade: selected.get('Quantidade'),
                Observacao: selected.get('Observacao'),
                Record: selected
            }
        });

        me.createMenu();
    },

    onActive: function () {
        var me = this,
            viewModel = me.getViewModel(),
            store = viewModel.get('Store');

        store.getProxy().extraParams = {
            EntregaId: me.EntregaId,
        };

        store.load();
    }
});