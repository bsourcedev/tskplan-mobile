/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.entrega.EntregaModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.entrega-tela',

    data: {
        showCleanButton: false
    },

    formulas: {
        // lblObra: function (get) {
        //     var lbl = get('Obra');

        //     return lbl ? lbl : 'Selecionar';
        // }
    },

    stores: {
        Store: {
            type: 'entrega-tela',
        }
    }
});