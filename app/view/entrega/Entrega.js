/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.entrega.Entrega', {
    extend: 'Ext.Panel',
    xtype: 'entrega-tela',
    requires: [],
    layout: 'vbox',

    controller: 'entrega-tela',
    viewModel: 'entrega-tela',


    bodyCls: 'tela-background',

    items: [{
        xtype: 'panel',
        bodyPadding: 20,
        layout: 'vbox',
        items: [{
            xtype: 'container',
            layout: 'hbox',
            items: [{
                xtype: 'datefield',
                label: 'Data de entrega',
                reference: 'dtEntrega',
                // value: new Date(),
                dateFormat: 'd/m/Y',
                width: '100%',
                flex: 1,
                margin: '-15 0 10 0',
                listeners: {
                    change: 'onDateChange'
                }
            }, {
                xtype: 'button',
                iconCls: 'x-fas fa-times',
                handler: 'cleanData',
                bind: {
                    hidden: '{!showCleanButton}'
                }
            }]
        }, {
            xtype: 'checkbox',
            width: 180,
            margin: '-10 0 -5 0',
            boxLabel: 'Exibir Entregas Processadas',
            listeners: {
                change: 'onEntregasChange'
            }
        }]
    }, {
        xtype: 'component',
        cls: 'divisor',
    }, {
        xtype: 'list',
        bodyCls: 'tela-background',
        padding: '0 10',
        width: '100%',
        flex: 1,
        bind: {
            store: '{Store}'
        },
        itemCls: 'entrega-card x-shadow',
        itemContentCls: 'entrega-content',
        itemTpl: [
            '<spam class="entrega-id">#{EntregaId}</spam>',
            '<div class="card-titulo entrega-nome" style="display: contents;">{FornecedorLabel}</div>',
            // '<div style="text-align: center;">{Porcentagem}%</div>',
            // '{[TskPlanManager.progressBar(values.Porcentagem)]}'
            '<spam class="entrega-icone">{IconEstoque}</spam>',
            '<spam class="entrega-data">{DataFormatada}</spam>'
        ],
        listeners: {
            select: 'onSelect'
        }
    }, {
        xtype: 'button',
        text: 'Nova Entrega',
        iconCls: 'x-fa fa-plus',
        floating: true,
        renderTo: Ext.getBody(),
        ui: 'action raised round shadow',
        height: 40,
        style: 'position: absolute; top: 115px; right: 10px;',
        handler: 'onClickNovaEntrega'
    }],

    // listeners: {
    //     painted: 'onActivate'
    // }
});