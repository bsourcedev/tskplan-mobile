/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.notificacoes.NotificacoesController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.notificacoes-tela',

    init: function () {
        var me = this,
            view = me.getView(),
            viewModel = me.getViewModel(),
            store = viewModel.get('Store');

        viewModel.set('Data', store.data.items);
    },

    onPainted: function () {
        if (window.ScreenManager)
            setTimeout(function () {
                ScreenManager.setTitulo('Notificações');
            }, 150);
    }

});