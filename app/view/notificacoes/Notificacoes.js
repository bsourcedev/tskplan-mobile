/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.notificacoes.Notificacoes', {
    extend: 'Ext.dataview.List',
    xtype: 'notificacoes-tela',
    requires: [],

    controller: 'notificacoes-tela',
    viewModel: 'notificacoes-tela',

    itemCls: 'card-notificacao',

    // padding: 10,
    height: '100%',
    flex: 1,

    scrollable: 'y',

    itemTpl: [
        '<div>',
        '<div {Hide} class="nova-notificacao"></div>',
        '<div class="area-notificacao">',
        '   <b>{Titulo}</b>',
        '   <br>',
        '   {Descricao}',
        '   <br>',
        '   <spam class="stamp-notificacao">{Stamp}</spam>',
        '</div>',
        '</div>',
    ],

    listeners: {
        painted: 'onPainted'
    },

    bind: {
        data: '{Data}'
    }
});