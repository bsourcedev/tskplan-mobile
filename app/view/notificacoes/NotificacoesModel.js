/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.notificacoes.NotificacoesModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.notificacoes-tela',

    data: {
        Data:[]
    },

    stores: {
        Store: {
            type: 'notificacoes-tela',
        }
    }
});