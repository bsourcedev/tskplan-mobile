/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.home.HomeController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.home-tela',

    onPainted: function () {
        if (window.ScreenManager)
            setTimeout(function () {
                ScreenManager.setTitulo('Inicio');
            }, 150);
    },

    onObraSelecionada: function (obra) {
        var me = this,
            view = me.getView(),
            tabView = view.up(),
            component = Ext.create('TskPlan.Mobile.view.checklist.View', {
                ObraId: obra.ObraId,
                Obra: obra.Obra,
                Sigla: obra.Sigla
            });

        ScreenManager.abrirTela({
            screen: component,
            anterior: mainTabView,
            title: 'Checklist'
        });
    }
});