/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.home.Home', {
    extend: 'Ext.Panel',
    xtype: 'home-tela',
    requires: [],
    controller: 'home-tela',

    scrollable: 'y',
    
    layout: 'fit',
    height: '100%',
    flex: 1,
    width: '100%',

    bodyCls: 'tela-background',

    items: [{
        xtype: 'obra-tela',
        height: '100%',
        width: '100%',
        flex: 1,
        voltarAposSelecao: false,
        listeners: {
            obraSelecionada: 'onObraSelecionada'
        }
    }],

    listeners: {
        painted: 'onPainted'
    }
});