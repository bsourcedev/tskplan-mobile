/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.menu.MenuController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.menu-tela',

    menu: [{
            group: 'Tarefas',
            items: [{
                icon: 'x-fa fa-tasks',
                text: 'Checklist',
                class: 'TskPlan.Mobile.view.checklist.View'
            }]
        }, {
            group: 'Pedidos',
            items: [{
                icon: 'x-fa fa-truck',
                text: 'Entregas',
                class: 'TskPlan.Mobile.view.entrega.Entrega'
            },{
                icon: 'x-fa fa-calendar-alt',
                text: 'Agenda de Entregas',
                class: 'TskPlan.Mobile.view.agendaEntrega.AgendaEntrega'
            }]
        }, {
            group: 'Tarefas',
            items: [{
                icon: 'x-fa fa-list',
                text: 'Tarefas',
                class: 'TskPlan.Mobile.view.tarefa.TarefaLista'
            }]
        }, 
        {
            group: 'Chat',
            items: [{
                icon: 'x-fa fa-comments',
                text: 'Conversas',
                class: 'TskPlan.Mobile.view.chat.ChatList'
            }]
        }
        //<debug>
        // , {
        //     group: 'Testes',
        //     items: [{
        //         icon: 'x-fa fa-file',
        //         text: 'Entrega - Produtos',
        //         class: 'TskPlan.Mobile.view.entrega.Produto'
        //     },{
        //         icon: 'x-fa fa-file',
        //         text: 'Entrega - Seletor Pedido',
        //         class: 'TskPlan.Mobile.view.entrega.SeletorPedido'
        //     }]
        // }
        //</debug>
    ],

    init: function () {
        var me = this,
            viewModel = me.getViewModel();

        viewModel.set('usuario', (TSKPLAN_SESSION && TSKPLAN_SESSION.token && TSKPLAN_SESSION.token.nameid) ? TSKPLAN_SESSION.token.nameid : 'Nome do Usuario');

        me.onActivate();
    },

    onActivate: function () {
        var me = this;

        me.prepareMenu(me.menu);
    },

    onPaint: function () {
        if (window.ScreenManager)
            ScreenManager.setTitulo('Menu');
    },

    prepareMenu: function (menuGroup) {
        var me = this,
            view = me.getView(),
            references = view.getReferences();

        menuGroup.forEach(function (menu) {
            references.menu.add({
                cls: 'menu-title',
                html: menu.group,
                groupData: menu
            });

            menu.items.forEach(function (item) {
                references.menu.add({
                    xtype: 'button',
                    cls: 'menu-item',
                    width: '100%',
                    textAlign: 'left',
                    iconCls: `button-small-icon ${item.icon}`,
                    text: item.text,
                    // html: `<i class="${item.icon}"></i> ${item.text}`,
                    itemData: item,
                    listeners: {
                        tap: me.onTapMenu
                    }
                });
            });
        });
    },

    onTapMenu: function (button) {
        var me = this,
            menu = me.up('menu-tela'),
            tabView = menu.up(),
            component;

        if (button.itemData && button.itemData.class) {
            component = Ext.create(button.itemData.class)

            menu.getController().abrirTela(component, button.itemData.text);
        }
    },

    onAcessarPerfilTap: function () {
        var me = this,
            view = me.getView(),
            tabView = view.up(),
            component = Ext.create('TskPlan.Mobile.view.perfil.Perfil');

        me.abrirTela(component, 'Perfil');
    },

    abrirTela: function (tela, title) {
        var me = this,
            view = me.getView(),
            tabView = view.up();


        // tabView.setActiveItem(tabView.items.findIndex('id', tela.getId()));

        ScreenManager.abrirTela({
            screen: tela,
            anterior: view,
            title: title
        });
    }
});