/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('TskPlan.Mobile.view.menu.MenuModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.menu-tela',

    data: {
        usuario: 'Nome do Usuario',
    }

    //TODO - add data, formulas and/or methods to support your view
});
