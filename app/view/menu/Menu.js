/**
 * This view is an example list of people.
 */
Ext.define('TskPlan.Mobile.view.menu.Menu', {
    extend: 'Ext.Panel',
    xtype: 'menu-tela',
    controller: 'menu-tela',
    viewModel: 'menu-tela',
    requires: [
        'Ext.Toast'
    ],

    items: [{
        xtype: 'panel',
        layout: 'hbox',
        reference: 'login',
        items: [{
            cls: 'menu-usuario-foto',
            reference: 'login-foto',
            pack: 'center'
        }, {
            reference: 'login-nome',
            cls: 'menu-usuario-nome',
            bind: {
                html: '{usuario}<br><spam class="menu-usuario-perfil">Acessar perfil</spam>'
            },
            listeners: {
                element: 'element',
                tap: 'onAcessarPerfilTap'
            }
        }]
    }, {
        xtype: 'panel',
        reference: 'menu',
        defaults: {
            xtype: 'component'
        },
        items: []
    }],

    listeners: {
        painted: 'onPaint',
    }
});