Ext.define('TskPlan.overrides.Store', {
    override: 'Ext.data.Store',
    loadApiWhenEmpty: true,

    config: {
        //Define se a store utiliza ou não iDB
        useIDB: false,
        //True para carregar automaticamente dados da API quando não houver dados salvos no iDB
        loadApiWhenEmpty: true,
        //proxy da API
        apiProxy: null,
        //Data da ultima sincronização
        lastSyncDate: null,
        //Sincronizar dados com a api quando a store for criada
        syncOnCreate: false,
        //Stores fantasmas não são controladas pela api (utilizdas para controles internos do app)
        iDBPhantomStore: false,
        //Marcação para dizer se a store precisa ser sincronizada
        isDirt: false
    },

    constructor: function () {
        var me = this;

        me.callParent(arguments);

        me.setApiProxy(me.getProxy());

        if (me.getUseIDB()) {
            me.setProxy(me.model.getProxy());
        }

        if (!me.autoLoad && me.getSyncOnCreate() && !me.getIDBPhantomStore()) {
            me.load();
            console.warn('Devera dar sync com a api e nao load pois algumas stores podem nao ter a config para sync com api no load');
            // me.syncApiData(true);
        }

        me.on({
            scope: me,
            beforesync: me.onBeforeSync
        });
    },

    //Chama os metodos para carregar os dados da api e aplicar ao IndexedDB
    //dontSyncDataWithStore = quando true os dados carregados não serão aplicados diretamente a store, somente ao IDB
    syncApiData: function (dontSyncDataWithStore, callback) {
        var me = this,
            onFinish = function (response) {
                var records = response && response.Data ? response.Data : null;

                if (!dontSyncDataWithStore) {
                    //<debug>
                    console.log(`Registros Carregados na store [${me.$className}]`);
                    console.log(records);
                    //</debug>
                    if (records)
                        me.loadData(records, true);
                }

                loading = false;

                if (Ext.isFunction(callback))
                    callback(records);
            };

        DataManager.loadStoredData(me, onFinish);

        loading = true;

        //<debug>
        console.log('Verificar se a store sendo exibida foi atualizada apos o carregamento dos novos dados');
        //</debug>
    },

    onProxyLoad: function (store, records) {
        var me = this;

        if (me.getUseIDB() && me.getLoadApiWhenEmpty() && !(store.getRecords().length > 0) && !me.getIDBPhantomStore()) {
            //<debug>
            console.log('Verificar se a conexão esta disponivel antes de carregar');

            me.syncApiData();
            //</debug>
        }

        me.callParent(arguments);
    },

    //Retornar a ultima data de sync
    // -> pegar este metodo do DataManager
    getLastSyncDate: function () {
        var me = this;

        //<debug>
        console.warn('store.getLastSyncDate - Adicionar este metodo?');
        debugger;
        //</debug>
    },

    //Definir Store como Dirt para que seja sincronizada
    setDirt: function () {
        var me = this,
            proxy = me.getProxy();

        if (!me.useIDB)
            return;

        if (me.getIDBPhantomStore()) {
            //<debug>
            console.log(`[${me.$className}] setDirt cancelado - não é possivel marcar uma iDBPhantomStore como dirt`);
            //</debug>
            return;
        }

        //<debug>
        console.log(`[${me.$className}] marcada como dirt`);
        //</debug>

        DataManager.dirtStore.add({
            Store: me.$className,
            Database: proxy.getDbName(),
            Object: proxy.getObjectStoreName()
        });

        me.setIsDirt(true);
    },

    //verifica se algum registro tenha sido adicionado/sofrido alteração e envia para set marcada como dirt
    onBeforeSync: function (options, eOpts) {
        var me = this,
            operation = Object.keys(options)[0],
            optionData = options[operation];

        if (optionData.length > 0 && !me.getIDBPhantomStore()) {
            me.setDirt();

            me.addDirtRecord(operation, optionData);
        }
    },

    addDirtRecord: function (operation, records) {
        var me = this,
            operations = {
                create: 'post',
                destroy: 'delete',
                update: 'put'
            },
            operationRules = (rec, operation) => {
                if (operation == 'put' && !rec.get('isCreated'))
                    operation = 'post';

                if (rec.get('isCreated') && rec.get('isDeleted'))
                    operation = 'delete';

                if (!rec.get('isCreated') && rec.get('isDeleted'))
                    console.warn('Remover registros não criados no servidor e deletados no app da sua store');

                if (operation == 'destroy')
                    console.warn('Testar - Transformar operação destroy em update');

                console.log('operation');

                return operation;
            };

        //<debug>
        console.log(`Operação ${operation} [${(operations[operation])}]`);
        console.log(records);
        console.warn('Criar um ordem para garantir q todos sejam inseridos em ordem ou filtrar na hora de salvar');
        //</debug>

        records.forEach(rec => {
            var dirtRecord = DataManager.dirtRecordStore.findRecord('RecordGuid', rec.getId());

            rec.set('isSync', 0);

            if (!dirtRecord)
                DataManager.dirtRecordStore.add({
                    RecordGuid: rec.getId(),
                    Store: me.$className,
                    Params: me.apiProxy.getExtraParams(),
                    Operation: operationRules(rec, operations[operation])
                });
            else
                dirtRecord.set({
                    Params: me.apiProxy.getExtraParams(),
                    Operation: operationRules(rec, operations[operation])
                });
        });

        //<debug>
        console.warn('Necessario criar uma logica para dar update em stores das telas parent (tarefaLista é arente de tarefa) para que as listas offline sejam atualizads com os novos registros criados offline');
        //</debug>
        DataManager.setSyncTask(true);
    },

    //Buscar o registro IDB
    getIDBRecord: function (guid, callback, _proxy) {
        var me = this,
            request,
            db,
            proxy = _proxy ? _proxy : me.getProxy(),
            dataBase = proxy ? proxy.getDbName() : null,
            objectModel = proxy ? proxy.getObjectStoreName() : null;

        if (!dataBase) {
            callback(null);
            return;
        }

        request = window.indexedDB.open(dataBase, 1);

        //Callback for success upon DB open
        request.onsuccess = function (event) {
            db = event.target.result;

            var store = db.transaction(objectModel, 'readwrite').objectStore(objectModel);

            dataRequest = store.get(guid);

            dataRequest.onsuccess = event => {
                callback(event.target.result);
            };

            dataRequest.onerror = function (event) {
                //<debug>
                console.groupCollapsed('Não foi possivel encontrar o registro');
                console.log(event);
                console.groupEnd();
                //</debug>

                callback(null);
            };
        };
    },

    load: function (options) {
        var me = this,
            args = arguments,
            // proxyParams = me.proxy && me.proxy.extraParams ? me.proxy.extraParams : null,
            // storeQueryParams = me.queryParams ? me.queryParams : [],
            params = me.params ? me.params : me.setParamsFromExtraParams();

        if (!options)
            options = {};

        if (window.navigator.onLine && me.getUseIDB() && !me.getIDBPhantomStore() && me.getApiProxy() && !options.isSync) {

            me.setProxyParamsFromParams();

            new Ext.Promise(function (resolve, reject) {
                me.syncApiData(false, () => {
                    resolve();
                });
            }).then((coords) => {
                me.load(Object.assign(options ? options : {}, {
                    isSync: true
                }));
            });

        } else {
            if (!me.isTreeStore) {
                if (me.filters)
                    me.filters.clear();

                params.filter(param => param.value && !param.exclude).forEach(param => {
                    me.addFilter(new Ext.util.Filter({
                        property: param.field,
                        value: param.value,
                        operator: param.operator,
                        anyMatch: param.anyMatch,
                        caseSensitive: param.caseSensitive,
                        filterFn: param.filterFn
                    }), true);
                });
            }

            me.callParent(args);
        }
    },

    setParams: function (params) {
        var me = this;

        params.forEach(param => {
            if (param.exclude == undefined)
                param.exclude = false;

            if (param.field == undefined)
                param.field = TskPlanManager.util.firstLetterUppercase(param.name);

            if (param.operator == undefined)
                param.operator = '==';

            param.anyMatch = param.anyMatch ? param.anyMatch : true;
            param.caseSensitive = param.caseSensitive ? param.caseSensitive : false;
            param.filterFn = param.filterFn ? param.filterFn : null;
        });

        me.params = params;
    },

    //Define os params da store pelo q foi definido no extra params (função provisoria até q nenhum store utilize mais extraParams)
    setParamsFromExtraParams: function () {
        var me = this,
            proxyParams = me.proxy && me.proxy.extraParams ? me.proxy.extraParams : null,
            params = [];

        for (var param in proxyParams) {
            params.push({
                name: param,
                field: TskPlanManager.util.firstLetterUppercase(param),
                value: proxyParams[param],
                exclude: false,
                operator: '==',
                anyMatch: false,
                caseSensitive: false,
                filterFn: null
            });
        }

        me.params = params;

        return params;
    },

    //Defini os parametros do proxy com os parametros da store
    setProxyParamsFromParams: function () {
        var me = this,
            extraParams = {};

        me.params.forEach(param => {
            extraParams[param.name] = param.value;
        });

        if (me.proxy)
            me.proxy.extraParams = extraParams;
    }
});