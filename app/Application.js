/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('TskPlan.Mobile.Application', {
    extend: 'Ext.app.Application',

    name: 'TskPlan.Mobile',

    require: [
        'TskPlan.Mobile.lib.SessionManager',
        'TskPlan.overrides.Store',
        'Ext.data.identifier.Uuid',
        'Ext.Toast'
    ],

    quickTips: false,
    platformConfig: {
        desktop: {
            quickTips: true
        }
    },

    init: function () {
        var me = this;

        me.configureManagers();
        // me.configureFireBaseMessaging();
        me.configureRequests();
    },

    launch: function () {
        var me = this,
            validSession = TskPlan.Mobile.lib.SessionManager.validSession(),
            mainView = validSession ? Ext.create('TskPlan.Mobile.view.load.Load') : Ext.create('TskPlan.Mobile.view.login.Login');

        if (validSession) {
            mainView.on({
                scope: me,
                close: function () {
                    if (Ext.Viewport.add) {
                        Ext.Viewport.add(Ext.create('TskPlan.Mobile.view.main.Main'));
                    }
                }
            });
        }

        if (Ext.Viewport.add) {
            Ext.Viewport.add(mainView);
        }
    },

    configureRequests: function () {
        Ext.Ajax.on('beforerequest', function (conn, opt) {
            opt.headers = {
                Authorization: 'Bearer ' + (TSKPLAN_SESSION.token ? TSKPLAN_SESSION.token.hash : "")
            }
        });
    },

    configureManagers: function () {
        var me = this;

        window.NavigatorManager = TskPlan.Mobile.lib.NavigatorManager;
        window.ScreenManager = TskPlan.Mobile.lib.ScreenManager;
        window.MediaManager = TskPlan.Mobile.lib.MediaManager;
        window.TskPlanManager = TskPlan.Mobile.lib.TskPlanManager;
        window.DataManager = TskPlan.Mobile.lib.DataManager;
        window.MessageLib = TskPlan.Mobile.lib.MessageLib;
    },

    configureFireBaseMessaging: function () {
        console.log('configureFireBaseMessaging')
        // Your web app's Firebase configuration
        var firebaseConfig = {
            apiKey: "AIzaSyB03LU3prPNJXcxfVW_unFMxl19gNBt5Qs",
            authDomain: "dev-tskplan.firebaseapp.com",
            databaseURL: "https://dev-tskplan.firebaseio.com",
            projectId: "dev-tskplan",
            storageBucket: "dev-tskplan.appspot.com",
            messagingSenderId: "202214121723",
            appId: "1:202214121723:web:8cc985c72fdad1a4ae105e",
            measurementId: "G-WPM47SVCDR"
        };
        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
        firebase.analytics();
        // Retrieve Firebase Messaging object.
        const messaging = firebase.messaging();
        messaging.usePublicVapidKey("BIPfyIlacIjlUDv3fQrtMyBBxICucWl_vNY2Ec325U4Xu90JEYqc-ZjLUUBfCroSdjIBl5pnJNGtKvkO-BXxnzA");

        window.GlobalSWMessageEventListener = function (event) {
            var response = event.data;

            console.log('[app] Data received from sw: ', response);

            if (response.method == 'messageReceived')
                MessageLib.receiveMessage(response.data)
            else if (response.method == 'messageBuffer')
                MessageLib.receiveMessageBuffer(response.data)
            else if (response.method == 'userChannels')
                MessageLib.loadUserChannelsCallback(response.data)

        }
        navigator.serviceWorker.addEventListener('message', window.GlobalSWMessageEventListener);

        messaging.onMessage((payload) => {
            console.log('Message received. ', payload);
            // ...
        });

        function requestPermission() {
            console.log('Requesting permission...');
            // [START request_permission]
            Notification.requestPermission().then((permission) => {
                if (permission === 'granted') {
                    console.log('Notification permission granted.');
                    // TODO(developer): Retrieve an Instance ID token for use with FCM.
                    // [START_EXCLUDE]
                    // In many cases once an app has been granted notification permission,
                    // it should update its UI reflecting this.

                    // [END_EXCLUDE]
                } else {
                    console.log('Unable to get permission to notify.');
                }
            });
            // [END request_permission]
        }
        window.addEventListener('load', function () {
            requestPermission();
        });

        window.messaging= messaging;
    },

    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});