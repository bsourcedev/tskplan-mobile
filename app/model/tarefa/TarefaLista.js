Ext.define('TskPlan.Mobile.model.tarefa.TarefaLista', {
    extend: 'TskPlan.Mobile.model.Base',
    alias: 'model.tarefa-lista-tela',

    fields: [{
        name: 'TarefaId',
        type: 'int'
    }, {
        name: 'Nome',
        type: 'string'
    }, {
        name: 'ObraId',
        type: 'int'
    }, {
        name: 'Obra',
        type: 'string'
    }, {
        name: 'Icone',
        type: 'string'
    }, {
        name: 'Porcentagem',
        type: 'float'
    }, {
        name: 'StatusId',
        type: 'int'
    }, {
        name: 'DataInicio',
        type: 'date'
    }, {
        name: 'DataFim',
        type: 'date'
    }, {
        name: 'DataInicioFormatada',
        persist: false,
        convert: function (value, rec) {
            return Ext.util.Format.date(rec.get('DataInicio'), 'd/m/Y');
        }
    }, {
        name: 'DataFimFormatada',
        persist: false,
        convert: function (value, rec) {
            return Ext.util.Format.date(rec.get('DataFim'), 'd/m/Y');
        }
    }, {
        name: 'IconeStatus',
        persist: false,
        convert: function (value, rec) {
            var statusId = rec.get('StatusId')
            return `<i class="fa fa-circle" style="font-size: 12px;color:${(statusId == 1 ? 'var(--azul)' : statusId == 2 ? 'var(--verde)' : 'var(--amarelo)')}"></i>`;
        }
    }, {
        name: 'IconeFormatado',
        type: 'string',
        convert: function (value, rec) {
            var icone = rec ? rec.get('Icone') : null;
            return `<i class="${(icone? icone.replace('fa fa-file-o', 'fas fa-tasks') : 'fas fa-tasks')}"></i>`;
        }
    }]
});