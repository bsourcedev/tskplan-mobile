Ext.define('TskPlan.Mobile.model.tarefa.Locais', {
    extend: 'TskPlan.Mobile.model.Base',
    alias: 'model.tarefa-tela',

    fields: [{
        name: 'ObraCronogramaTarefaLocalId',
        type: 'int'
    }, {
        name: 'ObraCronogramaTarefaId',
        type: 'int'
    }, {
        name: 'ObraLocalId',
        type: 'int'
    }, {
        name: 'Local',
        type: 'string'
    }, {
        name: 'TarefaGuid',
        type: 'string'
    }, {
        name: 'Andamento',
        type: 'int'
    }, {
        name: 'DataInicio',
        type: 'date'
    }, {
        name: 'DataFim',
        type: 'date'
    }, {
        name: 'Checked',
        type: 'bool'
    }, {
        name: 'AndamentoTexto',
        persist: false,
        convert: function (value, rec) {
            var andamento = rec.get('Andamento');
            return andamento == 2 ? 'Concluida' : andamento == 1 ? 'Andamento' : 'Aberta';
        }
    }, {
        name: 'DataInicioFormatada',
        persist: false,
        convert: function (value, rec) {
            return Ext.util.Format.date(rec.get('DataInicio'), 'd/m/Y');
        }
    }, {
        name: 'DataFimFormatada',
        persist: false,
        convert: function (value, rec) {
            return Ext.util.Format.date(rec.get('DataFim'), 'd/m/Y');
        }
    }]
});