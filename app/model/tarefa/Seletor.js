Ext.define('TskPlan.Mobile.model.tarefa.Seletor', {
    extend: 'TskPlan.Mobile.model.Base',
    alias: 'model.tarefa-seletor',

    fields: [{
        name: 'ObraCronogramaTarefaId',
        type: 'int'
    }, {
        name: 'ObraCronogramaTarefaLocalId',
        type: 'int'
    }, {
        name: 'Tarefa',
        type: 'string'
    }, {
        name: 'ObraId',
        type: 'int'
    }, {
        name: 'ObraLocalId',
        type: 'int'
    }]
});