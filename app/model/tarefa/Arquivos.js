Ext.define('TskPlan.Mobile.model.tarefa.Arquivos', {
    extend: 'TskPlan.Mobile.model.Base',
    alias: 'model.tarefa-arquivos',

    fields: [{
        name: 'ObraCronogramaTarefaId',
        type: 'int'
    }, {
        name: 'TarefaGuid',
        type: 'string'
    }, {
        name: 'Nome',
        type: 'string'
    }, {
        name: 'Caminho',
        type: 'string'
    }, {
        name: 'Extensao',
        type: 'string'
    }, {
        name: 'DataHoraAlteracao',
        type: 'date'
    }, {
        name: 'Tipo',
        persist: false,
        convert: function (value, rec) {
            if (new RegExp(['jpg', 'png', 'jpeg'].join("|")).test(rec.get('Extensao')))
                return 'image'
            else
                return 'file'
        }
    }]
});