Ext.define('TskPlan.Mobile.model.tarefa.TarefaLocaisChecklistAuxiliar', {
    extend: 'TskPlan.Mobile.model.Base',
    alias: 'model.tarefa-locais-checklist-auxiliar',
    fields: [{
        name: 'ObraLocalId',
        type: 'int'
    }, {
        name: 'ObraId',
        type: 'int'
    }, {
        name: 'Caminho',
        type: 'string'
    }, {
        name: 'Nome',
        type: 'string'
    }, {
        name: 'PaiId',
        type: 'int'
    }, {
        name: 'TarefaId',
        type: 'int'
    }]
});