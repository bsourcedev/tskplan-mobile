Ext.define('TskPlan.Mobile.model.entrega.ProdutoLista', {
    extend: 'TskPlan.Mobile.model.Base',
    alias: 'model.entrega-produto-lista',
    // idProperty: 'ProdutoId',
    fields: [{
        name: 'ProdutoId',
        type: 'int'
    }, {
        name: 'Produto',
        type: 'string'
    }]
});