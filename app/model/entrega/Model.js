Ext.define('TskPlan.Mobile.model.entrega.Model', {
    extend: 'TskPlan.Mobile.model.Base',
    alias: 'model.entrega-tela',
    // idProperty: 'Id',
    fields: [{
        name: 'EntregaId',
        type: 'int'
    }, {
        name: 'PedidoCompraId',
        type: 'int'
    }, {
        name: 'ObraId',
        type: 'int'
    }, {
        name: 'Obra',
        type: 'string'
    }, {
        name: 'FornecedorId',
        type: 'int'
    }, {
        name: 'Fornecedor',
        type: 'string'
    }, {
        name: 'Data',
        type: 'date',
    }, {
        name: 'DataFormatada',
        persist: false,
        convert: function (value, rec) {
            return Ext.util.Format.date(rec.get('Data'), 'd/m/Y');
        }
    }, {
        name: 'Porcentagem',
        type: 'float'
    }, {
        name: 'Observacao',
        type: 'string'
    }, {
        name: 'NotaFiscal',
        type: 'string'
    }, {
        name: 'Serie',
        type: 'string'
    }, {
        name: 'Processado',
        type: 'bool'
    }, {
        name: 'IconEstoque',
        persist: false,
        calculate: function (rec) {
            return rec.Processado ? '<i class="fas fa-archive" style="color:var(--base-color)"></i>' : '<i class="fas fa-archive" style="color:silver"></i>';
        }
    }, {
        name: 'FornecedorLabel',
        persist: false,
        calculate: function (rec) {
            return rec.Fornecedor ? rec.Fornecedor : '<spam style="color: var(--disabled-color);">Sem fornecedor</spam>';
        }
    }]
});