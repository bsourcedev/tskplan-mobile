Ext.define('TskPlan.Mobile.model.entrega.Pedido', {
    extend: 'TskPlan.Mobile.model.Base',
    alias: 'model.entrega-pedido',
    // idProperty: 'PedidoCompraId',
    fields: [{
        name: 'PedidoCompraId',
        type: 'int'
    }, {
        name: 'Fornecedor',
        type: 'string'
    }, {
        name: 'Data',
        type: 'date',
        convert: function (value) {
            return Ext.util.Format.date(value, 'd/m/Y');
        }
    }, {
        name: 'Porcentagem',
        type: 'float'
    }, {
        name: 'Obra',
        type: 'string'
    }]
});