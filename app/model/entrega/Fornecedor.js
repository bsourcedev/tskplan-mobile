Ext.define('TskPlan.Mobile.model.entrega.Fornecedor', {
    extend: 'TskPlan.Mobile.model.Base',
    alias: 'model.entrega-fornecedor',
    fields: [{
        name: 'FornecedorId',
        type: 'string'
    }, {
        name: 'Nome',
        type: 'string'
    }]
});