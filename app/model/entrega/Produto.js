Ext.define('TskPlan.Mobile.model.entrega.Produto', {
    extend: 'TskPlan.Mobile.model.Base',
    alias: 'model.entrega-produto',
    // idProperty: 'ProdutoId',
    fields: [{
        name: 'EntregaProdutoId',
        type: 'int'
    }, {
        name: 'EntregaId',
        type: 'int'
    }, {
        name: 'ProdutoId',
        type: 'int'
    }, {
        name: 'Produto',
        type: 'string'
    }, {
        name: 'Quantidade',
        type: 'float'
    }, {
        name: 'Pedido',
        type: 'bool'
    }, {
        name: 'Observacao',
        type: 'string'
    }]
});