Ext.define('TskPlan.Mobile.model.Personnel', {
    extend: 'TskPlan.Mobile.model.Base',

    fields: [
        'name', 'email', 'phone'
    ]
});
