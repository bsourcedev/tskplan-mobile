Ext.define('TskPlan.Mobile.model.notificacoes.Model', {
    extend: 'TskPlan.Mobile.model.Base',
    alias: 'model.notificacoes-tela',
    idProperty: 'NotificacaoId',
    fields: [{
        name: 'NotificacaoId',
        type: 'int'
    }, {
        name: 'Titulo',
        type: 'string'
    }, {
        name: 'Descricao',
        type: 'string'
    }, {
        name: 'Visualizado',
        type: 'bool'
    }, {
        name: 'Stamp',
        type: 'string'
    }, {
        name: 'Hide',
        calculate: function (rec) {
            return rec.Visualizado ? 'Hidden' : '';
        }
    }]
});