Ext.define('TskPlan.Mobile.model.indexedDB.DirtStores', {
    extend: 'TskPlan.Mobile.model.Base',
    alias: 'model.idb-dirt-stores',
    fields: [{
        name: 'Store',
        type: 'string'
    }, {
        name: 'Database',
        type: 'string'
    }, {
        name: 'Object',
        type: 'string'
    }]
});