Ext.define('TskPlan.Mobile.model.indexedDB.DirtRecords', {
    extend: 'TskPlan.Mobile.model.Base',
    alias: 'model.idb-dirt-records',
    fields: [{
        name: 'Store',
        type: 'string',
        index: true
    },  {
        name: 'Params'
    }, {
        name: 'RecordGuid',
        type: 'string',
        index: true
    }, {
        name: 'Operation',
        type: 'string'
    }, {
        name: 'Tries',
        type: 'int'
    }]
});