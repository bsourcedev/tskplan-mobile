Ext.define('TskPlan.Mobile.model.indexedDB.Imagens', {
    extend: 'TskPlan.Mobile.model.Base',
    alias: 'model.idb-imagens',
    fields: [{
        name: 'ImageGuid',
        type: 'string'
    }, {
        name: 'LocalURL',
        type: 'string'
    }, {
        name: 'ServerURL',
        type: 'string'
    }]
});