Ext.define('TskPlan.Mobile.model.global.UnidadeMedida', {
    extend: 'TskPlan.Mobile.model.Base',
    alias: 'model.unidademedida-global',
    fields: [{
        name: 'UnidadeMedidaId',
        type: 'int'
    }, {
        name: 'Nome',
        type: 'string'
    }]
});