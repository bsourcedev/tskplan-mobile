Ext.define('TskPlan.Mobile.model.global.Obra', {
    extend: 'TskPlan.Mobile.model.Base',
    alias: 'model.obra-global',
    fields: [{
        name: 'ObraId',
        type: 'int'
    }, {
        name: 'Nome',
        type: 'string'
    }, {
        name: 'Endereco',
        type: 'string'
    }]
});