Ext.define('TskPlan.Mobile.model.global.ModeloServico', {
    extend: 'TskPlan.Mobile.model.Base',
    alias: 'model.modeloservico-global',
    fields: [{
        name: 'ModeloServicoId',
        type: 'int'
    }, {
        name: 'Nome',
        type: 'string'
    },{
        name: 'GrupoCompraId',
        type: 'int'
    }]
});