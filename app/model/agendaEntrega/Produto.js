Ext.define('TskPlan.Mobile.model.agendaEntrega.Produto', {
    extend: 'TskPlan.Mobile.model.Base',
    fields: [{
        name: 'PedidoCompraEntregaItemId',
        type: 'int'
    }, {
        name: 'PedidoCompraEntregaId',
        type: 'int'
    }, {
        name: 'ProdutoId',
        type: 'int'
    }, {
        name: 'Nome',
        type: 'string',
    }, {
        name: 'AgendaGuid',
        type: 'string',
    }, {
        name: 'Quantidade',
        type: 'float',
    }]
});