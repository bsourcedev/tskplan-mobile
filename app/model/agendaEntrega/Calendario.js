Ext.define('TskPlan.Mobile.model.agendaEntrega.Calendario', {
    extend: 'TskPlan.Mobile.model.Base',
    fields: [{
        name: 'PedidoCompraEntregaId',
        type: 'int'
    }, {
        name: 'PedidoCompraId',
        type: 'int'
    }, {
        name: 'DataEntrega',
        type: 'date'
    }, {
        name: 'Observacao',
        type: 'string',
    }, {
        name: 'Fornecedor',
        type: 'string',
    }, {
        name: 'StatusId',
        type: 'int',
    }, {
        name: 'Status',
        type: 'string',
    }, {
        name: 'ObraId',
        type: 'int',
    }, {
        name: 'Obra',
        type: 'string',
    }, {
        name: 'Dia',
        type: 'int',
    }, {
        name: 'Mes',
        type: 'int',
    }, {
        name: 'Ano',
        type: 'int',
    }, {
        name: 'DataHoraAlteracao',
        type: 'date'
    }, {
        name: 'StatusPeso',
        persist: false,
        convert: function (value, rec) {
            var status = rec.get('StatusId'),
                peso = 0;

            // Status
            // 1	Disponível
            // 2	Aprovado
            // 3	Reprovado
            // 4	Finalizado
            // 5	Entrega Parcial

            switch (status) {
                case 3: //Reprovado
                case 4: //Finalizado
                    peso = 1
                    break;
                case 5: // Entrega Parcial
                    peso = 2
                    break;
                case 1: //Disponivel
                case 2: //Aprovado
                    peso = 3
                    break;
            }

            return peso;
        }
    }, {
        name: 'DataFormatada',
        persist: false,
        convert: function (value, rec) {
            return Ext.util.Format.date(rec.get('DataEntrega'), 'd/m/Y');
        }
    }]
});