Ext.define('TskPlan.Mobile.model.Base', {
    extend: 'Ext.data.Model',
    requires: [
        'DBProxies.data.proxy.CustomDB',
        'Ext.data.identifier.Uuid'
    ],

    schema: {
        namespace: 'TskPlan.Mobile.model'
    },

    identifier: 'uuid',
    idProperty: 'guid',

    apiFields: [{
        name: 'isSync',
        type: 'int',
        index: true
    }, {
        name: 'isDeleted',
        type: 'int',
        index: true
    }, {
        name: 'isCreated',
        type: 'int',
        index: true
    }, {
        name: 'syncDate',
        type: 'date'
    }],


    proxy: {
        type: 'indexeddb',
        // dbName: 'TskPlan', //<-- Para utilizar dbName sera necessario estudar a atualização das tables dentro do mesmo tb no CustomDB.js
        allConfig: {
            cloud: false
        },
        proxies: [{
            type: 'indexeddb'
        }]
    },

    //Override para adicionar os campos da api aos campos do model
    constructor: function () {
        var me = this,
            model = Ext.ClassManager.get(me.$className);

        // me.apiFields.forEach(field => me.fields.push(Ext.create('Ext.data.field.Field', field)));

        model.addFields(me.apiFields);

        me.callParent(arguments);
    },

    //Retorna os fields que são indexes
    getIndexes: function () {
        var me = this,
            fields = me.getFields().filter(field => field.index).map(field => field.name);

        return fields;
    },

    // set: function () {
    //     var me = this;

    //     //<debug>
    //     debugger;
    //     //</debug>

    //     me.callParent(arguments);
    // }
});