Ext.define('TskPlan.Mobile.model.local.Seletor', {
    extend: 'TskPlan.Mobile.model.Base',
    alias: 'model.local-seletor',
    fields: [{
        name: 'ObraLocalId',
        type: 'int'
    }, {
        name: 'ObraId',
        type: 'int'
    }, {
        name: 'Caminho',
        type: 'string'
    }, {
        name: 'Nome',
        type: 'string'
    }, {
        name: 'PaiId',
        type: 'int'
    }]
});