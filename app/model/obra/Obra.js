Ext.define('TskPlan.Mobile.model.obra.Obra', {
    extend: 'TskPlan.Mobile.model.Base',
    alias: 'model.obra-tela',
    fields: [{
        name: 'ObraId',
        type: 'int'
    }, {
        name: 'Nome',
        type: 'string'
    }, {
        name: 'Sigla',
        type: 'string'
    }, {
        name: 'Aprovados',
        type: 'float'
    }, {
        name: 'Rejeitados',
        type: 'float'
    }, {
        name: 'Pendentes',
        type: 'float'
    }]
});