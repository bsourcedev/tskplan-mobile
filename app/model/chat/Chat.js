Ext.define('TskPlan.Mobile.model.chat.Chat', {
    extend: 'TskPlan.Mobile.model.Base',
    fields: [{
        name: 'ChatId',
        type: 'int'
    }]
});