Ext.define('TskPlan.Mobile.model.chat.ChatList', {
    extend: 'TskPlan.Mobile.model.Base',
    fields: [{
        name: 'ChatId',
        type: 'int'
    }, {
        name: 'Titulo',
        type: 'string'
    }, {
        name: 'Grupo',
        type: 'bool'
    }, {
        name: 'Obra',
        type: 'string'
    }, {
        name: 'UltimaMensagem',
        type: 'string'
    }, {
        name: 'UltimaMensagemUsuario',
        type: 'string'
    }, {
        name: 'UltimaMensagemData',
        type: 'date'
    }, {
        name: 'Tarefa',
        type: 'string'
    },{
        name: 'TarefaId',
        type: 'int'
    },{
        name: 'TimeLabel',
        persist: false,
        convert: function (value, rec) {
            if (!rec.get('UltimaMensagemData'))
                return '';

            var retorno,
                hoje = new Date(),
                hojeFormatado = Ext.Date.format(hoje, 'd/m/Y'),
                date = rec.get('UltimaMensagemData'),
                dataFormatada = Ext.Date.format(date, 'd/m/Y'),
                ontem = Ext.Date.add(hoje, Ext.Date.DAY, -1),
                ontemFormatado = Ext.Date.format(ontem, 'd/m/Y'),
                semana = Ext.Date.add(hoje, Ext.Date.DAY, -7),
                semanaFormatado = Ext.Date.format(semana, 'd/m/Y'),
                time = new Date(Ext.Date.format(date, 'm/d/Y')).getTime(),
                from = new Date(Ext.Date.format(semana, 'm/d/Y')).getTime();

            //Hoje
            if (hojeFormatado == dataFormatada)
                retorno = Ext.util.Format.date(date, 'h:m');
            //Ontem
            else if (dataFormatada == ontemFormatado)
                retorno = 'Ontem';
            //Ultimos sete dias
            else if (time >= from)
                retorno = Ext.Date.format(date, 'l');
            //Outras datas
            else
                retorno = dataFormatada;

            return retorno;
        }
    }]
});