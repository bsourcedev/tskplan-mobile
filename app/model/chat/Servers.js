Ext.define('TskPlan.Mobile.model.chat.Servers', {
    extend: 'TskPlan.Mobile.model.Base',
    
    fields: [{
        name: 'ObraId',
        type: 'int'
    }, {
        name: 'Id',
        type: 'string'
    }, {
        name: 'ServerName',
        type: 'string'
    }, {
        name: 'ShortName',
        type: 'string'
    }, {
        name: 'IconUrl',
        type: 'string'
    }, {
        name: 'UnreadMessages',
        type: 'int'
    }]
});