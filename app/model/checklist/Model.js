Ext.define('TskPlan.Mobile.model.checklist.Model', {
    extend: 'TskPlan.Mobile.model.Base',
    fields: [{
        name: 'Id',
        type: 'string'
    }, {
        name: 'ObraCronogramaTarefaLocalId',
        type: 'int'
    }, {
        name: 'ObraCronogramaTarefaId',
        type: 'int'
    }, {
        name: 'ObraCronogramaTarefaCheckListId',
        type: 'int',
    }, {
        name: 'OrdemServicoCheckListId',
        type: 'int',
    }, {
        name: 'TarefaGuid',
        type: 'string',
    }, {
        name: 'ObraId',
        type: 'int',
    }, {
        name: 'ObraLocalId',
        type: 'int',
    }, {
        name: 'TarefaId',
        type: 'int',
    }, {
        name: 'Titulo',
        type: 'string'
    }, {
        name: 'Descricao',
        type: 'string'
    }, {
        name: 'StatusId',
        type: 'int'
    }, {
        name: 'Status',
        calculate: function (rec) {
            return rec.StatusId == 0 ? 'Rejeitado' : rec.StatusId == -1 ? 'Pendente' : 'Aprovado';
        }
    }]
});