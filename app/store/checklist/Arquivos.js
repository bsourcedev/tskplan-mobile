Ext.define('TskPlan.Mobile.store.checklist.Arquivos', {
    extend: 'Ext.data.Store',
    alias: 'store.checklist-arquivos',
    autoLoad: false,
    useIDB: true,
    model: 'TskPlan.Mobile.model.checklist.Arquivos',

    proxy: {
        type: 'rest',
        pageParam: null,
        limitParam: null,
        startParam: null,
        cacheParam: null,
        noCache: false,
        authenticate: true,
        url: API_CONFIG.ChecklistArquivosUrl,
        reader: {
            type: 'json',
            rootProperty: 'Data',
            successProperty: 'success',
            readRecordsOnFailure: false
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            dateFormat: 'Y-m-d H:i:s'
        },
        api: {
            read: API_CONFIG.ChecklistArquivosUrl,
            create: API_CONFIG.ChecklistArquivosUrl,
            update: API_CONFIG.ChecklistArquivosUrl,
            destroy: API_CONFIG.ChecklistArquivosUrl
        },
        headers: {
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjUzIiwibmFtZWlkIjoiQlNvdXJjZVgiLCJuYmYiOjE1OTA1MDQ4OTMsImV4cCI6MjUzNzIzMjg5MywiaWF0IjoxNTkwNTA0ODkzfQ.axGrOWhQ7Ed79yTMPzrPrkar6IcPfZLfk7X5iKc5PO4' //+ window.bearerToken
        }
    }
});