Ext.define('TskPlan.Mobile.store.local.Seletor', {
    extend: 'Ext.data.Store',
    alias: 'store.local-store',
    useIDB: true,
    autoLoad: false,
    model: 'TskPlan.Mobile.model.local.Seletor',
    proxy: {
        type: 'rest',
        pageParam: null,
        limitParam: null,
        startParam: null,
        cacheParam: true,
        noCache: false,
        appendId: false,
        authenticate: true,
        url: API_CONFIG.obraLocalUrl,
        reader: {
            type: 'json',
            rootProperty: 'Data',
            successProperty: 'success',
            readRecordsOnFailure: false
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            dateFormat: 'Y-m-d H:i:s'
        },
        api: {
            read: API_CONFIG.obraLocalUrl,
            create: API_CONFIG.obraLocalUrl,
            update: API_CONFIG.obraLocalUrl,
            destroy: API_CONFIG.obraLocalUrl,
        }
    }
});