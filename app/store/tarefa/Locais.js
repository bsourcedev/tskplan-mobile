Ext.define('TskPlan.Mobile.store.tarefa.Locais', {
    extend: 'Ext.data.Store',
    alias: 'store.tarefa-locais',

    useIDB: true,
    model: 'TskPlan.Mobile.model.tarefa.Locais',

    proxy: {
        type: 'rest',
        pageParam: null,
        limitParam: null,
        startParam: null,
        cacheParam: null,
        noCache: false,
        authenticate: true,
        url: API_CONFIG.TarefaOS,
        reader: {
            type: 'json',
            rootProperty: 'Data',
            successProperty: 'success',
            readRecordsOnFailure: false
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            dateFormat: 'Y-m-d H:i:s'
        },
        api: {
            read: API_CONFIG.TarefaOS,
            create: API_CONFIG.TarefaOS,
            update: API_CONFIG.TarefaOS,
            destroy: API_CONFIG.TarefaOS
        },
        headers: {
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjUzIiwibmFtZWlkIjoiQlNvdXJjZVgiLCJuYmYiOjE1OTA1MDQ4OTMsImV4cCI6MjUzNzIzMjg5MywiaWF0IjoxNTkwNTA0ODkzfQ.axGrOWhQ7Ed79yTMPzrPrkar6IcPfZLfk7X5iKc5PO4' //+ window.bearerToken
        }
    }
});