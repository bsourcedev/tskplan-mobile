Ext.define('TskPlan.Mobile.store.tarefa.TarefaLocaisChecklist', {
    extend: 'Ext.data.Store',
    alias: 'store.tarefa-locais-checklist',
    useIDB: true,
    autoLoad: false,
    model: 'TskPlan.Mobile.model.tarefa.TarefaLocaisChecklist',
    proxy: {
        type: 'rest',
        pageParam: null,
        limitParam: null,
        startParam: null,
        cacheParam: null,
        noCache: false,
        authenticate: true,
        url: API_CONFIG.TarefaOSLocaistUrl,
        reader: {
            type: 'json',
            rootProperty: 'Data',
            successProperty: 'success',
            readRecordsOnFailure: false
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            dateFormat: 'Y-m-d H:i:s'
        },
        api: {
            read: API_CONFIG.TarefaOSLocaistUrl,
            create: API_CONFIG.TarefaOSLocaistUrl,
            update: API_CONFIG.TarefaOSLocaistUrl,
            destroy: API_CONFIG.TarefaOSLocaistUrl
        },
        headers: {
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjUzIiwibmFtZWlkIjoiQlNvdXJjZVgiLCJuYmYiOjE1OTA1MDQ4OTMsImV4cCI6MjUzNzIzMjg5MywiaWF0IjoxNTkwNTA0ODkzfQ.axGrOWhQ7Ed79yTMPzrPrkar6IcPfZLfk7X5iKc5PO4' //+ window.bearerToken
        }
    }
});