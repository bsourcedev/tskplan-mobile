Ext.define('TskPlan.Mobile.store.tarefa.TarefaLocaisChecklistAuxiliar', {
    extend: 'Ext.data.Store',
    alias: 'store.tarefa-locais-checklist-auxiliar',
    useIDB: true,
    autoLoad: false,
    model: 'TskPlan.Mobile.model.tarefa.TarefaLocaisChecklistAuxiliar',
    proxy: {
        type: 'rest',
        pageParam: null,
        limitParam: null,
        startParam: null,
        cacheParam: null,
        noCache: false,
        authenticate: true,
        url: API_CONFIG.TarefaOSLocaisAuxiliarUrl,
        reader: {
            type: 'json',
            rootProperty: 'Data',
            successProperty: 'success',
            readRecordsOnFailure: false
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            dateFormat: 'Y-m-d H:i:s'
        },
        api: {
            read: API_CONFIG.TarefaOSLocaisAuxiliarUrl,
            create: API_CONFIG.TarefaOSLocaisAuxiliarUrl,
            update: API_CONFIG.TarefaOSLocaisAuxiliarUrl,
            destroy: API_CONFIG.TarefaOSLocaisAuxiliarUrl
        },
        headers: {
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjUzIiwibmFtZWlkIjoiQlNvdXJjZVgiLCJuYmYiOjE1OTA1MDQ4OTMsImV4cCI6MjUzNzIzMjg5MywiaWF0IjoxNTkwNTA0ODkzfQ.axGrOWhQ7Ed79yTMPzrPrkar6IcPfZLfk7X5iKc5PO4' //+ window.bearerToken
        }
    }
});