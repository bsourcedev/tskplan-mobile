Ext.define('TskPlan.Mobile.store.indexedDB.DirtStores', {
    extend: 'Ext.data.Store',
    alias: 'store.idb-dirt-stores',
    autoLoad: true,
    autoSync: true,
    useIDB: true,
    iDBPhantomStore: true,
    model: 'TskPlan.Mobile.model.indexedDB.DirtStores',
    proxy: {
        type: 'rest',
        pageParam: null,
        limitParam: null,
        startParam: null,
        cacheParam: true,
        noCache: false,
        authenticate: true,
        url: '',
        reader: {
            type: 'json',
            rootProperty: 'Data',
            successProperty: 'success',
            readRecordsOnFailure: false
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            dateFormat: 'Y-m-d H:i:s'
        },
        api: {
            read: '',
            create: '',
            update: '',
            destroy: '',
        }
    },

    // add: function () {
    //     var me = this;

    //     //<debug>
    //     console.warn('Validar se a store ja foi previamente adicionada como dirt')
    //     //</debug>

    //     // me.callParent(arguments);
    // }
});