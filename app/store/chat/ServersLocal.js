Ext.define('TskPlan.Mobile.store.chat.ServersLocal', {
    extend: 'Ext.data.Store',
    alias: 'store.chat-servers-local',
    autoLoad: false,

    model: 'TskPlan.Mobile.model.chat.Servers',
});