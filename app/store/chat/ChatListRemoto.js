Ext.define('TskPlan.Mobile.store.chat.ChatListRemoto', {
    extend: 'Ext.data.Store',
    alias: 'store.chat-list-remoto',
    autoLoad: false,
    useIDB: false,

    model: 'TskPlan.Mobile.model.chat.ChatList',

    proxy: {
        type: 'rest',
        pageParam: null,
        limitParam: null,
        startParam: null,
        cacheParam: null,
        noCache: false,
        authenticate: true,
        url: API_CONFIG.Chat.NewChannels,
        reader: {
            type: 'json',
            rootProperty: 'Data',
            successProperty: 'success',
            readRecordsOnFailure: false
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            dateFormat: 'Y-m-d H:i:s'
        },
        api: {
            read: window.API_CONFIG.Chat.NewChannels,
            create: window.API_CONFIG.Chat.NewChannels,
            update: window.API_CONFIG.Chat.NewChannels,
            destroy: window.API_CONFIG.Chat.NewChannels,
        },
        headers: {
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjUzIiwibmFtZWlkIjoiQlNvdXJjZVgiLCJuYmYiOjE1OTA1MDQ4OTMsImV4cCI6MjUzNzIzMjg5MywiaWF0IjoxNTkwNTA0ODkzfQ.axGrOWhQ7Ed79yTMPzrPrkar6IcPfZLfk7X5iKc5PO4' //+ window.bearerToken
        }
    }
});