Ext.define('TskPlan.Mobile.store.chat.Chat', {
    extend: 'Ext.data.Store',
    alias: 'store.chat-tela',

    model: 'TskPlan.Mobile.model.chat.Chat',

    // proxy: {
    //     type: 'rest',
    //     appendId: false,
    //     pageParam: null,
    //     limitParam: null,
    //     startParam: null,
    //     cacheParam: null,
    //     noCache: false,
    //     authenticate: true,
    //     url: API_CONFIG.ChecklistListaUrl,
    //     reader: {
    //         type: 'json',
    //         rootProperty: 'Data',
    //         successProperty: 'success',
    //         readRecordsOnFailure: false
    //     },
    //     writer: {
    //         type: 'json',
    //         writeAllFields: true,
    //         dateFormat: 'Y-m-d H:i:s'
    //     },
    //     api: {
    //         read: API_CONFIG.ChecklistListaUrl,
    //         create: API_CONFIG.ChecklistListaUrl,
    //         update: API_CONFIG.ChecklistListaUrl,
    //         destroy: API_CONFIG.ChecklistListaUrl,
    //     },
    //     headers: {
    //         'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjUzIiwibmFtZWlkIjoiQlNvdXJjZVgiLCJuYmYiOjE1OTA1MDQ4OTMsImV4cCI6MjUzNzIzMjg5MywiaWF0IjoxNTkwNTA0ODkzfQ.axGrOWhQ7Ed79yTMPzrPrkar6IcPfZLfk7X5iKc5PO4' //+ window.bearerToken
    //     }
    // }
});