Ext.define('TskPlan.Mobile.store.chat.ChatList', {
    extend: 'Ext.data.Store',
    alias: 'store.chat-list-tela',

    model: 'TskPlan.Mobile.model.chat.ChatList',
});