Ext.define('TskPlan.Mobile.store.obra.Obra', {
    extend: 'Ext.data.Store',
    alias: 'store.obra-tela',
    autoLoad: false,
    useIDB: true,
    model: 'TskPlan.Mobile.model.obra.Obra',
    proxy: {
        type: 'rest',
        pageParam: null,
        limitParam: null,
        startParam: null,
        cacheParam: true,
        noCache: false,
        authenticate: true,
        url: API_CONFIG.obraUrl,
        reader: {
            type: 'json',
            rootProperty: 'Data',
            successProperty: 'success',
            readRecordsOnFailure: false
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            dateFormat: 'Y-m-d H:i:s'
        },
        api: {
            read: API_CONFIG.obraUrl,
            create: API_CONFIG.obraUrl,
            update: API_CONFIG.obraUrl,
            destroy: API_CONFIG.obraUrl,
        }
    }
});