Ext.define('TskPlan.Mobile.store.global.UnidadeMedida', {
    extend: 'Ext.data.Store',
    alias: 'store.unidademedida-global',
    autoLoad: true,
    useIDB: true,
    model: 'TskPlan.Mobile.model.global.UnidadeMedida',
    proxy: {
        type: 'rest',
        pageParam: null,
        limitParam: null,
        startParam: null,
        cacheParam: true,
        noCache: false,
        authenticate: true,
        url: API_CONFIG.Global.UnidadeMedida,
        reader: {
            type: 'json',
            rootProperty: 'Data',
            successProperty: 'success',
            readRecordsOnFailure: false
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            dateFormat: 'Y-m-d H:i:s'
        },
        api: {
            read: API_CONFIG.Global.UnidadeMedida,
            create: API_CONFIG.Global.UnidadeMedida,
            update: API_CONFIG.Global.UnidadeMedida,
            destroy: API_CONFIG.Global.UnidadeMedida,
        }
    }
});