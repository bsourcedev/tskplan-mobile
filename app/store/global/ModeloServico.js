Ext.define('TskPlan.Mobile.store.global.ModeloServico', {
    extend: 'Ext.data.Store',
    alias: 'store.modeloservico-global',
    autoLoad: true,
    useIDB: true,
    model: 'TskPlan.Mobile.model.global.ModeloServico',
    proxy: {
        type: 'rest',
        pageParam: null,
        limitParam: null,
        startParam: null,
        cacheParam: true,
        noCache: false,
        authenticate: true,
        url: API_CONFIG.Global.ModeloServico,
        reader: {
            type: 'json',
            rootProperty: 'Data',
            successProperty: 'success',
            readRecordsOnFailure: false
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            dateFormat: 'Y-m-d H:i:s'
        },
        api: {
            read: API_CONFIG.Global.ModeloServico,
            create: API_CONFIG.Global.ModeloServico,
            update: API_CONFIG.Global.ModeloServico,
            destroy: API_CONFIG.Global.ModeloServico,
        }
    }
});