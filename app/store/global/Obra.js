Ext.define('TskPlan.Mobile.store.global.Obra', {
    extend: 'Ext.data.Store',
    alias: 'store.obra-global',
    autoLoad: true,
    useIDB: true,
    model: 'TskPlan.Mobile.model.global.Obra',
    proxy: {
        type: 'rest',
        pageParam: null,
        limitParam: null,
        startParam: null,
        cacheParam: true,
        noCache: false,
        authenticate: true,
        url: API_CONFIG.Global.Obra,
        reader: {
            type: 'json',
            rootProperty: 'Data',
            successProperty: 'success',
            readRecordsOnFailure: false
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            dateFormat: 'Y-m-d H:i:s'
        },
        api: {
            read: API_CONFIG.Global.Obra,
            create: API_CONFIG.Global.Obra,
            update: API_CONFIG.Global.Obra,
            destroy: API_CONFIG.Global.Obra,
        }
    }
});