Ext.define('TskPlan.Mobile.store.notificacoes.Store', {
    extend: 'Ext.data.Store',
    alias: 'store.notificacoes-tela',

    model: 'TskPlan.Mobile.model.notificacoes.Model',

    data: {
        items: [{
            NotificacaoId: 01,
            Titulo: 'Nada de novo',
            Descricao: 'Nenhuma nova notificação!',
            Visualizado: false,
            Stamp:'Agora'
        }]
    },
    //<debug>
    data: {
        items: [{
            NotificacaoId: 01,
            Titulo: 'Nova Notificação',
            Descricao: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercita',
            Visualizado: false,
            Stamp:'5 minutos'
        }, {
            NotificacaoId: 02,
            Titulo: 'Notificação',
            Descricao: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ',
            Visualizado: false,
            Stamp:'1 hora'
        }, {
            NotificacaoId: 03,
            Titulo: 'Titulo',
            Descricao: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore ',
            Visualizado: true,
            Stamp:'2 dias'
        }, {
            NotificacaoId: 04,
            Titulo: 'Novo Checklist',
            Descricao: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitaeu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
            Visualizado: true,
            Stamp:'5 dias'
        }]
    },
    //</debug>  
    
    proxy: {
        type: 'memory',
        reader: {
            type: 'json',
            rootProperty: 'items'
        }
    }
});