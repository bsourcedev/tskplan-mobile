Ext.define('TskPlan.Mobile.store.agendaEntrega.AgendaEntrega', {
    extend: 'Ext.data.Store',
    alias: 'store.agenda-entrega-agenda-entrega',
    model: 'TskPlan.Mobile.model.agendaEntrega.Calendario',
});