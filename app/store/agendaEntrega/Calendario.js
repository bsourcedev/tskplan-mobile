Ext.define('TskPlan.Mobile.store.agendaEntrega.Calendario', {
    extend: 'Ext.data.Store',
    alias: 'store.agenda-entrega-calendario',

    useIDB: true,
    model: 'TskPlan.Mobile.model.agendaEntrega.Calendario',

    proxy: {
        type: 'rest',
        appendId: false,
        pageParam: null,
        limitParam: null,
        startParam: null,
        cacheParam: null,
        noCache: false,
        authenticate: true,
        url: API_CONFIG.AgendaEntrega.Calendario,
        reader: {
            type: 'json',
            rootProperty: 'Data',
            successProperty: 'success',
            readRecordsOnFailure: false
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            dateFormat: 'Y-m-d H:i:s'
        },
        api: {
            read: API_CONFIG.AgendaEntrega.Calendario,
            create: API_CONFIG.AgendaEntrega.Calendario,
            update: API_CONFIG.AgendaEntrega.Calendario,
            destroy: API_CONFIG.AgendaEntrega.Calendario,
        },
        headers: {
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjUzIiwibmFtZWlkIjoiQlNvdXJjZVgiLCJuYmYiOjE1OTA1MDQ4OTMsImV4cCI6MjUzNzIzMjg5MywiaWF0IjoxNTkwNTA0ODkzfQ.axGrOWhQ7Ed79yTMPzrPrkar6IcPfZLfk7X5iKc5PO4' //+ window.bearerToken
        }
    }
});