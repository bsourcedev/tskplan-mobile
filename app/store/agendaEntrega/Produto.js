Ext.define('TskPlan.Mobile.store.agendaEntrega.Produto', {
    extend: 'Ext.data.Store',
    alias: 'store.agenda-entrega-produto',

    useIDB: true,
    model: 'TskPlan.Mobile.model.agendaEntrega.Produto',

    proxy: {
        type: 'rest',
        appendId: false,
        pageParam: null,
        limitParam: null,
        startParam: null,
        cacheParam: null,
        noCache: false,
        authenticate: true,
        url: API_CONFIG.AgendaEntrega.Produto,
        reader: {
            type: 'json',
            rootProperty: 'Data',
            successProperty: 'success',
            readRecordsOnFailure: false
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            dateFormat: 'Y-m-d H:i:s'
        },
        api: {
            read: API_CONFIG.AgendaEntrega.Produto,
            create: API_CONFIG.AgendaEntrega.Produto,
            update: API_CONFIG.AgendaEntrega.Produto,
            destroy: API_CONFIG.AgendaEntrega.Produto,
        },
        headers: {
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjUzIiwibmFtZWlkIjoiQlNvdXJjZVgiLCJuYmYiOjE1OTA1MDQ4OTMsImV4cCI6MjUzNzIzMjg5MywiaWF0IjoxNTkwNTA0ODkzfQ.axGrOWhQ7Ed79yTMPzrPrkar6IcPfZLfk7X5iKc5PO4' //+ window.bearerToken
        }
    }
});