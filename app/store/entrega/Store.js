Ext.define('TskPlan.Mobile.store.entrega.Store', {
    extend: 'Ext.data.Store',
    alias: 'store.entrega-tela',
    autoLoad: true,
    model: 'TskPlan.Mobile.model.entrega.Model',

    proxy: {
        type: 'rest',
        pageParam: null,
        limitParam: null,
        startParam: null,
        cacheParam: true,
        noCache: false,
        authenticate: true,
        appendId:false,
        url: API_CONFIG.Entrega,
        reader: {
            type: 'json',
            rootProperty: 'Data',
            successProperty: 'success',
            readRecordsOnFailure: false
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            dateFormat: 'Y-m-d H:i:s'
        },
        api: {
            read: API_CONFIG.Entrega,
            create: API_CONFIG.Entrega,
            update: API_CONFIG.Entrega,
            destroy: API_CONFIG.Entrega,
        }
    }
});