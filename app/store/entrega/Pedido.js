Ext.define('TskPlan.Mobile.store.entrega.Pedido', {
    extend: 'Ext.data.Store',
    alias: 'store.entrega-pedido',
    autoLoad: true,
    useIDB:true,
    model: 'TskPlan.Mobile.model.entrega.Pedido',
    proxy: {
        type: 'rest',
        pageParam: null,
        limitParam: null,
        startParam: null,
        cacheParam: true,
        noCache: false,
        authenticate: true,
        url: API_CONFIG.EntregaPedido,
        reader: {
            type: 'json',
            rootProperty: 'Data',
            successProperty: 'success',
            readRecordsOnFailure: false
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            dateFormat: 'Y-m-d H:i:s'
        },
        api: {
            read: API_CONFIG.EntregaPedido,
            create: API_CONFIG.EntregaPedido,
            update: API_CONFIG.EntregaPedido,
            destroy: API_CONFIG.EntregaPedido,
        }
    }
});