Ext.define('TskPlan.Mobile.store.entrega.Fornecedor', {
    extend: 'Ext.data.Store',
    alias: 'store.entrega-fornecedor',
    xtype:'teste',
    autoLoad: true,
    useIDB:true,
    model: 'TskPlan.Mobile.model.entrega.Fornecedor',
    proxy: {
        type: 'rest',
        pageParam: null,
        limitParam: null,
        startParam: null,
        cacheParam: true,
        noCache: false,
        authenticate: true,
        url: API_CONFIG.EntregaPedido,
        reader: {
            type: 'json',
            rootProperty: 'Data',
            successProperty: 'success',
            readRecordsOnFailure: false
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            dateFormat: 'Y-m-d H:i:s'
        },
        api: {
            read: API_CONFIG.EntregaFornecedor,
            create: API_CONFIG.EntregaFornecedor,
            update: API_CONFIG.EntregaFornecedor,
            destroy: API_CONFIG.EntregaFornecedor,
        }
    }
});