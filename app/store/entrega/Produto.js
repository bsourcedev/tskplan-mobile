Ext.define('TskPlan.Mobile.store.entrega.Produto', {
    extend: 'Ext.data.Store',
    alias: 'store.entrega-produto',
    model: 'TskPlan.Mobile.model.entrega.Produto',
    useIDB: true,
    proxy: {
        type: 'rest',
        pageParam: null,
        limitParam: null,
        startParam: null,
        cacheParam: true,
        noCache: false,
        authenticate: true,
        url: API_CONFIG.EntregaProduto,
        reader: {
            type: 'json',
            rootProperty: 'Data',
            successProperty: 'success',
            readRecordsOnFailure: false
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            dateFormat: 'Y-m-d H:i:s'
        },
        api: {
            read: API_CONFIG.EntregaProduto,
            create: API_CONFIG.EntregaProduto,
            update: API_CONFIG.EntregaProduto,
            destroy: API_CONFIG.EntregaProduto,
        }
    }
});