Ext.define('TskPlan.Mobile.store.entrega.ProdutoLista', {
    extend: 'Ext.data.Store',
    alias: 'store.entrega-produto-lista',
    model: 'TskPlan.Mobile.model.entrega.ProdutoLista',
    proxy: {
        type: 'rest',
        pageParam: null,
        limitParam: null,
        startParam: null,
        cacheParam: true,
        noCache: false,
        authenticate: true,
        url: API_CONFIG.EntregaProdutoLista,
        reader: {
            type: 'json',
            rootProperty: 'Data',
            successProperty: 'success',
            readRecordsOnFailure: false
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            dateFormat: 'Y-m-d H:i:s'
        },
        api: {
            read: API_CONFIG.EntregaProdutoLista,
            create: API_CONFIG.EntregaProdutoLista,
            update: API_CONFIG.EntregaProdutoLista,
            destroy: API_CONFIG.EntregaProdutoLista,
        }
    }
});