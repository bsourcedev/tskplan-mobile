Ext.define('TskPlan.Mobile.store.Seletor', {
    extend: 'Ext.data.Store',
    alias: 'store.tarefa-seletor',

    model: 'TskPlan.Mobile.model.tarefa.Seletor',

    // data: {
    //     items: [{
    //         TarefaId: 01,
    //         Nome: "Pintura"
    //     }, {
    //         TarefaId: 02,
    //         Nome: "Concreto"
    //     }, {
    //         TarefaId: 03,
    //         Nome: "Laje"
    //     }, {
    //         TarefaId: 04,
    //         Nome: "Eletrica"
    //     }]
    // },

    proxy: {
        type: 'rest',
        pageParam: null,
        limitParam: null,
        startParam: null,
        cacheParam: null,
        noCache: false,
        authenticate: true,
        url: API_CONFIG.TarefaListaUrl,
        reader: {
            type: 'json',
            rootProperty: 'Data',
            successProperty: 'success',
            readRecordsOnFailure: false
        },
        writer: {
            type: 'json',
            writeAllFields: true,
            dateFormat: 'Y-m-d H:i:s'
        },
        api: {
            read: API_CONFIG.TarefaListaUrl,
            create: API_CONFIG.TarefaListaUrl,
            update: API_CONFIG.TarefaListaUrl,
            destroy: API_CONFIG.TarefaListaUrl
        },
        headers: {
            'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjUzIiwibmFtZWlkIjoiQlNvdXJjZVgiLCJuYmYiOjE1OTA1MDQ4OTMsImV4cCI6MjUzNzIzMjg5MywiaWF0IjoxNTkwNTA0ODkzfQ.axGrOWhQ7Ed79yTMPzrPrkar6IcPfZLfk7X5iKc5PO4' //+ window.bearerToken
        }
    }
});