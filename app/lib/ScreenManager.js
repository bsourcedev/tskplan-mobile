/**
 * TskPlan.Mobile.lib.ScreenManager
 * Lib de gerenciamento de telas
 * @returns {} 
 */
Ext.define('TskPlan.Mobile.lib.ScreenManager', {
    singleton: true,

    telasAbertas: [],

    telaAberta: null,

    //True para criar um hook do botão voltar do dispositivo para a fn voltar()
    hookBackButton: false,

    //Define se a tela ativa é uma tela principal para permitir que o botão voltar do dispositivo saia ou não do app
    mainScreenActive: true,

    constructor: function () {
        window.animacoes = {
            ir: {
                type: 'slide',
                direction: 'left'
            },
            voltar: {
                type: 'slide',
                direction: 'right'
            }
        };
    },

    animacao: function (animacao) {
        mainTabView.getLayout().setAnimation(window.animacoes.ir);
    },

    cancelarAnimacao: function () {
        mainTabView.getLayout().setAnimation();
    },

    abrirTela: function (screenData) {
        var me = this;

        screenData.id = screenData.screen.getId();

        me.telasAbertas.push(screenData);

        me.updateTelaAberta(screenData.id);
    },

    exibirTab: function (animacao) {
        var me = this,
            tela = me.telaAberta.screen,
            index = mainTabView.items.findIndex('id', me.telaAberta.id);

        if (index == -1)
            mainTabView.add(me.telaAberta.screen);

        mainController.ativarMenu();

        me.animacao(animacao);

        mainTabView.setActiveItem(mainTabView.items.items.length);
    },

    updateTelaAberta: function (id, animacao) {
        var me = this,
            tela = me.telasAbertas.find(x => x.id == id);

        if (!tela) {
            tela = {
                screen: mainTabMenu,
                anterior: null,
                title: 'Menu',
                id: mainTabMenu.getId(),
                voltar: false,
                fullScreen: false
            };
            me.mainScreenActive = true;
        } else
            me.mainScreenActive = false;

        me.setTitulo(tela.title);

        me.telaAberta = {
            screen: tela.screen,
            anterior: tela.anterior,
            title: tela.title,
            id: tela.id,
            voltar: tela.voltar === undefined ? true : tela.voltar,
            fullScreen: tela.fullScreen ? tela.fullScreen : false
        };

        if (me.telaAberta.fullScreen)
            window.navigationTabs.hide()
        else
            window.navigationTabs.show()

        navigationBar.getController().exibirVoltar(me.telaAberta.voltar ? (me.telaAberta.anterior ? true : false) : false);

        me.hookBackButton = me.telaAberta.voltar ? (me.telaAberta.anterior ? true : false) : false;

        console.log(`hookBackButton: ${me.hookBackButton}\n voltar: ${me.telaAberta.voltar}\n telaAberta.anterior: ${(me.telaAberta.anterior ? true : false)}`)

        me.exibirTab(animacao ? animacao : window.animacoes.ir);

        NavigatorManager.overrideBackButton();
    },

    fecharTela: function (id) {
        var me = this,
            screen = me.telasAbertas.find(x => x.id == id);

        Ext.Array.remove(me.telasAbertas, screen);

        mainTabView.remove(id);
    },

    voltar: function () {
        var me = this;

        if (me.telaAberta && me.telaAberta.anterior) {
            me.fecharTela(me.telaAberta.id);

            // if (!me.telaAberta.voltar)
            //     me.updateTelaAberta(me.telasAbertas.find(x => x.id == me.telaAberta.anterior.id).anterior.id);
            // else
            me.updateTelaAberta(me.telaAberta.anterior.getId(), window.animacoes.voltar);
        }
    },

    limparTelasAbertas: function () {
        var me = this;

        Ext.each(me.telasAbertas, function (tela) {
            var me = this;

            mainTabView.remove(tela.id);

            //<debug>
            console.log(`Tela removida: ${tela.id}`)
            //</debug>  
        });

        navigationBar.getController().exibirVoltar(false);
        me.telasAbertas = [];
        me.telaAberta = null;

        me.mainScreenActive = true;
        me.hookBackButton = false;

        NavigatorManager.overrideBackButton();
    },

    setTitulo: function (titulo) {
        var me = this,
            tela = me.telasAbertas.find(x => x.id == id),
            viewModel = navigationBar.getViewModel();

        //<debug>
        console.log(`Nome Alterado de [${viewModel.get('tela')}] para [${titulo}]`)
        //</debug>

        viewModel.set('tela', titulo);
    }
});