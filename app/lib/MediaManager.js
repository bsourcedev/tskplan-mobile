Ext.define('TskPlan.Mobile.lib.MediaManager', {
    singleton: true,
    cookieName: 'TSKPLAN_MEDIA',
    cookieInUse: true,
    mediaAwaiting: [],
    mediaAwaitingError: [],
    syncTimeout: null,
    storeImagem: Ext.create('TskPlan.Mobile.store.indexedDB.Imagens'),

    constructor: function () {
        var me = this,
            cookieMediaData = JSON.parse(me.getCookie());

        me.cookieInUse = true;

        me.mediaAwaiting = cookieMediaData ? cookieMediaData : [];

        me.cookieInUse = false;

        //<debug>
        if (cookieMediaData) {
            console.groupCollapsed(`Media não sincronizada durante o ultimo uso [${cookieMediaData.length}]`);
            Ext.each(cookieMediaData, function (media) {
                console.log(media);
            });
            console.groupEnd();
        }
        //</debug>  
    },

    //Adiciona media a fila
    addMedia: function (media) {
        var me = this,
            mediaData = media.guid ? media : {
                guid: TskPlanManager.newGuid(),
                geolocation: null,
                media: media
            };

        if (!mediaData.geolocation)
            new Ext.Promise(function (resolve, reject) {
                NavigatorManager.geolocation.getCurrentPosition(function (coords) {
                    resolve(coords);
                });
            }).then((coords) => {
                mediaData.geolocation = coords;
                console.log(mediaData.geolocation);
                me.addMediaToCookie(mediaData);
            });
        else
            me.addMediaToCookie(mediaData);

        return mediaData.guid;
    },
    // addMedia: function (media, ordemServicoChecklistId, tipo) {
    //     var me = this,
    //         mediaData;

    //     new Ext.Promise(function (resolve, reject) {
    //         NavigatorManager.geolocation.getCurrentPosition(function (coords) {
    //             resolve(coords);
    //         })
    //     }).then((coords) => {
    //         mediaData = media.guid ? media : {
    //             guid: TskPlanManager.newGuid(),
    //             geolocation: coords,
    //             media: media,
    //             ordemServicoChecklistId: ordemServicoChecklistId,
    //             tipo: tipo
    //         };
    //     });


    //     return mediaData.guid;
    // },

    //Remove media a fila

    addMediaToCookie: function (mediaData) {
        var me = this;

        clearTimeout(me.syncTimeout);

        if (me.cookieInUse) {
            setTimeout(me.addMedia(mediaData), 500);
            return mediaData.guid;
        }

        me.cookieInUse = true;

        me.addImagemLocal(mediaData);

        me.mediaAwaiting.push(mediaData);

        me.setCookie();

        me.cookieInUse = false;

        me.syncTimeout = setTimeout(function () {
            me.sync();
        }, 3000);
    },


    //Remove media a fila
    deleteMedia: function (mediaData) {
        var me = this;

        if (me.cookieInUse) {
            setTimeout(me.deleteMedia(mediaData), 1500);
            return false;
        }

        me.cookieInUse = true;

        Ext.Array.remove(me.mediaAwaiting, mediaData);

        me.setCookie();

        me.cookieInUse = false;
    },

    //Pega a fila de media  nos cookies
    // getLocalMedia: function () {
    //     var me = this;

    //     me.mediaAwaiting = me.getCookie();
    // },

    //Pega media no azure
    getMedia: function (id) {
        return 'link do azure';
    },

    //Envia array de midia para o servidor salvar
    sync: function () {
        var me = this;

        if (me.cookieInUse) {
            setTimeout(me.sync(), 500);
            return false;
        }

        //salva media
        Ext.each(me.mediaAwaiting, Ext.bind(me.saveFile, me));
    },

    //Salva arquivo no servidor
    saveFile: function (mediaData) {
        var me = this;

        //<debug>
        console.groupCollapsed(`Salvando media [${mediaData.guid}]`);
        console.log(mediaData)
        console.groupEnd(`Salvando arquivo`);
        //</debug>

        //Remove possiveis erros armazenados nos cookies
        if (!mediaData.media) {
            me.deleteMedia(mediaData);
        }

        //Adiciona localização nula caso nao tiver sido preenchida
        if (!mediaData.geolocation) {
            mediaData.geolocation = {
                coords: {
                    latitude: 0,
                    longitude: 0,
                    altitude: 0,
                    accuracy: 0,
                    altitudeAccuracy: 0,
                    heading: 'N 0°',
                    speed: 0
                },
                timestamp: +new Date()
            };
        }

        me.toDataUrl(mediaData.media, function (myBase64) {
            var varSplit = myBase64.split(';'),
                mimeType = varSplit[0].replace('data:', ''),
                base64 = varSplit[1].replace('base64,', ''),
                nome = mediaData.media.split('/').pop();

            Ext.Ajax.request({
                url: window.API_CONFIG.Media,
                async: false,
                method: 'POST',
                jsonData: {
                    "Guid": mediaData.guid,
                    "Latitude": `${mediaData.geolocation.coords.latitude}`,
                    "Longitude": `${mediaData.geolocation.coords.longitude}`,
                    "Extensao": mimeType,
                    "Nome": nome,
                    "Base64": base64 //myBase64 <- voltar isso aq qnd for aceitar outros tipos de arquivo
                },
                timeout: 300000,
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjUzIiwibmFtZWlkIjoiQlNvdXJjZVgiLCJuYmYiOjE1OTA1MDQ4OTMsImV4cCI6MjUzNzIzMjg5MywiaWF0IjoxNTkwNTA0ODkzfQ.axGrOWhQ7Ed79yTMPzrPrkar6IcPfZLfk7X5iKc5PO4' //+ window.bearerToken

                },
                success: function (response, opts) {
                    //<debug>
                    console.log(`Sucesso [${mediaData.guid}]`);
                    //</debug>
                    me.deleteMedia(mediaData);
                },
                failure: function (response, opts) {
                    //<debug>
                    console.log(`Erro [${mediaData.guid}]`);
                    console.log('Desenvolver tratamento p/ fila de erros');
                    //</debug>
                    me.deleteMedia(mediaData);
                    me.mediaAwaitingError.push(mediaData);
                }
            });
        });
    },

    setCookie: function () {
        var me = this;

        Ext.util.Cookies.set(me.cookieName, JSON.stringify(me.mediaAwaiting));
    },

    getCookie: function () {
        var me = this;

        return Ext.util.Cookies.get(me.cookieName);
    },

    toDataUrl: function (url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function () {
            var reader = new FileReader();
            reader.onloadend = function () {
                callback(reader.result);
            }
            reader.readAsDataURL(xhr.response);
        };
        xhr.open('GET', url);
        xhr.responseType = 'blob';
        xhr.send();
    },

    base64ArrayBuffer: function (arrayBuffer) {
        var base64 = '',
            encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/',
            bytes = new Uint8Array(arrayBuffer),
            byteLength = bytes.byteLength,
            byteRemainder = byteLength % 3,
            mainLength = byteLength - byteRemainder,
            a, b, c, d,
            chunk;

        // Main loop deals with bytes in chunks of 3
        for (var i = 0; i < mainLength; i = i + 3) {
            // Combine the three bytes into a single integer
            chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];
            // Use bitmasks to extract 6-bit segments from the triplet
            a = (chunk & 16515072) >> 18; // 16515072 = (2^6 - 1) << 18
            b = (chunk & 258048) >> 12; // 258048   = (2^6 - 1) << 12
            c = (chunk & 4032) >> 6; // 4032     = (2^6 - 1) << 6
            d = chunk & 63; // 63       = 2^6 - 1
            // Convert the raw binary segments to the appropriate ASCII encoding
            base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d];
        }
        // Deal with the remaining bytes and padding
        if (byteRemainder == 1) {
            chunk = bytes[mainLength]
            a = (chunk & 252) >> 2; // 252 = (2^6 - 1) << 2
            // Set the 4 least significant bits to zero
            b = (chunk & 3) << 4; // 3   = 2^2 - 1
            base64 += encodings[a] + encodings[b] + '==';
        } else if (byteRemainder == 2) {
            chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];
            a = (chunk & 64512) >> 10; // 64512 = (2^6 - 1) << 10
            b = (chunk & 1008) >> 4; // 1008  = (2^6 - 1) << 4
            // Set the 2 least significant bits to zero
            c = (chunk & 15) << 2; // 15    = 2^4 - 1
            base64 += encodings[a] + encodings[b] + encodings[c] + '=';
        }
        return base64;
    },

    addImagemLocal: function (mediaData) {
        var me = this;

        me.storeImagem.add({
            ImageGuid: mediaData.guid,
            LocalURL: mediaData.media,
            isSync: 1,
            isCreated: 1
        });
    },

    getImagemLocal: function (guid) {
        var me = this,
            imagem = me.storeImagem.data.find('ImageGuid', guid);

        return imagem ? imagem.get('LocalURL') : null;
    }
});