Ext.define('TskPlan.Mobile.lib.DataManager', {
    singleton: true,

    dirtStore: Ext.create('TskPlan.Mobile.store.indexedDB.DirtStores'),
    dirtRecordStore: Ext.create('TskPlan.Mobile.store.indexedDB.DirtRecords'),

    lastRun: null,
    syncTask: null,
    syncTimer: (60 * 1000) * 0.3, //(60 * 1000) * 5 = 5 minutos
    //<debug>
    syncTimer: 9999999,
    //</debug>
    syncRunning: false,

    syncTries: 0,
    syncTriesStamps: [
        1000, //1s
        1000, //1s
        2000, //2s
        3000, //3s
        5000, //5s
        15000, //15s
        30000, //30s
        60000, //60s
        60000, //60s
        120000, //2m
        300000, //5m
    ],

    config: {
        syncTries: 0,
        syncTriesStamps: [],
        syncTriesTime: 0
    },

    constructor: function () {
        var me = this;

    },

    //Operações da API
    operations: {
        request: function (url, method, jsonData, params, onSuccess, onFailure) {
            Ext.Ajax.request({
                url: url,
                method: method,
                jsonData: jsonData ? jsonData : null,
                params: params ? params : {},
                success: onSuccess,
                failure: onFailure
            });
        },

        read: function (url, proxy, onFinish, params) {
            var me = this,
                onSucess = function (response, opts) {
                    var obj = response && response.responseText ? Ext.decode(response.responseText) : null;

                    DataManager.updateLocalStoredData(obj, proxy, onFinish, response);
                };

            if (proxy.extraParams)
                Object.assign(params, proxy.extraParams);

            me.request(url, 'GET', null, params, onSucess, me.defaultFailure);
        },

        post: function (url, proxy, data, onFinish, params) {
            var me = this,
                onSucess = function (response, opts) {
                    var obj = response && response.responseText ? Ext.decode(response.responseText) : null;

                    //<debug>
                    console.warn('Marcar registros como sincronizados');
                    //</debug>

                    if (response && response.status == 200) DataManager.removeDirtRecord(data.guid);

                    DataManager.updateLocalStoredData(obj, proxy, onFinish, response);
                };

            //Estudar necessidade pois a store dirtRecords ja armazena os parametros qnd vai realizar esta operação
            // if (proxy.extraParams)
            //     Object.assign(params, proxy.extraParams);

            me.request(url, 'POST', data, params, onSucess, me.defaultFailure);
        },

        put: function (url, proxy, data, onFinish, params) {
            var me = this,
                onSucess = function (response, opts) {
                    var obj = response && response.responseText ? Ext.decode(response.responseText) : null;

                    //<debug>
                    console.warn('Marcar registros como sincronizados');
                    //</debug>

                    if (response && response.status == 200)
                        DataManager.removeDirtRecord(data.guid);

                    DataManager.updateLocalStoredData(obj, proxy, onFinish, response);
                };

            me.request(url, 'PUT', data, params, onSucess, me.defaultFailure);
        },

        delete: function (url, proxy, data, onFinish, params) {
            var me = this,
                operation = {
                    method: '',
                    response: ''
                },
                onSucess = function (response, opts) {
                    var obj = response && response.responseText ? Ext.decode(response.responseText) : null;

                    //<debug>
                    console.warn('Remover registro como sincronizados');
                    //Dar delete no rec na dirtStore
                    debugger;
                    //</debug>

                    if (response && response.status == 200)
                        DataManager.removeDirtRecord(data.guid);

                    DataManager.updateLocalStoredData(obj, proxy, onFinish, response);
                };

            me.request(url, 'DELETE', data, params, onSucess, me.defaultFailure);
        },

        defaultFailure: function (response, opts) {
            if (opts.method !== 'GET') {
                var recordData = opts.jsonData,
                    storeRecord;

                if (recordData && recordData.guid)
                    storeRecord = DataManager.dirtRecordStore.findRecord('RecordGuid', recordData.guid);

                if (storeRecord)
                    storeRecord.set('Tries', storeRecord.get('Tries') + 1);
            }

            opts.success();

            console.log('server-side failure with status code ' + response.status);

            //<debug>
            debugger;
            //</debug>
        }
    },

    //Retorna a stamp time de espera para a proxima tentativa de sincronização
    getSyncTriesTime: function () {
        var me = this,
            length = me.syncTriesStamps.length;

        return me.syncTries > length ?
            me.syncTriesStamps[length - 1] : //Retorna a ultima stamp
            me.syncTriesStamps[me.syncTries]; //Retorna a stamp atual
    },

    //Defini a tarefa de sync de tempo em tempo
    setSyncTask: function (forceRun) {
        var me = this,
            //Envia as stores nao sincronizadas para o metodo sync
            syncDirtStores = function () {
                var conexao = me.getConnection(),
                    dirtStores = me.getDirtStores();

                me.syncRunning = true;

                //<debug>
                //</debug>

                if (conexao) {
                    if (dirtStores.length > 0) {
                        me.syncStores(dirtStores, {
                            syncTask: true
                        });
                    } else {
                        //<debug>
                        console.log(`Nenhuma store a ser sincronizada`);
                        //</debug>
                        me.syncRunning = false;
                        me.setSyncTask();
                    }
                } else {
                    var tryTime = me.getSyncTriesTime(),
                        sec = (tryTime / 1000);

                    me.syncTries++;

                    //<debug>
                    console.log(`[tentativa ${me.syncTries}] Não foi possivel sincronizar, tentando novamente em [${(Ext.util.Format.plural(sec,'segundo','segundos'))}]`);
                    //</debug>

                    setTimeout(function () {
                        syncDirtStores();
                    }, tryTime);
                }

                //Aplicar utilização do Ext.bind nesta classe
                //Ext.bind(this.openDbSuccess, this, [request, callback, scope], false);
            },
            starTask = function () {
                //<debug>
                console.log('Iniciando task de sincronização');
                //</debug>
                me.syncTries = 0;
                syncDirtStores();
            };


        //<debug>
        console.warn('getMaxDate - Mover o uso deste metodo para store.getLastSyncDate?');
        console.warn('Atualizar(Adicionar/Remover) indices de uma store ja criada');
        console.warn('Verificar Conexão em .getConnection antes de iniciar sincronização');
        //</debug>

        if (me.syncRunning)
            return;

        if (!forceRun)
            me.syncStore = setTimeout(starTask, me.syncTimer);
        else
            starTask();
    },

    //Retorna as stores não sincronizadas
    getDirtStores: function () {
        var me = this,
            stores = [];

        //<debug>
        //Deixei o metodo para caso precise ser aplicado alguma regra sobre as stores capturadas
        //</debug>

        stores = me.dirtStore.data.items.map(rec => rec.get('Store'));

        return stores;
    },

    //Carrega novos dados (api) para atualizar o (local)
    loadStoredData: function (store, onFinish) {
        var me = this,
            proxy = store.getProxy(),
            apiProxy = store.getApiProxy();

        me.operations.read(apiProxy.api.read, proxy, onFinish, {});
    },

    //Atualiza dados retornados da api no IDB 
    updateLocalStoredData: function (records, proxy, onFinish, operationResponse) {
        var me = this,
            dataBase = proxy.getDbName(),
            objectModel = proxy.getObjectStoreName(),
            request,
            db,
            nonReadOperation = operationResponse && operationResponse.request.method !== 'GET';

        request = window.indexedDB.open(dataBase, 1);

        //Callback for success upon DB open
        request.onsuccess = function (event) {
            db = event.target.result;

            var store = db.transaction(objectModel, 'readwrite').objectStore(objectModel);

            if (!store) {
                //<debug>
                console.warn('Tratar store nao criada');
                //</debug>
                return;
            }

            if (records)
                records.Data.forEach(rec => {
                    rec.guid = rec.guid ? rec.guid : TskPlanManager.newGuid();

                    var reg = store.get(rec.guid);

                    reg.onsuccess = function (event) {
                        var cursor = event.target.result;

                        //Possui registro && (esta sincronizado || (operação de edição && status 200 OK)) || Registro nao esta sync mas não esta na syncStore
                        if (cursor && (cursor.isSync || (nonReadOperation && operationResponse && operationResponse.status == 200)) || (!nonReadOperation && DataManager.dirtRecordStore.findRecord('RecordGuid', rec.guid) == null))
                            store.put(rec);
                        else
                            store.add(rec);
                    };

                    reg.onerror = function (event) {
                        //<debug>
                        console.groupCollapsed('Não foi possivel criar/atualizar o registro');
                        console.log(event);
                        console.groupEnd();
                        //</debug>
                    };
                });

            if (onFinish)
                onFinish(records);
        };
    },

    //sincroniza todas as stores
    syncStores: function (stores, options) {
        var me = this,
            total = 0,
            //Sincroniza a store
            syncStore = function (storeArray, syncOptions) {
                var storeName = storeArray[0],
                    storeClass = eval(storeName),
                    storeModel = eval(storeClass.$config.values.model),
                    modelProxy = storeModel.getProxy(),
                    url = storeClass.$config.values.proxy.url,
                    //Finaliza sincronização e inicia a proxima da fila
                    fnFinishStoreSync = function () {
                        Ext.Array.remove(storeArray, storeArray[0]);

                        if (DataManager.dirtRecordStore.find('Store', storeName) < 0) {
                            DataManager.dirtStore.remove(DataManager.dirtStore.findRecord('Store', storeName));
                            //<debug>
                            if (syncOptions.syncTask)
                                console.log(`Store [${storeName}] sincronizada`);
                            //</debug>
                        } else {
                            //<debug>
                            console.log(`Store [${storeName}] com pendencias a sincronizar`);
                            //</debug>
                        }

                        Ext.globalEvents.fireEvent('appLoadingProgressUpdate', {
                            store: storeName,
                            current: syncOptions.current,
                            total: total
                        });

                        me.syncStores(storeArray, syncOptions);
                    };

                modelProxy.ensureDb({}, function () {
                    //Caso não for uma phantomStore inicia a sinc com a api
                    if (!storeClass.$config.values.iDBPhantomStore) {
                        me.getMaxDate(modelProxy.getDbName(), modelProxy.getObjectStoreName(), function (maxDate) {
                            var records = DataManager.dirtRecordStore.data.items.filter(rec => rec.get('Store') == storeName),
                                //Ações a serem tomadas
                                actions = {
                                    post: function (iDBRecord, params) {
                                        me.operations.post(url, modelProxy, iDBRecord, function () {}, params);
                                    },
                                    put: function (iDBRecord, params) {
                                        me.operations.put(url, modelProxy, iDBRecord, function () {}, params);
                                    },
                                    delete: function (iDBRecord, params) {
                                        me.operations.delete(url, modelProxy, iDBRecord, function () {}, params);
                                    }
                                };

                            //Actions
                            records.forEach(record => {
                                var params = record.get('Params');

                                storeClass.superclass.getIDBRecord(record.get('RecordGuid'), iDBRecord => {
                                    actions[record.get('Operation')](iDBRecord, params ? params : {});
                                }, modelProxy);
                            });

                            //Read
                            me.operations.read(url, modelProxy, fnFinishStoreSync, maxDate ? {
                                lastSync: maxDate
                            } : {});
                        });
                    } else
                        fnFinishStoreSync();
                }, me);
            };

        me.syncRunning = true;

        if (!options)
            options = {};

        if (!stores) {
            stores = me.getIDBStores();
            total = stores.length;
            options.current = 0;
        } else {
            total = stores.length + options.current;
            options.current = options.current ? options.current : 0;
        }

        if (stores.length) {
            options.current = options.current + 1;
            syncStore(stores, options);
        } else {
            me.updateLastRun();

            Ext.globalEvents.fireEvent('appLoadingFinished');

            //<debug>
            console.log('Stores sincronizadas');
            //</debug>

            me.syncRunning = false;
            me.setSyncTask();
        }
    },

    //Buscar Stores IDB
    getIDBStores: function (folders) {
        var me = this,
            stores = [];

        folders = folders ? folders : TskPlan.Mobile.store;

        for (var item in folders) {
            if (folders[item].$isClass) {
                if (folders[item].$config.values.useIDB)
                    stores.push(folders[item].$className);
            } else {
                var folderStores = me.getIDBStores(folders[item]);
                if (folderStores.length > 0)
                    stores = stores.concat(folderStores);
            }
        }

        return stores;
    },

    //Retorna maior data de sincronização da tabela
    getMaxDate: function (dataBase, objectModel, callback) {
        var me = this,
            request,
            db;

        request = window.indexedDB.open(dataBase, 1);

        //Callback for success upon DB open
        request.onsuccess = function (event) {
            db = event.target.result;

            var store = db.transaction(objectModel, 'readwrite').objectStore(objectModel);

            dataRequest = store.getAll();

            dataRequest.onsuccess = function (event) {
                var dataSet = event.target.result,
                    dates = dataSet.map(rec => new Date(rec.syncDate)),
                    maxDate = new Date(Math.max.apply(null, dates));

                callback((maxDate instanceof Date) && !isNaN(maxDate) ? maxDate : null);
            };

            dataRequest.onerror = function (event) {
                //<debug>
                console.groupCollapsed('Não foi possivel encontrar a maior data');
                console.log(event);
                console.groupEnd();
                //</debug>

                callback(null);
            };
        };
    },

    //Retorna conexão com a internet - mover daq para o NavigatorManager posteriormentes
    getConnection: function () {
        ''
        var conn = true;

        return conn;
    },

    //
    updateLastRun: function () {
        var me = this;

        //<debug>
        console.warn('Definir data limite para ficar sem atualização apos a ultima data ser definida');
        //</debug>
        me.lastRun = new Date();
    },

    removeDirtRecord: function (guid) {
        var me = this,
            record = DataManager.dirtRecordStore.findRecord('RecordGuid', guid);

        if (!record) {
            //<debug>
            console.log(`Registro [${guid}] não removido pois nao foi encontrado na dirtRecordStore`)
            //</debug>
            return;
        };

        DataManager.dirtRecordStore.remove(record);

        //<debug>
        console.log(`Registro [${guid}] removido da dirtRecordStore`)
        //</debug>
    },

    //<debug>
    //Função para testes
    teste: function (dtb, obj, indice) {
        var me = this,
            request,
            db,
            dataBase = dtb ? dtb : 'obra.Obra',
            dataObject = obj ? obj : 'obra.Obra';

        request = window.indexedDB.open(dataBase, 1);

        //Callback for success upon DB open
        request.onsuccess = function (event) {
            db = event.target.result;

            var store = db.transaction(dataObject, 'readwrite').objectStore(dataObject);

            //<debug>
            debugger;
            //</debug>

            // if (value)
            //     dataRequest = store.index(indice ? indice : 'isSync').get(value);
            // else
            dataRequest = store.index(indice ? indice : 'isSync').getAll();

            dataRequest.onsuccess = function (event) {
                var dataSet = event.target.result;
                // dates = dataSet.map(rec => new Date(rec.syncDate));

                //<debug>
                debugger;
                //</debug>
            };

            dataRequest.onerror = function (event) {
                //<debug>
                console.groupCollapsed('Não foi possivel encontrar a maior data');
                console.log(event);
                console.groupEnd();
                //</debug>

                //<debug>
                debugger;
                //</debug>
            };
        };

    }
    //</debug>
});