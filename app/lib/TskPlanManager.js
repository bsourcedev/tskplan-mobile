Ext.define('TskPlan.Mobile.lib.TskPlanManager', {
    singleton: true,

    constructor: function () {
        var me = this;

    },

    newGuid: function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0,
                v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        }).toUpperCase();
    },

    progressBar: function (valor) {
        return `<div class="barra-progresso"><div class="barra-progresso-valor" style="width:${valor}%"></div></div>`;
    },

    util: {
        firstLetterUppercase: function (word) {
            return word.charAt(0).toUpperCase() + word.slice(1)
        }
    }
});