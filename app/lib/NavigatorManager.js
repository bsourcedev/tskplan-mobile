Ext.define('TskPlan.Mobile.lib.NavigatorManager', {
    singleton: true,
    cordovaActive: false,

    constructor: function () {
        var me = this;

        document.addEventListener("deviceready", onDeviceReady, false);

        function onDeviceReady() {
            console.log('Cordova ativo');
            // Ext.Msg.alert('Cordova ativo');
            me.cordovaActive = true;
            //Bloquear Rotação
            // screen.orientation.lock('portrait-primary');
            me.overrideBackButton();
        }
    },

    overrideBackButton: function () {
        if (window.ScreenManager && NavigatorManager.cordovaActive) {
            if (!ScreenManager.mainScreenActive && ScreenManager.hookBackButton) {
                document.addEventListener("backbutton", () => window.ScreenManager.voltar(), false);
            } else if (ScreenManager.mainScreenActive) {
                document.removeEventListener("backbutton", () => {
                    navigator.app.exitApp()
                }, false);
                // console.log(`Removido 1 \n mainScreenActive: ${ScreenManager.mainScreenActive}\n hookBackButton: ${ScreenManager.hookBackButton}`);
                // window.plugins.appMinimize.minimize();

            }
        } else {
            //<debug>
            //não fechar o app aq se não toda vez q um botão de menu for clicado o app ser fechado
            //</debug>
            document.removeEventListener("backbutton", () => {}, false);
        }
    },

    camera: {
        getPicture: function (onSuccess, onFailure, config) {
            var me = this;

            if (NavigatorManager.cordovaActive)
                navigator.camera.getPicture(
                    onSuccess ? onSuccess : me.onSuccessEmpty,
                    onFailure ? onFailure : me.onFailureEmpty,
                    config ? config : {
                        quality: 60,
                        destinationType: Camera.DestinationType.FILE_URI
                    });
            else {
                var results = '/resources/cordova-no-image.png';

                onSuccess ? onSuccess(results) : me.onSuccessEmpty(results);
            }
        },

        onSuccessEmpty: function () {
            console.log('Camera - Sucesso');
            console.log(arguments);
        },

        onFailureEmpty: function () {
            console.log('Camera - Falha');
            console.log(arguments);
        }
    },

    geolocation: {
        getCurrentPosition: function (onSuccess, onFailure, config) {
            var me = this;

            if (NavigatorManager.cordovaActive)
                navigator.geolocation.getCurrentPosition(
                    onSuccess ? onSuccess : me.onSuccessEmpty,
                    onFailure ? onFailure : me.onFailureEmpty,
                    config ? config : {

                    });
            else {
                var result = {
                    coords: {
                        latitude: -22.74638053322089,
                        longitude: -47.34234387060771,
                        altitude: 100,
                        accuracy: 150,
                        altitudeAccuracy: 80,
                        heading: 'N 0°',
                        speed: 0
                    },
                    timestamp: +new Date()
                };

                return onSuccess ? onSuccess(result) : me.onSuccessEmpty(result);
            }
        },

        onSuccessEmpty: function (geolocation) {
            console.log('Geolocation - Sucesso');

            return geolocation;
        },

        onFailureEmpty: function () {
            console.log('Geolocation - Falha')
            console.log(arguments);

            return null;
        }
    },

    file: {
        download: function (fileurl, filename) {
            var blob = null,
                xhr = new XMLHttpRequest(),
                webDownload = function (url, fileName) {
                    var xhr = new XMLHttpRequest();
                    xhr.open("GET", url, true);
                    xhr.responseType = "blob";
                    xhr.onload = function () {
                        var urlCreator = window.URL || window.webkitURL;
                        var imageUrl = urlCreator.createObjectURL(this.response);
                        var tag = document.createElement('a');
                        tag.href = imageUrl;
                        tag.download = fileName;
                        document.body.appendChild(tag);
                        tag.click();
                        document.body.removeChild(tag);
                    }
                    xhr.send();
                };

            if (!fileurl) {
                //<debug>
                console.log('Link do arquivo não informado')
                //</debug>
                return;
            }
            if (!filename)
                filename = fileurl.split('/').pop();

            if (NavigatorManager.cordovaActive) {
                xhr.open("GET", fileurl);
                xhr.responseType = "blob"; //force the HTTP response, response-type header to be blob
                xhr.onload = function () {
                    blob = xhr.response; //xhr.response is now a blob object

                    console.log(blob);

                    var storageLocation = "";
                    switch (device.platform) {
                        case "Android":
                            storageLocation = 'file:///storage/emulated/0/';
                            break;
                        case "iOS":
                            storageLocation = cordova.file.documentsDirectory;
                            break;
                    }
                    var folderpath = storageLocation + "Download",
                        DataBlob = blob,
                        filePath = `${folderpath}/${filename}`;

                    NavigatorManager.file.checkIfFileExists(filePath, () => {
                        NavigatorManager.file.open(filePath, DataBlob.type)
                    }, () => {
                        //<debug>
                        console.warn('Verificar conexão com a internet antes de tentar realizar o download')
                        //</debug>
                        window.resolveLocalFileSystemURL(folderpath, function (dir) {
                            dir.getFile(filename, {
                                create: true
                            }, function (file) {
                                file.createWriter(function (fileWriter) {
                                    fileWriter.write(DataBlob);
                                    //Download was succesfull
                                    NavigatorManager.file.open(filePath, DataBlob.type)
                                }, function (err) {
                                    // failed
                                    console.log(err);
                                });
                            });
                        });
                    });
                }
                xhr.send();
            } else {
                webDownload(fileurl, filename);
            }
        },
        open: function (filePath, mimeType) {
            if (NavigatorManager.cordovaActive) {
                window.resolveLocalFileSystemURL(filePath, function success(fileEntry) {
                    cordova.plugins.fileOpener2.open(
                        fileEntry.toInternalURL(),
                        mimeType
                    );
                });
            } else {
                //<debug>
                console.log('Abertura de arquivos não desenvolvida para web')
                //</debug>
            }
        },
        checkIfFileExists: function (path, fileExists, fileDoesNotExist) {
            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
                fileSystem.root.getFile(path, {
                    create: false
                }, fileExists, fileDoesNotExist);
            }, () => {
                console.log('error while checking if file exists;');
                console.log(arguments)
            }); //of requestFileSystem
        }
    }
});