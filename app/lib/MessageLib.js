﻿Ext.define('TskPlan.Mobile.lib.MessageLib', {
    singleton: true,
    //chat ativo(component) 
    activeChannel: null,
    servers: [],
    serversLoading: false,

    constructor: function () {
        var me = this;


        console.log('MessageLib started')
    },

    //Envio de mensagens
    sendMessage: function (message) {
        var me = this,
            data = JSON.stringify({
                messageId: message.messageId ? message.messageId : window.TskPlanManager.newGuid(),
                messageType: message.messageType ? message.messageType : 'text',
                messageText: message.messageText ? message.messageText : '',
                senderCreationDate: message.senderCreationDate ? message.senderCreationDate : new Date(),
                status: 0,
                urlItem: message.urlItem ? message.urlItem : '',
                channel: message.channel ? message.channel : '',
                channelName: message.channelName ? message.channelName : '',
                responseGuid: message.responseGuid ? message.responseGuid : null,
                server: message.server ? message.server : '',
                serverName: message.serverName ? message.serverName : '',
                serverShortName: message.serverShortName ? message.serverShortName : '',
                user: TSKPLAN_SESSION.token.nameid,
                userName: TSKPLAN_SESSION.token.nameid,
            }),
            postData = function (message) {
                me.saveMessage(JSON.parse(data));
                me.SendMessageToServiceWorker('sendMessage', data)
            };

        console.log('%c Alterar para pegar o user da sessao', 'color:#ff47fa');

        if (message.media) {
            var postMedia = new Promise(function (resolve, reject) {
                me.sendMessageMedia(message.media,
                    function (url) {
                        data.urlItem = url;
                        resolve("Success!")
                    },
                    function (message) {
                        reject("[Error] " + message);
                    });
            });

            postMedia.then(function (value) {
                postData(data);
            }, function (reason) {
                console.log(reason); // Error!
            });
        } else {
            //Envia para o messageWorker
            postData(data);

            //Envia mensagem para o socket
            if (window.messageWorker)
                window.messageWorker.postMessage({
                    route: 'message',
                    data: data
                });
        }
    },
    //Envio de media da mensagem
    sendMessageMedia: function (media, successCallback, failureCallback) {
        successCallback('')
    },
    //Registra token do Firebase
    registerToken: function (token) {
        if (!token) {
            console.log('Token não informado')
            return false;
        }

        var me = this,
            data = JSON.stringify({
                token: token
            });

        //me.postHandler('/TokenReg', data);
        me.SendMessageToServiceWorker('tokenReg', data)
    },
    //Registra sessão do usuario
    registerSession: function (session) {
        if (!session) {
            console.log('Sessao não informada')
            return false;
        }

        if (window.messageWorker)
            window.messageWorker.postMessage({
                route: 'sessionReg',
                data: {
                    session: session
                }
            });
    },
    //Recebimento de mensagens
    receiveMessage: function (message) {
        var me = this,
            body = message.notification ? JSON.parse(message.notification.body) : message;

        me.saveMessage(body);
        me.notifyUser(body);

        me.updateChannelLastMessage(body);

        if (me.activeChannel && me.activeChannel.channel.id == body.channel)
            me.activeChannel.addMessage({
                messageId: body.messageId,
                messageData: body,
                primeiraMensagem: true,
                owner: body.user.toUpperCase() == TSKPLAN_SESSION.token.nameid.toUpperCase() ? true : false,
                data: {
                    usuario: body.userName,
                    mensagem: body.messageText,
                    horario: me.activeChannel.getHorario((typeof body.senderCreationDate === 'string' ? new Date(body.senderCreationDate) : body.senderCreationDate)),
                    primeiraMensagem: true,
                    owner: body.user.toUpperCase() == TSKPLAN_SESSION.token.nameid.toUpperCase() ? true : false,
                    text: body.messageType == 'text' ? true : false,
                    audio: body.messageType == 'audio' ? true : false,
                    audioData: body.urlItem,
                    audioComponent: body.messageType == 'audio' && body.urlItem !== '' ? new Audio(body.urlItem) : null,
                    urlItem: body.urlItem
                }
            });

        console.log('%cMessage received', 'color: #1eb6ff', body);
    },
    //Recebimento de mensagens em lote
    receiveMessageBuffer: function (data) {
        var me = this;

        if (data.length > 0) {
            //Salva mensagens
            data.forEach(function (message) {
                message.senderCreationDate = message.senderCreationDate + (message.senderCreationDate.slice(-1) == 'Z' ? '' : 'Z')
                me.saveMessage(message);
            });

            //Atualiza ultima mensagem de cada canal
            var channels = data.map(x => x.channel);

            //Filtra valores unicos
            channels = channels.filter(function (value, index, self) {
                return self.indexOf(value) === index;
            })

            channels.forEach(function (channel) {
                var channelMessages = data.filter(x => x.channel == channel),
                    mostRecentMessageMaxDate = Math.max.apply(null, channelMessages.map(function (e) {
                        return new Date(e.senderCreationDate);
                    })),
                    mostRecentMessageIndex = channelMessages.findIndex(x => new Date(x.senderCreationDate).getTime() == mostRecentMessageMaxDate),
                    mostRecentMessage = data[mostRecentMessageIndex];

                setTimeout(() => {
                    me.updateChannelLastMessage(mostRecentMessage);
                }, 600);

                //Melhorar este treco
                if (me.activeChannel && me.activeChannel.channel.id == mostRecentMessage.channel) {
                    me.activeChannel.configuraChat();
                }
            })
        }
    },
    //marcar mensagem como lida
    markAsRead: function (messageId) {

    },
    //Recebe a marcação de entrega de uma mensagem
    receiveSentMark: function () {

    },
    //Recebe a marcação de recebimento de uma mensagem
    receiveReadMark: function () {

    },
    //Envia mensagem ao service worker
    SendMessageToServiceWorker: function (method, data) {
        window.serviceWorker.postMessage({
            method: method,
            data: data
        });

        // navigator.serviceWorker.ready.then(registration => {
        //     registration.active.postMessage({
        //         method: method,
        //         data: data
        //     });
        // });
    },
    //Enviar os post para o SW
    postHandler: function (url, data) {
        var ajax = new XMLHttpRequest();
        ajax.withCredentials = true;

        // Seta tipo de requisição: Post e a URL da API
        ajax.open("POST", url, true);
        ajax.setRequestHeader("Content-type", "application/json");
        //ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");

        // Seta paramêtros da requisição e envia a requisição
        ajax.send(data);

        // Cria um evento para receber o retorno.
        ajax.onreadystatechange = function () {

            // Caso o state seja 4 e o http.status for 200, é porque a requisiçõe deu certo.
            if (ajax.readyState == 4 && ajax.status == 200) {

                var data = ajax.responseText;

                // Retorno do Ajax
                console.log(data);
            }
        }
    },
    //Notifica o usuario de uma nova mensagem
    notifyUser: function () {
        var me = this;
    },
    //Carrega os canais abertos do usuario
    loadUserChannels: function () {
        var me = this;

        me.SendMessageToServiceWorker('getUserChannel', {
            usuario: TSKPLAN_SESSION.token.nameid
        });
    },
    //
    loadUserChannelsCallback: function (data) {
        var me = this,
            serversFilter = data.map(x => x.Server).filter(function onlyUnique(value, index, self) {
                return self.indexOf(value) === index;
            });

        serversFilter.forEach(function (serverId) {
            me.registerServerBuffer(serverId);
        })

        data.forEach(function (channel) {
            me.registerChannel({
                id: channel.Channel,
                name: channel.ChannelName,
                server: channel.Server,
                serverName: channel.ServerName,
                serverShortName: channel.ServerShortName
            }, true);

            console.log('%c Descomentar aqui', 'color:#ff47fa');
            me.getChannelMessageBuffer(channel.Channel);
        });
    },
    //Salva o servidor dos canais do usuario
    registerServerBuffer: function (serverId) {
        var me = this,
            loadServers = function () {
                me.serversLoading = true;

                me.storeServers = me.storeServers || Ext.create('TskPlan.Mobile.store.chat.Servers');

                me.storeServers.getProxy().extraParams = {
                    TodosServers: true
                };

                me.storeServers.load({
                    callback: function (data) {
                        if (data.length > 0)
                            me.servers = data.map(x => x.data);

                        me.serversLoading = false;

                        me.registerServerBuffer(serverId);
                    }
                });
            };

        //Se estiver carregando espera 1s p tentar novamente
        if (me.serversLoading) {
            setTimeout(function () {
                me.registerServerBuffer(serverId);
            }, 1000)
        }
        //Se nao houver servers carregado inicia o load
        else if (me.servers.length == 0) {
            loadServers();
        } else {
            var serverData = me.servers.find(x => x.Id == serverId);

            //se nao encontrar o servidor da load novamente na lista, caso encontrar, envia para cadastro no IDB
            if (serverData) {
                var _server = {
                    id: serverData.Id,
                    obraId: serverData.ObraId,
                    serverName: serverData.ServerName,
                    shortName: serverData.ShortName,
                    iconUrl: serverData.IconUrl
                };

                me.registerServer(_server);
            } else
                loadServers();
        }
    },
    //Pega as mensagens do canal
    getChannelMessageBuffer: function (channelGuid) {
        var me = this;

        me.SendMessageToServiceWorker('getChannelMessages', {
            channel: channelGuid
        });
    },
    //Atualiza a ultima mensagem do chat
    updateChannelLastMessage: function (lastMessage) {
        var me = this,
            request = window.indexedDB.open('user-channels', 1);

        request.onupgradeneeded = me.userChannelOnUpgradeNeeded;

        request.onerror = function (event) {
            console.log('[updateChannelLastMessage] request.onerror')
        };

        request.onsuccess = function (event) {
            var db = request.result,
                transaction = db.transaction(['user-channels'], "readwrite"),
                objectStore = transaction.objectStore('user-channels'),
                updated = false;

            console.log('%c Alterar para pegar o login da sessao', 'color:#ff47fa'); //Todos 

            // report on the success of the transaction completing, when everything is done
            transaction.oncomplete = function (event) {
                if (ScreenManager.telaAberta &&
                    ScreenManager.telaAberta.screen &&
                    ScreenManager.telaAberta.screen.xtype == 'chat-list-tela' &&
                    updated) {
                    var screenMessage = ScreenManager.telaAberta.screen,
                        screenMessageController = ScreenManager.telaAberta.screen.getController();

                    screenMessageController.onUpdateChannelsLastMessage(lastMessage);
                }

                console.log('%c Adicionar mensgem recebida ao chat', 'color:#ff47fa')

            };

            transaction.onerror = function (event) {
                console.log('[updateChannelLastMessage] transaction.onerror')
            };

            objectStore.get(TSKPLAN_SESSION.token.nameid + '-' + lastMessage.channel.toUpperCase()).onsuccess = function (event) {
                var channelData = {
                    id: lastMessage.channel,
                    name: lastMessage.channelName,
                    server: lastMessage.server.toUpperCase(),
                    lastMessage: lastMessage
                };
                if (event.target.result && event.target.result.channel.lastMessage) {
                    var dataUltimaSalva = new Date(event.target.result.channel.lastMessage.senderCreationDate + (event.target.result.channel.lastMessage.senderCreationDate.slice(-1) == 'Z' ? '' : 'Z')),
                        dataNova = new Date(lastMessage.senderCreationDate);

                    if (dataUltimaSalva.getTime() > dataNova.getTime())
                        return false;
                }

                objectStore.put({
                    id: TSKPLAN_SESSION.token.nameid + '-' + lastMessage.channel.toUpperCase(),
                    user: TSKPLAN_SESSION.token.nameid,
                    channel: channelData
                });

                updated = true;
            }
        }
    },
    //Registra o usuario em um canal
    registerChannel: function (channelData, registerLoop) {
        var me = this,
            channel = "user-channels",
            request = window.indexedDB.open(channel, 1);

        console.log('%c Alterar para pegar o login da sessao', 'color:#ff47fa'); //Todos

        request.onupgradeneeded = me.userChannelOnUpgradeNeeded;

        request.onsuccess = function (event) {
            var db = request.result,
                transaction = db.transaction([channel], "readwrite"),
                objectStore = transaction.objectStore(channel);

            // report on the success of the transaction completing, when everything is done
            transaction.oncomplete = function (event) {};

            transaction.onerror = function (event) {
                console.log('[registerChannel]transaction.onerror')
            };

            objectStore.put({
                id: TSKPLAN_SESSION.token.nameid + '-' + channelData.id.toUpperCase(),
                user: TSKPLAN_SESSION.token.nameid,
                channel: {
                    id: channelData.id,
                    name: channelData.name,
                    server: channelData.server.toUpperCase(),
                    lastMessage: null
                }
            });

            if (!registerLoop)
                me.SendMessageToServiceWorker('registerUserChannel', JSON.stringify({
                    guid: channelData.id
                }));
        };
    },
    //Registra o usuario em um Server
    registerServer: function (serverData) {
        var me = this,
            channel = "user-servers",
            request = window.indexedDB.open(channel, 1);

        console.log('%c Alterar para pegar o login da sessao', 'color:#ff47fa'); //Todos TSKPLAN_SESSION.token.nameid

        request.onupgradeneeded = me.userServerOnUpgradeNeeded;

        request.onsuccess = function (event) {
            var db = request.result,
                transaction = db.transaction([channel], "readwrite"),
                objectStore = transaction.objectStore(channel);

            // report on the success of the transaction completing, when everything is done
            transaction.oncomplete = function (event) {};

            transaction.onerror = function (event) {
                console.log('[registerServer] transaction.onerror')
            };

            objectStore.put({
                id: TSKPLAN_SESSION.token.nameid + '-' + serverData.id.toUpperCase(),
                user: TSKPLAN_SESSION.token.nameid,
                server: {
                    id: serverData.id,
                    obraId: serverData.obraId,
                    serverName: serverData.serverName,
                    shortName: serverData.shortName,
                    iconUrl: serverData.iconUrl,
                    unreadMessages: 0
                }
            });
        };
    },
    //Retorna os canais que o usuario esta ativo
    getUserChannels: function (callback) {
        var me = this,
            request = window.indexedDB.open('user-channels', 1);

        request.onupgradeneeded = me.userChannelOnUpgradeNeeded;

        request.onsuccess = function (event) {
            var db = request.result;

            if (!db.objectStoreNames.contains('user-channels')) {
                callback([]);
                return;
            }

            var transaction = db.transaction(['user-channels'], 'readonly'),
                objectStore = transaction.objectStore('user-channels');

            console.log('%c Alterar para pegar o login da sessao', 'color:#ff47fa'); //Todos TSKPLAN_SESSION.token.nameid

            objectStore.index('user').getAll(TSKPLAN_SESSION.token.nameid).onsuccess = function (event) {
                var cursor = event.target.result;
                callback(cursor)
            }
        }
    },
    //Retorna os servidores que o usuario esta ativo
    getUserServers: function (callback) {
        var me = this,
            request = window.indexedDB.open('user-servers', 1);

        request.onupgradeneeded = me.userServerOnUpgradeNeeded;

        request.onsuccess = function (event) {
            var db = request.result;

            if (!db.objectStoreNames.contains('user-servers')) {
                callback([]);
                return;
            }

            var transaction = db.transaction(['user-servers'], 'readonly'),
                objectStore = transaction.objectStore('user-servers');

            console.log('%c Alterar para pegar o login da sessao', 'color:#ff47fa'); //Todos TSKPLAN_SESSION.token.nameid

            objectStore.index('user').getAll(TSKPLAN_SESSION.token.nameid).onsuccess = function (event) {
                var cursor = event.target.result;
                callback(cursor)
            }
        }
    },
    //Retornas mensagens do canal
    getChannelMessages: function (channelData, callback) {
        var me = this,
            channel = 'channel-' + channelData.id,
            request = window.indexedDB.open(channel, 1);

        request.onupgradeneeded = Ext.bind(me.newChannelOnUpgradeNeeded, me, {
            id: channelData.id,
            name: channelData.name,
            idb: channel,
            server: channelData.server
        }, true);

        request.onsuccess = function (event) {
            var db = request.result;

            if (!db.objectStoreNames.contains('channel')) {
                callback([]);
                return;
            }

            var transaction = db.transaction(['channel'], 'readonly'),
                objectStore = transaction.objectStore('channel');

            objectStore.getAll().onsuccess = function (event) {
                var cursor = event.target.result;
                callback(cursor)
            }
        }
    },
    //Salva a mensagem recebida no IDB
    saveMessage: function (message) {
        var me = this,
            channel = "channel-" + message.channel,
            request = window.indexedDB.open(channel, 1);

        request.onupgradeneeded = Ext.bind(me.newChannelOnUpgradeNeeded, me, {
            id: message.channel,
            name: message.channelName,
            idb: channel,
            server: message.server
        }, true);

        request.onsuccess = function (event) {
            var db = request.result;

            // open a read/write db transaction, ready for adding the data
            var transaction = db.transaction(['channel'], "readwrite");

            // report on the success of the transaction completing, when everything is done
            transaction.oncomplete = function (event) {};

            transaction.onerror = function (event) {};

            // create an object store on the transaction
            var objectStore = transaction.objectStore('channel');

            // Make a request to add our newItem object to the object store
            var objectStoreRequest = objectStore.put(message);

            objectStoreRequest.onsuccess = function (event) {
                // report the success of our request
            };
        };
    },
    //onupgradeneeded do user-channel do idb
    userChannelOnUpgradeNeeded: function (event) {
        var db = event.target.result,
            store = db.createObjectStore("user-channels", {
                keyPath: "id"
            }),
            transaction = event.target.transaction;

        store.createIndex("user", "user", {
            unique: false
        });

        transaction.oncomplete = function (event) {
            // Now store is available to be populated
            console.log('[registerChannel] onupgradeneeded - transaction.oncomplete')
        }
    },
    //onupgradeneeded do user-servers do idb
    userServerOnUpgradeNeeded: function (event) {
        var db = event.target.result,
            store = db.createObjectStore("user-servers", {
                keyPath: "id"
            }),
            transaction = event.target.transaction;

        store.createIndex("user", "user", {
            unique: false
        });

        transaction.oncomplete = function (event) {
            // Now store is available to be populated
            console.log('[userServerOnUpgradeNeeded] onupgradeneeded - transaction.oncomplete')
        }
    },
    //onupgradeneeded dos channels do idb
    newChannelOnUpgradeNeeded: function (event, channel) {
        var me = this,
            db = event.target.result,
            store = db.createObjectStore('channel', {
                keyPath: "messageId"
            }),
            transaction = event.target.transaction;

        transaction.oncomplete = function (event) {
            console.log('[saveMessage] onupgradeneeded - transaction.oncomplete')
        }
        transaction.onerror = function (event) {
            console.log('[saveMessage] onupgradeneeded - transaction.onerror')
        };

        me.registerChannel({
            id: channel.id,
            name: channel.name,
            idb: 'channel-' + channel.id,
            server: channel.server,
        });
    }
});