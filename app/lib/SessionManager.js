Ext.define('TskPlan.Mobile.lib.SessionManager', {
    singleton: true,
    cookieName: 'TSKPLAN_SESSION',

    constructor: function () {
        var me = this,
            cookie = Ext.util.Cookies.get(me.cookieName),
            token = cookie ? JSON.parse(cookie) : null;

        window.TSKPLAN_SESSION = {
            token: {
                hash: null
            }
        }

        if (token) {
            // TSKPLAN_SESSION.token.hash = token.token;
            // me.configureRequest();
        }
    },

    validSession: function () {
        var me = this,
            cookie = Ext.util.Cookies.get(me.cookieName);

        if (cookie) {
            cookie = JSON.parse(cookie);

            var token = {};

            token.hash = cookie.Token;
            //
            Ext.apply(token, me.parseJwt(token.hash));
            //
            TSKPLAN_SESSION.token = token;

            return true;
        }

        return false;
    },

    logout: function () {
        var me = this;

        Ext.util.Cookies.clear(me.cookieName);
        location.reload();
    },

    login: function (email, senha, onSuccess, onFailure) {
        var me = this;

        me.requestLogin(API_CONFIG.loginUrl, 'POST', {
            User: email,
            Password: senha
        }, function (response, options) {
            if (response.responseText !== "") {
                var reponseJson = JSON.parse(response.responseText),
                    token = {};

                token.hash = reponseJson.Token;
                //
                Ext.apply(token, me.parseJwt(token.hash));
                //
                TSKPLAN_SESSION.token = token;
                //
                Ext.util.Cookies.set(me.cookieName, response.responseText);

                onSuccess();
            } else {
                onFailure();
            }
        }, function (response, options) {
            if (response.responseText !== "") {
                var resp = JSON.parse(response.responseText);

                onFailure(response.responseText);
            } else
                onFailure();
        });
    },

    requestLogin: function (url, method, jsonData, onSuccessRequest, onFailureRequest) {
        Ext.Ajax.request({
            url: url,
            method: method,
            timeout: 300000,
            jsonData: jsonData,
            headers: {
                'Content-Type': 'application/json'
            },
            success: onSuccessRequest,
            failure: onFailureRequest
        });
    },

    parseJwt: function (token) {
        var base64Url = token.split('.')[1],
            base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/'),
            jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
                return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
            }).join(''));

        return JSON.parse(jsonPayload);
    }
});

// Ext.define('OpusStorage.lib.SessionManager', {
//     singleton: true,
//     cookieName: 'OPUSSTORAGE_TOKEN',
//     validSession: function () {
//         var me = this,
//             cookie = Ext.util.Cookies.get(me.cookieName);

//         if (cookie) {
//             cookie = JSON.parse(cookie);

//             return me.verifySession(cookie.token);
//         }

//         return false
//     },

//     verifySession: function (token) {
//         var me = this;

//         return new Ext.Promise(function (resolve, reject) {
//             Ext.Ajax.request({
//                 url: OPUSSTORAGE_CONFIG.verifySession,
//                 method: 'GET',
//                 timeout: 300000,
//                 jsonData: {},
//                 headers: {
//                     'Content-Type': 'application/json',
//                     'authorization': 'bearer ' + token
//                 },
//                 success: function (response, options) {
//                     if (response.status == 200) {
//                         var _token = {};

//                         _token.hash = token;
//                         //
//                         Ext.apply(_token, me.parseJwt(_token.hash));
//                         //
//                         OS_SESSION.token = _token;

//                         resolve(response.responseText);
//                         return true;
//                     } else {
//                         Ext.util.Cookies.clear(me.cookieName)

//                         reject(response.status);
//                         return false
//                     }
//                 },
//                 failure: function (response, options) {
//                     if (response.responseText !== "") {
//                         var resp = JSON.parse(response.responseText);

//                         if (resp.message) {
//                             Ext.MessageBox.show({
//                                 message: resp.message,
//                                 title: resp.title,
//                                 icon: Ext.Msg.ERROR,
//                                 buttons: Ext.Msg.OK
//                             });
//                         }
//                     }
//                     alert('Token expirado');

//                     Ext.util.Cookies.clear('OPUSSTORAGE_TOKEN');
//                     //TODO: verificar necessidade do reload
//                     location.reload();

//                     reject(response.status);
//                 }
//             });
//         });
//     },

//     logout: function () {
//         Ext.util.Cookies.clear('OPUSSTORAGE_TOKEN')
//         location.reload();
//     },

//     requestFuncionalidades: function (onSuccessRequest, onFailureRequest) {
//         Ext.Ajax.request({
//             url: OPUSSTORAGE_CONFIG.verifySession,
//             method: 'GET',
//             timeout: 300000,
//             jsonData: {},
//             headers: {
//                 'Content-Type': 'application/json',
//             },
//             success: onSuccessRequest,
//             failure: onFailureRequest
//         });
//     },

//     configureRequest: function () {
//         var me = this;

//         Ext.Ajax.on({
//             scope: me,
//             beforerequest: me.onBeforeRequest,
//             requestexception: me.onRequestException
//         });
//     },

//     onBeforeRequest: function (conn, options, eOpts) {
//         var me = this;
//         //
//         options.headers = options.headers ? options.headers : {};
//         //
//         if (!options.headers.authorization &&
//             window.OS_SESSION &&
//             window.OS_SESSION.token &&
//             window.OS_SESSION.token.hash) {
//             options.headers.authorization = 'Bearer ' + window.OS_SESSION.token.hash;
//         }
//     },

//     onRequestException: function (conn, response, options, eOpts) {
//         var me = this,
//             message = '',
//             json = null;
//         //
//         try {
//             if (response.responseJson) {
//                 json = response.responseJson;
//             } else if (response.responseText) {
//                 json = JSON.parse(response.responseText);
//             }
//         } catch (error) {
//             console.error('(onRequestException) Erro ao converter o conteúdo para JSON: ' + response.responseText);
//         }
//         //
//         json = json ? json : {
//             message: ''
//         };
//         //
//         if (response.status === 401) {
//             if (!options.suppressDefaultException) {
//                 message = json.message ? json.message : 'Operação não autorizada';

//                 //TODO:Aplicar logica de renovação de Token
//                 alert('Token expirado');
//             }
//         } else if (response.status === 400) {
//             message = json.message ? json.message : 'Ocorreu um erro ao processar a operação';
//         } else if (response.status === 500) {
//             message = 'Ocorreu um erro ao processar a operação'
//             console.error(json.message);
//         }
//         //
//         if (message) {
//             Ext.MessageBox.show({
//                 message: message ? message : 'Ocorreu um erro ao processar a operação',
//                 icon: Ext.Msg.ERROR,
//                 buttons: Ext.Msg.OK
//             });
//         }
//     }
// });