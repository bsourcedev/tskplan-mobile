/**
 * View de seleção de obra para outras telas
 */
Ext.define('TskPlan.Mobile.components.galeria.GaleriaDisplay', {
    extend: 'Ext.Panel',
    xtype: 'galeria-display',
    layout: 'vbox',
    requires: [],
    title: 'Galeria',

    fullscreen: true,
    closable: true,

    bodyCls: 'galeria-display',
    bodyPadding: '5 10 10 10',
    padding: '5 10 10 10',
    scrollable: 'y',
    showAnimation: 'pop',
    hideAnimation: 'popOut',
    closeAction: 'hide',

    defaults: {
        xtype: 'component',
        cls: 'galeria-display-items',
        listeners: {
            element: 'element',
            click: 'onClickFile'
        }
    },

    items: [{
        xtype: 'panel',
        width: '100%',
        docked: 'top',
        cls: '',
        bodyPadding: '5 10 5 10',
        items: [{
            xtype: 'segmentedbutton',
            width: '100%',
            items: [{
                text: 'Todos',
                pressed: true,
                value: 0,
                cls: 'tarefa-lista-segmented-button',
                flex: 0.8
            }, {
                text: 'Media',
                value: 'image',
                cls: 'tarefa-lista-segmented-button',
                flex: 1.1
            }, {
                text: 'Arquivos',
                value: 'file',
                cls: 'tarefa-lista-segmented-button',
                flex: 1.1
            }],
            listeners: {
                toggle: 'onToggleFileType'
            }
        }],
        listeners: {
            click: ()=>{}
        }
    }]
});