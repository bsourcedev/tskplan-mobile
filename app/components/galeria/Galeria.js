/**
 * View de seleção de obra para outras telas
 */
Ext.define('TskPlan.Mobile.components.galeria.Galeria', {
    extend: 'Ext.Panel',
    xtype: 'galeria',
    layout: 'vbox',
    requires: [],

    controller: 'galeria',

    bodyCls: 'galeria',

    defaults: {
        xtype: 'component',
        cls: 'galeria-items',
        listeners: {
            element: 'element',
            click: 'onClickFile'
        }
    },

    items: [],

    loadFiles: function (files) {
        var me = this,
            controller = me.getController();

        controller.files = files;
        controller.displayGalleryWidget();
    }
});