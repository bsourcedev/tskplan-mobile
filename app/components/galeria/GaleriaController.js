/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define('TskPlan.Mobile.view.local.GaleriaController', {
    extend: 'Ext.app.ViewController',

    alias: 'controller.galeria',

    init: function () {
        var me = this,
            view = me.getView();

        // me.displayGalleryWidget();
    },

    displayGalleryWidget: function () {
        var me = this,
            view = me.getView();

        if (!me.files || !me.files.length) {
            //<debug>
            console.log('[Galeria] Nenhum arquivo a ser exibido')
            //</debug>
            return;
        }

        view.removeAll();

        for (let index = 0; index < (me.files.length > 4 ? 3 : (me.files.length - 1)); index++) {
            var file = me.files[index];
            view.add({
                style: `background-image: url("${file.Caminho}")`,
                file: file,
                ...(file.Extensao == 'file' ? {
                    html: `<i class="fas fa-download" style="font-size: 24px;"></i><br>${file.Nome}`,
                    style: '',
                    cls: 'galeria-file'
                } : {})
            });
        }

        if (me.files.length > 4) {
            var plusFile = me.files[3];
            view.add({
                xtype: 'label',
                cls: 'galeria-item-plus',
                style: plusFile.Extensao == 'image' ? `background-image: url("${plusFile.Caminho}")` : '',
                html: '+' + (me.files.length - 3),
                listeners: {
                    scope: me,
                    element: 'element',
                    click: me.onClickOpenGallery
                }
            });
        }
    },

    onClickFile: function (event) {
        var me = this,
            componentEl = event.target.closest('.x-label') 
            || event.target.closest('.galeria-display-file') 
            || event.target.closest('.galeria-items')
            || event.target.closest('.galeria-items')
            || event.target.closest('.galeria-file'),
            component = Ext.getCmp(componentEl.id);

        if (component.file.Extensao == 'image')
            Ext.create('TskPlan.Mobile.components.galeria.ImagemDisplay', {
                file: component.file
            }).show();
        else
            NavigatorManager.file.download(component.file.Caminho, component.file.Nome);
    },

    onClickOpenGallery: function () {
        var me = this,
            view = me.getView(),
            gallery = me.gallery || Ext.create('TskPlan.Mobile.components.galeria.GaleriaDisplay', {
                controller: me
            });

        if (!me.gallery) {
            me.gallery = gallery;
            me.gallery.add(me.files.map(file => new Object({
                style: `background-image: url("${file.Caminho}")`,
                file: file,
                // listeners: {
                //     scope: me,
                //     element: 'element',
                //     click: me.onClickOpenImage
                // }
                ...(file.Extensao == 'file' ? {
                    html: `<i class="fas fa-download" style="font-size: 24px;"></i><br>${file.Nome}`,
                    style: '',
                    cls: 'galeria-display-file'
                } : {})
            })))
        }

        gallery.show();
    },

    onToggleFileType: function (button) {
        var me = this,
            valor = button.getValue();

        if (valor == 0)
            me.gallery.items.items.forEach(item => item.show());
        else {
            me.gallery.items.items.filter(item => item.file && item.file.Extensao !== valor).forEach(item => item.hide());
            me.gallery.items.items.filter(item => item.file && item.file.Extensao == valor).forEach(item => item.show());
        }
    }

});