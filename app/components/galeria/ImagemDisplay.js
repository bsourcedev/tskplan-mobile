/**
 * View de seleção de obra para outras telas
 */
Ext.define('TskPlan.Mobile.components.galeria.ImagemDisplay', {
    extend: 'Ext.Panel',
    xtype: 'imagem-display',
    layout: 'fit',
    requires: [],
    header: false,

    fullscreen: true,

    showAnimation: 'pop',
    hideAnimation: 'popOut',
    closeAction: 'hide',

    defaults: {
        xtype: 'component',
    },

    initialize: function () {
        var me = this;

        console.log(me.file.Caminho)
        
        me.add({
            xtype: 'container',
            flex: 1,
            height: '100%',
            width: '100%',
            items: [{
                xtype: 'component',
                cls: 'galeria-imagem-view',
                flex: 1,
                height: '100%',
                width: '100%',
                style: `background-image:url("${me.file.Caminho}")`
            }, {
                xtype: 'button',
                iconCls: 'x-fas fa-times',
                cls: 'close-button',
                handler: function () {
                    var me = this,
                        view = me.up('panel');

                    view.hide();

                    setTimeout(() => {
                        view.close();
                    }, 650);
                }
            }]
        })
    }
});