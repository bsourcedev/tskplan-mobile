/**
 * A Ext.data.proxy.Client extension for storing data in IndexedDB
 */

window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
window.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;

Ext.define('MyApp.data.proxy.IndexedDbProxy', {
    extend: 'Ext.data.proxy.Client',

    alias: 'proxy.idb',
    alternateClassName: 'Ext.data.IdbProxy',

    config: {
        /**
         * The Database Name
         */
        databaseName: null,

        /**
         * The IndexedDB Object Store name. Defaults to the Model's name if not set
         */
        objectStoreName: null,

        /**
         * private
         */
        uniqueIdStrategy: false,

        /**
         * private
         */
        objectStoreExists: false
    },

    updateModel: function (model) {
        this.callParent(arguments);

        var me = this,
            modelName = model.$className,
            osName = modelName.slice(modelName.lastIndexOf('.') + 1);

        //Default the objectStoreName to the class name of the Model
        if (!me.getObjectStoreName()) {
            me.setObjectStoreName(osName);
        }

        me.setUniqueIdStrategy(true);
    },

    checkObjectStoreExists: function (cb, args) {
        var me = this,
            vRequest,
            model = me.getModel(),
            osName = me.getObjectStoreName() || model.modelName.slice(model.modelName.lastIndexOf('.') + 1);

        var doUpgrade = function (e) {
            var keyPath = {
                    keyPath: model.idProperty,
                    autoIncrement: !true
                },
                os = e.target.result.createObjectStore(osName, keyPath);
        };

        if (!me.getObjectStoreExists()) {
            vRequest = indexedDB.open(me.getDatabaseName());

            vRequest.onupgradeneeded = doUpgrade;

            vRequest.onsuccess = function (e) {
                var db = e.target.result,
                    name = db.name,
                    version = db.version,
                    osExists = db.objectStoreNames.contains(osName),
                    uRequest;

                db.close();

                if (!osExists) {
                    version += 1;
                    uRequest = indexedDB.open(me.getDatabaseName(), version);

                    uRequest.onupgradeneeded = doUpgrade;

                    uRequest.onsuccess = function (e) {
                        e.target.result.close();
                        me.setObjectStoreExists(true);
                        cb.apply(me, args);
                    };
                } else {
                    me.setObjectStoreExists(true);
                    cb.apply(me, args);
                }
            };
        } 

        return me.getObjectStoreExists();
    },

    /**
     * create override
     */
    create: function (operation, callback, scope) {
        var me = this,
            records = operation.getRecords(),
            errors = [],
            data = [],
            createdRecords = [],
            executed = 0,
            totalRecords = records.length,
            result,
            uniqueIdStrategy = me.getUniqueIdStrategy(),
            transaction, store;

        if (!me.checkObjectStoreExists(me.create, arguments)) {
            return;
        }

        var onSuccess = function (e) {
            var db = e.target.result;

            operation.setStarted();

            transaction = db.transaction([me.getObjectStoreName()], "readwrite");
            store = transaction.objectStore(me.getObjectStoreName());

            result = new Ext.data.ResultSet({
                records: createdRecords,
                success: true
            });

            transaction.onerror = function (e) {
                db.close();
                if (operation.process(operation.getAction(), result) === false) {
                    me.fireEvent('exception', me, operation);
                }
                operation.setException(e.target.result);
                if (typeof callback == 'function') {
                    callback.call(scope || me, operation);
                }
            };

            Ext.each(records, function (record) {
                var data = me.getRecordData(record),
                    request = store.add(data),
                    id = record.getId();

                request.onerror = function (e) {
                    executed++;

                    errors.push({
                        clientId: id,
                        error: e.target.result
                    });

                    if (executed === totalRecords) {
                        db.close();
                        if (operation.process(operation.getAction(), result) === false) {
                            me.fireEvent('exception', me, operation);
                        }
                        operation.setException(errors);
                        if (typeof callback == 'function') {
                            callback.call(scope || me, operation);
                        }
                    }
                };

                request.onsuccess = function (e) {
                    executed++;

                    createdRecords.push({
                        clientId: id,
                        id: uniqueIdStrategy ? id : e.target.result,
                        data: data,
                        node: data
                    });

                    if (executed === totalRecords) {
                        db.close();
                        if (operation.process(operation.getAction(), result) === false) {
                            me.fireEvent('exception', me, operation);
                        }
                        if (errors.length > 0) {
                            operation.setException(errors);
                        }
                        if (typeof callback == 'function') {
                            callback.call(scope || me, operation);
                        }
                    }
                };

            });
        };

        var oRequest = indexedDB.open(me.getDatabaseName());
        oRequest.onsuccess = onSuccess;
    },

    /**
     * read override
     */
    read: function (operation, callback, scope) {
        var me = this,
            model = me.getModel(),
            idProperty = model.idProperty,
            params = operation.getParams() || {},
            id = params[idProperty],
            sorters = operation.getSorters(),
            filters = operation.getFilters(),
            page = operation.getPage(),
            start = operation.getStart(),
            limit = operation.getLimit(),
            records = [],
            count = 0,
            filtered, i, ln, transaction, store, cursor, result;

        if (!me.checkObjectStoreExists(me.read, arguments)) {
            return;
        }

        var onSuccess = function (e) {
            var db = e.target.result;

            operation.setStarted();

            transaction = db.transaction([me.getObjectStoreName()], "readonly");
            store = transaction.objectStore(me.getObjectStoreName());

            result = new Ext.data.ResultSet({
                records: records,
                success: true
            });


            //TODO use indexes for valid filters?
            cursor = store.openCursor();

            cursor.onsuccess = function (e) {
                var c = e.target.result,
                    data = {};

                if (c === null) {
                    db.close();
                    result.setSuccess(true);
                    result.setTotal(count);
                    result.setCount(count);

                    if (operation.process(result, operation.getAction()) === false) {
                        me.fireEvent('exception', me, operation);
                    }

                    if (filters && filters.length) {
                        filtered = Ext.create('Ext.util.Collection', function (record) {
                            return record.getId();
                        });
                        filtered.setFilterRoot('data');
                        for (i = 0, ln = filters.length; i < ln; i++) {
                            filtered.addFilter(filters[i]);
                        }
                        filtered.addAll(operation.getRecords());

                        operation.setRecords(filtered.items.slice());
                        resultSet.setRecords(operation.getRecords());
                        resultSet.setCount(filtered.items.length);
                        resultSet.setTotal(filtered.items.length);
                    }

                    if (typeof callback == 'function') {
                        callback.call(scope || me, operation);
                    }
                    return;
                }

                for (var f in c.value) {
                    data[f] = c.value[f];
                }
                records.push(Ext.create(me.model.$className, data));
                count++;
                c['continue']();
            };

            //TODO errors
            cursor.onerror = function (e) {
                //console.debug(e);
            };

            transaction.onerror = function (e) {
                db.close();
                operation.setException(e.target.result);
                if (typeof callback == 'function') {
                    callback.call(scope || me, operation);
                }
            };
        };

        var oRequest = indexedDB.open(me.getDatabaseName());
        oRequest.onsuccess = onSuccess;

    },

    /**
     * update override
     */
    update: function (operation, callback, scope) {
        var me = this,
            records = operation.getRecords(),
            errors = [],
            data = [],
            updatedRecords = [],
            executed = 0,
            totalRecords = records.length,
            result,
            transaction, store;

        if (!me.checkObjectStoreExists(me.update, arguments)) {
            return;
        }

        var onSuccess = function (e) {
            var db = e.target.result;

            operation.setStarted();

            transaction = db.transaction([me.getObjectStoreName()], "readwrite");
            store = transaction.objectStore(me.getObjectStoreName());

            result = new Ext.data.ResultSet({
                records: updatedRecords,
                success: true
            });

            transaction.onerror = function (e) {
                if (operation.process(operation.getAction(), result) === false) {
                    me.fireEvent('exception', me, operation);
                }
                operation.setException(e.target.result);
            };

            Ext.each(records, function (record) {
                var data = me.getRecordData(record),
                    request = store.put(data),
                    id = record.getId();

                request.onerror = function (e) {
                    executed++;

                    errors.push({
                        clientId: id,
                        error: e.target.result
                    });

                    if (executed === totalRecords) {
                        db.close();
                        if (operation.process(operation.getAction(), result) === false) {
                            me.fireEvent('exception', me, operation);
                        }
                        operation.setException(errors);
                        if (typeof callback == 'function') {
                            callback.call(scope || me, operation);
                        }
                    }
                };

                request.onsuccess = function (e) {
                    executed++;

                    updatedRecords.push({
                        clientId: id,
                        id: id,
                        data: Ext.create(me.model.$className, data),
                        node: data
                    });

                    if (executed === totalRecords) {
                        db.close();
                        if (operation.process(operation.getAction(), result) === false) {
                            me.fireEvent('exception', me, operation);
                        }
                        if (errors.length > 0) {
                            operation.setException(errors);
                        }
                        if (typeof callback == 'function') {
                            callback.call(scope || me, operation);
                        }
                    }
                };

            });
        };

        var oRequest = indexedDB.open(me.getDatabaseName());
        oRequest.onsuccess = onSuccess;
    },

    /**
     * destroy override
     */
    destroy: function (operation, callback, scope) {
        var me = this,
            records = operation.getRecords(),
            errors = [],
            data = [],
            destroyedRecords = [],
            executed = 0,
            totalRecords = records.length,
            result,
            uniqueIdStrategy = me.getUniqueIdStrategy(),
            transaction, store;

        if (!me.checkObjectStoreExists(me.destroy, arguments)) {
            return;
        }

        var onSuccess = function (e) {
            var db = e.target.result;

            operation.setStarted();

            result = new Ext.data.ResultSet({
                records: destroyedRecords,
                success: true
            });

            transaction = db.transaction([me.getObjectStoreName()], "readwrite");
            store = transaction.objectStore(me.getObjectStoreName());

            transaction.onerror = function (e) {
                db.close();
                if (operation.process(operation.getAction(), result) === false) {
                    me.fireEvent('exception', me, operation);
                }
                operation.setException(e.target.result);
                if (typeof callback == 'function') {
                    callback.call(scope || me, operation);
                }
            };

            Ext.each(records, function (record) {
                var id = record.getId();
                request = store['delete'](id);

                request.onerror = function (e) {
                    executed++;

                    errors.push({
                        clientId: id,
                        error: e.target.result
                    });

                    if (executed === totalRecords) {
                        db.close();
                        if (operation.process(operation.getAction(), result) === false) {
                            me.fireEvent('exception', me, operation);
                        }
                        operation.setException(errors);
                        if (typeof callback == 'function') {
                            callback.call(scope || me, operation);
                        }
                    }
                };

                request.onsuccess = function (e) {
                    executed++;

                    destroyedRecords.push({
                        clientId: id,
                        id: id,
                        data: data,
                        node: data
                    });

                    if (executed === totalRecords) {
                        db.close();
                        if (operation.process(operation.getAction(), result) === false) {
                            me.fireEvent('exception', me, operation);
                        }
                        if (errors.length > 0) {
                            operation.setException(errors);
                        }
                        if (typeof callback == 'function') {
                            callback.call(scope || me, operation);
                        }
                    }
                };

            });
        };

        var oRequest = indexedDB.open(me.getDatabaseName());
        oRequest.onsuccess = onSuccess;

    },

    /**
     * @private
     * Bundle model data up into an object to write to in IndexedDB Object Store.
     */
    getRecordData: function (record) {
        var me = this,
            fields = record.getFields(),
            idProperty = record.getIdProperty(),
            data = {},
            name, value;

        fields.each(function (field) {
            if (field.getPersist()) {
                name = field.getName();
                if (name === idProperty && !me.getUniqueIdStrategy()) {
                    return;
                }
                value = record.get(name);
                data[name] = value;
            }
        }, me);

        return data;
    }

});