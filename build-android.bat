@Echo off

python replaceVersion.py

cd phonegap

cordova platform remove android

cordova platform add android

cd ..

sencha switch 7.2.0.66

sencha app build native

cd phonegap

phonegap build android --release

xcopy tskplan.keystore "platforms\android\app\build\outputs\apk\release"

cd "platforms\android\app\build\outputs\apk\release"

REM del TskPlan.apk

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore tskplan.keystore app-release-unsigned.apk tskplan -storepass "TskPl@n2000"

zipalign -v 4 app-release-unsigned.apk TskPlan.apk 

start .

pause
