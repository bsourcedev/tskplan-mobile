#!/usr/bin/env python3
import os
import fileinput
import pathlib

#Pega codigo da versão
from xml.dom import minidom

path = (os.path.dirname(os.path.abspath(__file__)))
path_phonegap = path + '\\phonegap\\'
path_android = path_phonegap + '\\platforms\\android\\app\\build\\outputs\\apk\\release\\'
path_android_www = path_phonegap + '\\www\\'

config_file = path_phonegap+'config.xml'
html_file = path+'\\index.html'
html_apiconfig = path+'\\apiConfig.js'

def rreplace(s, old, new, occurrence):
  li = s.rsplit(old, occurrence)
  return new.join(li)

xmldoc = minidom.parse(config_file)
itemlist = xmldoc.getElementsByTagName('widget')

currentVersion = itemlist[0].attributes['version'].value
print ("Current Version: ", currentVersion)

oldVersionNumber = currentVersion.rpartition('.')[-1]
newVersionNumber = int(oldVersionNumber)+1

newVersion = rreplace(currentVersion, oldVersionNumber, str(newVersionNumber), 1)
print ("New Version: ", newVersion)

#Atualiza Versão no config.xml
with fileinput.FileInput(config_file, inplace=True) as file:
    for line in file:
        print(line.replace('version="'+currentVersion+'"', 'version="'+newVersion+'"'), end='')

#Atualiza Versão no index.html
with fileinput.FileInput(html_file, inplace=True) as file:
    for line in file:
        print(line.replace('version="'+currentVersion+'"', 'version="'+newVersion+'"'), end='')
        


#Atualiza apiConfig para produção

with fileinput.FileInput(html_apiconfig, inplace=True, backup='.bak') as file:
    for line in file:
        print(line.replace('apiUrl: \'http://localhost:59488/\'', '//apiUrl: \'http://localhost:59488/\''), end='')
        
with fileinput.FileInput(html_apiconfig, inplace=True, backup='.bak') as file:
    for line in file:
        print(line.replace('//apiUrl: \'https://appapi.tskplan.com.br/\'', 'apiUrl: \'https://appapi.tskplan.com.br/\' '), end='')


   
#Copy do apiConfig p/ www

os.chdir(path) 

os.system('xcopy apiConfig.Js "'+path_android_www+'" /Y')



#Retorna apiConfig para dev apos copiar

with fileinput.FileInput(html_apiconfig, inplace=True, backup='.bak') as file:
    for line in file:
        print(line.replace('//apiUrl: \'http://localhost:59488/\'', 'apiUrl: \'http://localhost:59488/\''), end='')
        
with fileinput.FileInput(html_apiconfig, inplace=True, backup='.bak') as file:
    for line in file:
        print(line.replace('apiUrl: \'https://appapi.tskplan.com.br/\'', '//apiUrl: \'https://appapi.tskplan.com.br/\' '), end='')


   
#Build app

os.chdir(path)

os.system('sencha switch 7.2.0.66')

os.system('sencha app build native')



#Build phonegap

os.chdir(path_phonegap)

os.system('phonegap build android --release')

#Copy keystore para pasta release

os.system('xcopy tskplan.keystore "'+path_android+'" /Y')



#Build apk

os.chdir(path_android)

try:
    os.remove('TskPlan.apk')
except OSError:
    pass
    
os.system('jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore tskplan.keystore app-release-unsigned.apk tskplan -storepass "TskPl@n2000"')

os.system('zipalign -v 4 app-release-unsigned.apk TskPlan.apk')

os.system('start .')

os.system('pause')