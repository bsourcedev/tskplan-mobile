﻿var started = false,
    socket = null,
    socketStarted = false,
    socketRegistered = false,
    socketSession = null;

//Socket
function startSocket() {
    var me = this;

    me.socket = new WebSocket('ws://localhost:59291/ws');
    //me.socket = new WebSocket('wss://message.tskplan.com.br/ws');
    
    //Open
    me.socket.onopen = function (event) {
        console.log('%c[socket] Connection opened', 'color: #bada55')
        //window.BSF.token
        me.socketStarted = true;

        if (me.socketSession) {
            me.RegisterSession(me.socketSession);
            console.log('%c[socket] socket session registered', 'color: #bada55')
        }
    };
    //Close
    me.socket.onclose = function (event) {
        console.log('%c[socket] Connection closed', 'color: #bada55')
        //<debug>
        console.log('%c[socket] **Carregar possiveis mensagens perdidas no periodo de reconexão', 'color: #bada55')
        //</debug>

        me.socket = null;
        me.socketStarted = false;
        me.socketRegistered = false;

        me.startSocket();
    };
    //Error
    me.socket.onerror = () => {
        console.log('%c[socket] Connection error ', 'color: #bada55', arguments)
    };
    //Message
    me.socket.onmessage = function (event) {
        var data = event.data;

        if (isJson(data)) {
            var jsonData = JSON.parse(data);

            console.log('%c[socket] Message received from server: ' + jsonData.route, 'color: #bada55', jsonData)

            if (jsonData.route == 'message')
                self.postMessage({
                    method: 'messageReceived',
                    data: jsonData.data
                })
            else if (jsonData.route == 'tokenRegistered') {
                if (jsonData.data == me.socketSession)
                    me.socketRegistered = true;
            }
        }
    };
}

self.onmessage = function (event) {
    var me = this,
        data = event.data,
        route = data.route,
        postData = data.data;

    if (route == 'sessionReg')
        me.onSessionReg(event, postData);
    if (route == 'message')
        me.onMessage(postData);
}

//onMessage envia mensagem ao socket
function onMessage(data) {
    var me = this;

    console.log('%c[socket] Mensagem enviada', 'color: #bada55', data)

    if (me.socketStarted)
        if (socket.readyState)
            socket.send(JSON.stringify({
                route: 'message',
                data: data
            }));
        else
            setTimeout(function () {
                me.onMessage(data);
            }, 100);
    else {
        startSocket();
        setTimeout(function () {
            me.onMessage(data);
        }, 100);
    }
}

//onSessionReg
function onSessionReg(event, data) {
    var me = this;

    me.socketSession = data.session;

    RegisterSession(data.session);

    console.log('%c[socket] socket session registered', 'color: #bada55')
}

//Registra a sessão do usuario no socket
function RegisterSession(session) {
    var me = this;

    if (me.socketStarted)
        if (socket.readyState)
            socket.send(JSON.stringify({
                route: 'registerToken',
                data: session
            }));
        else
            setTimeout(function () {
                me.RegisterSession(session);
            }, 100)
    else {
        startSocket();
    }
    //Pesquisar refresh token
}

//Verifica se a string é um json
function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

//
function postHandler(url, data, callback) {
    fetch(url, {
        method: 'post',
        body: data
    }).then(function (response) {
        return response.text();
    }).then(function (data) {
        console.log('%c[sw] post response:', 'color: #ff41ff', data);
        callback(data);
    });
}