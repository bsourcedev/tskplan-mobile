window.API_CONFIG = {
    apiUrl: 'http://localhost:59488/'
    //apiUrl: 'https://appapi.tskplan.com.br/'                                                                                                              
};

//Login
window.API_CONFIG.loginUrl = window.API_CONFIG.apiUrl + 'v1/session';

//Obra
window.API_CONFIG.obraUrl = window.API_CONFIG.apiUrl + 'v1/obra';
window.API_CONFIG.obraLocalUrl = window.API_CONFIG.obraUrl + '/local/';

//Checklist
window.API_CONFIG.ChecklistUrl = window.API_CONFIG.apiUrl + 'v1/checklist';
window.API_CONFIG.ChecklistListaUrl = window.API_CONFIG.ChecklistUrl + '/lista/';
window.API_CONFIG.ChecklistArquivosUrl = window.API_CONFIG.ChecklistUrl + '/arquivos';

//Tarefa
window.API_CONFIG.TarefaUrl = window.API_CONFIG.apiUrl + 'v1/tarefa';
window.API_CONFIG.TarefaArquivosUrl = window.API_CONFIG.TarefaUrl + '/arquivos';
window.API_CONFIG.TarefaOS = window.API_CONFIG.TarefaUrl + '/OS';
window.API_CONFIG.TarefaOSLocaistUrl = window.API_CONFIG.TarefaOS + '/locais';
window.API_CONFIG.TarefaOSLocaisAuxiliarUrl = window.API_CONFIG.TarefaOSLocaistUrl + '/auxiliar';
window.API_CONFIG.TarefaListaUrl = window.API_CONFIG.apiUrl + 'v1/tarefa/lista';
window.API_CONFIG.TarefaListaLocaisUrl = window.API_CONFIG.apiUrl + 'v1/tarefa/listaLocais';

//Media
window.API_CONFIG.Media = window.API_CONFIG.apiUrl + 'v1/media';

//Entrega
window.API_CONFIG.Entrega = window.API_CONFIG.apiUrl + 'v1/entrega';
window.API_CONFIG.EntregaProduto = window.API_CONFIG.Entrega + '/produtos';
window.API_CONFIG.EntregaProdutoLista = window.API_CONFIG.Entrega + '/produtosLista';
window.API_CONFIG.EntregaPedido = window.API_CONFIG.Entrega + '/pedidos';
window.API_CONFIG.EntregaFornecedor = window.API_CONFIG.Entrega + '/fornecedor';

//Global
window.API_CONFIG.Global = window.API_CONFIG.apiUrl + 'v1/global';
window.API_CONFIG.Global = {
    Obra: window.API_CONFIG.Global + '/Obra',
    ModeloServico: window.API_CONFIG.Global + '/ModeloServico',
    UnidadeMedida: window.API_CONFIG.Global + '/UnidadeMedida'
};

//AgendaEntrega
window.API_CONFIG.AgendaEntrega = window.API_CONFIG.apiUrl + 'v1/agendaEntrega';
window.API_CONFIG.AgendaEntrega = {
    Agenda: window.API_CONFIG.AgendaEntrega,
    Calendario: window.API_CONFIG.AgendaEntrega + '/calendario',
    Produto: window.API_CONFIG.AgendaEntrega + '/produto',
};

//Chat
window.API_CONFIG.Chat = window.API_CONFIG.apiUrl + 'v1/chat';
window.API_CONFIG.Chat = {
    Chat: window.API_CONFIG.Chat,
    Message: window.API_CONFIG.Chat+'/Message',
    MessageBuffer: window.API_CONFIG.Chat+'/MessageBuffer',
    UserChannels: window.API_CONFIG.Chat+'/UserChannels',
    NewChannels: window.API_CONFIG.Chat+'/NewChannels',
    TokenReg: window.API_CONFIG.Chat+'/TokenReg',
    UserChannelReg: window.API_CONFIG.Chat+'/UserChannelReg',
    Servers: window.API_CONFIG.Chat+'/Servers',
};