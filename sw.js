var started = false,
    firebaseToken;

var apiUrl = 'http://localhost:59488/v1';
//var apiUrl = 'https://appapi.tskplan.com.br/v1';

//
self.addEventListener('install', event => {
    self.skipWaiting();
});

self.addEventListener('install', event => {
    self.skipWaiting();

    //event.waitUntil(
    //    // caching etc
    //);
});

self.addEventListener('sync', function (event) {
    if (event.tag === 'start') {
        event.waitUntil(startSync());
    }

    if (!started) {
        started = true;
        firstStart();
    }

    console.log('%c[sw] Background Sync', 'color: #ff41ff');
});

self.addEventListener('push', function (event) {
    //console.log('%c[sw] Background push event', 'color: #ff41ff',event);

    //self.clients.matchAll().then(clients => {
    //    clients.forEach(client => {
    //        client.postMessage({
    //            method: 'messageReceived',
    //            data: event.data.json()
    //        })
    //    });
    //});
});

function firstStart() {
    console.log('%c[sw] First Start', 'color: #ff41ff');
}

async function startSync() {
    fetch(new Request("/app.json", {
            cache: "no-store"
        }))
        .then(response => {
            if (response.status == 200) {
                return response.text();
            } else {
                throw new Error("" + response.status + " " + response.statusText);
            }
        })
        .then(responseText => {
            console.log(responseText);
        })
}

//Client event Listener
self.onmessage = function (event) {
    var me = this,
        method = event.data.method,
        data = event.data.data;

    me.globalEvent = event;

    if (method == 'sendMessage')
        me.onSendMessage(event, data);
    else if (method == 'tokenReg')
        me.onTokenReg(event, data);
    else if (method == 'registerUserChannel')
        me.onRegisterUserChannel(event, data);
    else if (method == 'getChannelMessages')
        me.onGetChannelMessages(event, data);
    else if (method == 'getUserChannel')
        me.onGetUserChannel(event, data);
}

//Envia mensagem ao canal
function onSendMessage(event, data) {
    console.log('%c[sw] onSendMessage', 'color: #ff41ff', data);

    // data.responseGuid = data.responseGuid ? data.responseGuid : '00000000-0000-0000-0000-000000000000';

    fetch(apiUrl + '/chat/message', {
        method: 'post',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: data
    }).then(function (response) {
        return response.text();
    }).then(function (response) {
        var data = response.Data;

        console.log('%c[sw] post response:', 'color: #ff41ff', data);
        // event.source.postMessage("/message proceeded");
        self.postMessage('/message proceeded')
    });
}

//Registra o usuario no canal
function onRegisterUserChannel(event, data) {
    console.log('%c[sw] onRegisterUserChannel', 'color: #ff41ff', data);
    postHandler('/UserChannelReg', data, (returnedData) => {
        // event.source.postMessage("/UserChannelReg proceeded");
        self.postMessage('/UserChannelReg proceeded')
    });
}

//Pega as canais do usuario
function onGetUserChannel(event, data) {
    fetch(apiUrl + '/chat' + '/UserChannels?usuario=' + data.usuario, {
        method: 'get'
    }).then(function (response) {
        return response.json();
    }).then(function (response) {
        var data = response.Data;

        console.log('%c[sw] /UserChannels ' + data.length + ' canais carregados', 'color: #ff41ff', data);

        // event.source.postMessage({
        //     method: 'userChannels',
        //     data: data
        // });

        self.postMessage({
            method: 'userChannels',
            data: data
        })
    });
}

//Pega as mensagens do canal
function onGetChannelMessages(event, data) {
    var channel = data.channel;

    fetch(apiUrl + '/chat' + '/MessageBuffer?channel=' + channel, {
        method: 'get'
    }).then(function (response) {
        return response.json();
    }).then(function (response) {

        var data = response.Data;
        console.log('%c[sw] /MessageBuffer [' + channel + '] ' + data.length + ' messages loaded', 'color: #ff41ff');

        // event.source.postMessage({
        //     method: 'messageBuffer',
        //     data: data
        // });

        self.postMessage({
            method: 'messageBuffer',
            data: data
        })
    });
}

//Registra o token do firebase na api
function onTokenReg(event, data) {
    var me = this;

    console.log('%c[sw] onTokenReg', 'color: #ff41ff', data);

    postHandler('/TokenReg', data, (returnedData) => {
        var token = JSON.parse(data).token;
        console.log('%c[sw] token registered', 'color: #ff41ff', data);
        //event.source.postMessage("/ToknReg proceeded");

        me.firebaseToken = token;
    });
}

//
function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
//
function postHandler(url, data, callback) {

    fetch(apiUrl + '/chat' + url, {
        method: 'post',
        body: data
    }).then(function (response) {
        return response.text();
    }).then(function (response) {
        var data = response.Data;

        console.log('%c[sw] post response:', 'color: #ff41ff', data);
        callback(data);
    });
}