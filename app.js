/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
Ext.application({
    extend: 'TskPlan.Mobile.Application',

    name: 'TskPlan.Mobile',

    requires: [
        'TskPlan.Mobile.*',
        'Ext.*'
    ],

    // The name of the initial view to create.
    // mainView: 'TskPlan.Mobile.view.login.Login'
});
